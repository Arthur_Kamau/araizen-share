﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share_Service
{
    class App_History
    {

        public string  Action { get; set; } //upload or download
        public string  FileName { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }
        public string  DestinationEmail { get; set; }
        public string SourceEmail { get; set; }
        public string UnixTimeFileItemReferenceId { get; set; }
        public DateTime CompletionDate { get; set; }

        public App_History(string action, string fileName, string fileType, string fileSize, string destinationEmail, string sourceEmail, DateTime completionDate,string unixTimeFileItemReferenceId)
        {
            Action = action;
            FileName = fileName;
            FileType = fileType;
            FileSize = fileSize;
            DestinationEmail = destinationEmail;
            SourceEmail = sourceEmail;
            CompletionDate = completionDate;
            UnixTimeFileItemReferenceId = UnixTimeFileItemReferenceId;
        }
    }
}
