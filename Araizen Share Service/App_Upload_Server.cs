﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Araizen_Share_Service
{
    class App_Upload_Server
    {

        #region  Upload file
        public static async Task<string> Upload( string name, string email, string olddate, string fileName, string filePath)
        {
            string baseUrl = App_Url.UploadsUrlAddEmailDateFileName();
            byte[] fileData = System.IO.File.ReadAllBytes(filePath);

            string date = olddate.Replace("/", "-").Replace(":", "-").Replace(" ", "-");
            using (var client = new HttpClient())
            {
                using (var content =
                    new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                {
                    var memStream = new MemoryStream(fileData);
                    var streamContent = new StreamContent(memStream);
                    content.Add(streamContent, "file", fileName);

                    ///share/upload
                    /////name , email , date
                  //   Console.WriteLine("send to {0}", string.Format(baseUrl + "{0}/{1}/{2}", email, date, name));
                    using (
                       var message =
                          await client.PostAsync(string.Format(baseUrl + "{0}/{1}/{2}", email, date, name), content)) //http://127.0.0.1:12000/share/upload/sample-file
                    {
                        var input = await message.Content.ReadAsStringAsync();
                      //   Console.WriteLine("hey response {0}", input);

                        memStream.Flush();
                        memStream.Close();
                        streamContent.Dispose();
                        return !string.IsNullOrWhiteSpace(input) ? Regex.Match(input, @"http://\w*\.directupload\.net/images/\d*/\w*\.[a-z]{3}").Value : null;
                    }
                }
            }
        }
        #endregion


        //set is started
        #region SetuploadItemIsStarted
        public void SetuploadItemIsStarted(App_Upload uploadItem)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.UpdateUploadItemIsStarted());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(uploadItem);
              //   Console.WriteLine("SetuploadItemIsStarted sending {0} ", json);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
              //   Console.WriteLine("the result is {0} ", result);


            }
        }
        #endregion

        //set is complted
        #region SetuploadItemIsComlplete
        public void SetuploadItemIsComlplete(App_Upload uploadItem)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.UpdateUploadItemIsCompleted());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(uploadItem);

              //   Console.WriteLine("SetuploadItemIsComlplete sending {0} ", json);


                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
              //   Console.WriteLine("the result is {0} ", result);


            }
        }
        #endregion

    }
}