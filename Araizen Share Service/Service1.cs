﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share_Service
{
    public partial class Service1 : ServiceBase
    {
        ShareBootstrap sharebootstraper = new ShareBootstrap();
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
           
            CreateDatabaseFilesIfNecessary();
            sharebootstraper.Start();
        }

        protected override void OnStop()
        {
            sharebootstraper.Stop();
        }

        #region CreateDatabaseFilesIfNecessary
        private static void CreateDatabaseFilesIfNecessary()
        {
            //create databases if not exist
            //
            //first get all paths
            //notifications db
            App_System_Paths paths = new App_System_Paths();

            var pathToNotificationsDb = paths.NotificationsSQL();

            //downloading db
            var pathToDownloadDb = paths.DownloadSQL();

            //uploading db
            var pathToUploadDb = paths.UploadSQL();

            //history db
            var pathToHistoryDb = paths.HistorySQL();



        }
        #endregion

    }
}
