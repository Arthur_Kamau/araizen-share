﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share_Service
{
    class App_System_Paths
    {
        String SystemApplicationDataStorage = String.Empty;

        /// <summary>
        /// constructor intializes SystemApplicationDataStorage variable
        /// 
        /// </summary>
        /// <returns> (Constructor)</returns>
        #region path to system data storage
        public App_System_Paths()
        {
            // The folder for the roaming current user 
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            // Combine the base folder with your specific folder....
            string specificFolder = Path.Combine(folder, "Araizen Share");

            // CreateDirectory will check if folder exists and, if not, create it.
            // If folder exists then CreateDirectory will do nothing.
            Directory.CreateDirectory(specificFolder);

            SystemApplicationDataStorage = specificFolder;
        }
        #endregion

        public string GetSystemApplicationDataStorage()
        {
            return SystemApplicationDataStorage;
        }

        /// <summary>
        /// share operation s folder
        /// sub folder 1) uploads 2)Downloads 3)logs
        /// path  ({windows installed drive}/{user name}/Araizen share/)
        /// </summary>
        /// <returns> string (folder)</returns>
        #region ShareOperationsFolder
        public string ShareOperationsFolder()
        {
            // The folder for the roaming current user 
            //string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            // Combine the base folder with your specific folder....
            string specificFolder = Path.Combine(folder, "Araizen Share");

            // CreateDirectory will check if folder exists and, if not, create it.
            // If folder exists then CreateDirectory will do nothing.
            Directory.CreateDirectory(specificFolder);

            return specificFolder;
        }
        #endregion

        /// <summary>
        /// path to my documents folder
        /// </summary>
        /// <returns></returns>
        #region documents folder
        public string MyDocuments()
        {
           // Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        }
        #endregion

        /// <summary>
        ///  path to downloads folder
        /// </summary>
        /// <returns></returns>
        #region downloads folder
        public string MyDownloads()
        {
            return KnownFolders.GetPath(KnownFolder.Downloads);
        }
        #endregion

        /// <summary>
        ///  path to videos folder
        /// </summary>
        /// <returns></returns>
        #region videos folder
        public string MyVideos()
        {
          ////   Console.WriteLine("Path to videos folder {0} ", Environment.GetFolderPath(Environment.SpecialFolder.MyVideos));
            return Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
        }
        #endregion

        /// <summary>
        /// path to araizen Share  data folder
        /// example of file stored are ->Store auth,reg,tandom,key files
        /// </summary>
        /// <returns> path to araizen share data folder</returns>
        #region PathToAraizenShare
        public string PathToAraizenShare()
        {

            // The folder for the roaming current user 
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            // Combine the base folder with your specific folder....
            string specificFolder = Path.Combine(folder, "Araizen Share");

            // CreateDirectory will check if folder exists and, if not, create it.
            // If folder exists then CreateDirectory will do nothing.
            Directory.CreateDirectory(specificFolder);

            // SystemApplicationDataStorage = specificFolder;
            return specificFolder;
        }
        #endregion

        /// <summary>
        /// share folder in documents 
        /// this is optional if special folder ({windows installed drive}/{user name}/Araizen share/)
        /// fails use this directory
        /// </summary>
        /// <returns></returns>
        #region Share Folder
        public string ShareFolder()
        {
          

                String path = string.Concat(MyDocuments(), "\\Araizen Share");
                bool exists = Directory.Exists(path);
                if (!exists)
                {
                    Directory.CreateDirectory(path);
                }
                return path;
          
        }
        #endregion

        /// <summary>
        /// licence file that works with registry keys
        /// </summary>
        /// <returns></returns>
        #region ShareLicenceJsonFile
        public string ShareLicenceJsonFile()
        {

            String path = string.Concat(SystemApplicationDataStorage, "\\lice.json");

            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
            }
            return path;

        }
        #endregion

        /// <summary>
        /// ShareInboxJsonFile 
        /// </summary>
        /// <returns></returns>
        #region Share inbox json file
        public string ShareInboxJsonFile()
        {

            String path = string.Concat(SystemApplicationDataStorage, "\\inbox.json");

            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
            }
            return path;

        }
        #endregion

        /// <summary>
        /// waiting user consent to download 
        /// </summary>
        /// <returns></returns>
        #region Share install  install 
        public string AppInstallDetails()
        {

            String path = string.Concat(SystemApplicationDataStorage, "\\install.json");

            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
            }
            return path;

        }
        #endregion

        /// <summary>
        /// share upload is where the file is
        /// </summary>
        /// <returns></returns>
        #region Share download json folder
        public string ShareDownloadJsonFile()
        {

            String path = string.Concat(SystemApplicationDataStorage, "\\download.json");

            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
            }
            return path;

        }
        #endregion

        /// <summary>
        ///  download folder for araizen share 
        ///  from where merging will occur
        /// </summary>
        /// <returns></returns>
        #region Share log Folder
        public string ShareDownloadFolder()
        {


            String path = string.Concat(ShareOperationsFolder(), "\\download");
            bool exists = Directory.Exists(path);
            if (!exists)
            {
                Directory.CreateDirectory(path);
            }
            return path;

        }
        #endregion


        /// <summary>
        /// share upload is where the file is
        /// </summary>
        /// <returns></returns>
        #region Share upload json file
        public string ShareUploadJsonFile()
        {

            String path = string.Concat(SystemApplicationDataStorage, "\\upload.json");

            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
            }
            return path;

        }
        #endregion

        /// <summary>
        /// share upload is where the file is
        /// </summary>
        /// <returns></returns>
        #region Share upload folder Folder
        public string ShareUploadFolder()
        {

            String path = string.Concat(ShareOperationsFolder(), "\\upload");
            bool exists = Directory.Exists(path);
            if (!exists)
            {
                Directory.CreateDirectory(path);
            }
            return path;

        }
        #endregion

        /// <summary>
        /// path to user log files
        /// last log in ,dowload activity in progress when logged out
        /// </summary>
        /// <returns></returns>
        #region Share user log file
        public string ShareUserLogFile()
        {


            String path = string.Concat(ShareOperationsFolder(), "\\Logs\\log_user.json");
            bool exists = Directory.Exists(path);
            if (!exists)
            {
                Directory.CreateDirectory(path);
            }
            return path;

        }
        #endregion

        /// <summary>
        /// path to info log file
        /// </summary>
        /// <returns></returns>
        #region Share info log file 
        public string ShareInfoLogFile()
        {


            String path = string.Concat(ShareOperationsFolder(), "\\Logs\\log_info.json");
            bool exists = Directory.Exists(path);
            if (!exists)
            {
                Directory.CreateDirectory(path);
            }
            return path;

        }
        #endregion

        /// <summary>
        /// register the crushes and the number of times they occur
        /// used in the exceptions method
        /// </summary>
        /// <returns></returns>
        #region Share Crush Log file 
        public string ShareCrushLogFile()
        {


            String path = string.Concat(ShareOperationsFolder(), "\\Logs\\log_crush.json");
            bool exists = Directory.Exists(path);
            if (!exists)
            {
                Directory.CreateDirectory(path);
            }
            return path;

        }
        #endregion


        /// <summary>
        /// path to video thumbanails
        /// </summary>
        /// <returns> path to folder</returns>
        #region path to video thumbnails 
        public string Path_to_thumbanils()
        {

            string path = string.Concat(SystemApplicationDataStorage, @"/Thumbanils");
            Directory.CreateDirectory(path);
            return path;

        }
        #endregion


        /// <summary>
        /// path to update json file
        /// store the download file path,new version number ,new update
        /// </summary>
        /// <returns></returns>
        #region path to update details json
        public string PathToUpdateDetails()
        {

            string path = string.Concat(SystemApplicationDataStorage, @"/UpdateDetails.json");
            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
            }
            return path;

        }
        #endregion


        /// <summary>
        /// let user get notification of sent items to download
        /// </summary>
        /// <returns></returns>
        #region NotificationsSQL
        public string NotificationsSQL()
        {
            string path = string.Concat(SystemApplicationDataStorage, @"/notifications.db");
            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);

            }
            return path;

        }
        #endregion


        /// <summary>
        /// let user get notification of sent items to download
        /// </summary>
        /// <returns></returns>
        #region NotificationsSQL
        public string HistorySQL()
        {
            string path = string.Concat(SystemApplicationDataStorage, @"/history.db");
            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);

            }
            return path;

        }
        #endregion

        /// <summary>
        /// database to store download items object in database
        /// </summary>
        /// <returns></returns>
        #region DownloadSQL
        public string DownloadSQL()
        {
            string path = string.Concat(SystemApplicationDataStorage, @"/download.db");
            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
                //create table

            }
            return path;

        }
        #endregion
        /// <summary>
        /// database to store upload items object in database
        /// </summary>
        /// <returns></returns>
        #region UploadSQL
        public string UploadSQL()
        {
            string path = string.Concat(SystemApplicationDataStorage, @"/upload.db");
            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
                //create table

            }
            return path;

        }
        #endregion

        /// <summary>
        /// the password and recovery pasword are stores here
        /// </summary>
        /// <returns></returns>
        #region Auth file
        public string AuthFile()
        {
            string path = string.Concat(SystemApplicationDataStorage, @"/oa.json");
            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
            }
            return path;

        }
        #endregion

        /// <summary>
        /// store 
        /// purchase date,purchase time,licence period
        /// </summary>
        /// <returns></returns>
        #region purchase
        public string PurchaseFile()
        {
            string path = string.Concat(SystemApplicationDataStorage, @"/Pur.json");
            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
            }
            return path;

        }
        #endregion



        /// <summary>
        /// user data colleted
        ///     wake date and time
        ///     shut down time and date
        ///     number of items in Enccounter
        ///     
        /// </summary>
        /// <returns></returns>
        #region UserLogs
        public string UserLogs()
        {
            string path = string.Concat(SystemApplicationDataStorage, @"/log.json");
            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
            }
            return path;

        }
        #endregion

        /// <summary>
        /// stores the number of items enc,the names  and the datetime
        /// </summary>
        /// <returns></returns>
        #region EncCounter
        public string EncCounter()
        {
            string path = string.Concat(SystemApplicationDataStorage, @"/Counter.json");
            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
            }
            return path;

        }
        #endregion


        /// <summary>
        /// stores the register of items enc,the names  and the datetime
        /// </summary>
        /// <returns></returns>
        #region UserRegister
        public string UserRegister()
        {
            string path = string.Concat(SystemApplicationDataStorage, @"/reg.json");
            bool exist = File.Exists(path);
            if (!exist)
            {
                File.Create(path);
            }
            return path;

        }
        #endregion
    }

    /// <summary>
    /// Class containing methods to retrieve specific file system paths.
    /// </summary>
    public static class KnownFolders
    {
        private static string[] _knownFolderGuids = new string[]
        {
        "{56784854-C6CB-462B-8169-88E350ACB882}", // Contacts
        "{B4BFCC3A-DB2C-424C-B029-7FE99A87C641}", // Desktop
        "{FDD39AD0-238F-46AF-ADB4-6C85480369C7}", // Documents
        "{374DE290-123F-4565-9164-39C4925E467B}", // Downloads
        "{1777F761-68AD-4D8A-87BD-30B759FA33DD}", // Favorites
        "{BFB9D5E0-C6A9-404C-B2B2-AE6DB6AF4968}", // Links
        "{4BD8D571-6D19-48D3-BE97-422220080E43}", // Music
        "{33E28130-4E1E-4676-835A-98395C3BC3BB}", // Pictures
        "{4C5C32FF-BB9D-43B0-B5B4-2D72E54EAAA4}", // SavedGames
        "{7D1D3A04-DEBB-4115-95CF-2F29DA2920DA}", // SavedSearches
        "{18989B1D-99B5-455B-841C-AB7C74E4DDFC}", // Videos
        };

        /// <summary>
        /// Gets the current path to the specified known folder as currently configured. This does
        /// not require the folder to be existent.
        /// </summary>
        /// <param name="knownFolder">The known folder which current path will be returned.</param>
        /// <returns>The default path of the known folder.</returns>
        /// <exception cref="System.Runtime.InteropServices.ExternalException">Thrown if the path
        ///     could not be retrieved.</exception>
        public static string GetPath(KnownFolder knownFolder)
        {
            return GetPath(knownFolder, false);
        }


        /// <summary>
        /// Gets the current path to the specified known folder as currently configured. This does
        /// not require the folder to be existent.
        /// </summary>
        /// <param name="knownFolder">The known folder which current path will be returned.</param>
        /// <param name="defaultUser">Specifies if the paths of the default user (user profile
        ///     template) will be used. This requires administrative rights.</param>
        /// <returns>The default path of the known folder.</returns>
        /// <exception cref="System.Runtime.InteropServices.ExternalException">Thrown if the path
        ///     could not be retrieved.</exception>
        public static string GetPath(KnownFolder knownFolder, bool defaultUser)
        {
            return GetPath(knownFolder, KnownFolderFlags.DontVerify, defaultUser);
        }

        private static string GetPath(KnownFolder knownFolder, KnownFolderFlags flags,
            bool defaultUser)
        {
            int result = SHGetKnownFolderPath(new Guid(_knownFolderGuids[(int)knownFolder]),
                (uint)flags, new IntPtr(defaultUser ? -1 : 0), out IntPtr outPath);
            if (result >= 0)
            {
                string path = Marshal.PtrToStringUni(outPath);
                Marshal.FreeCoTaskMem(outPath);
                return path;
            }
            else
            {
                throw new ExternalException("Unable to retrieve the known folder path. It may not "
                    + "be available on this system.", result);
            }
        }

        [DllImport("Shell32.dll")]
        private static extern int SHGetKnownFolderPath(
            [MarshalAs(UnmanagedType.LPStruct)]Guid rfid, uint dwFlags, IntPtr hToken,
            out IntPtr ppszPath);

        [Flags]
        private enum KnownFolderFlags : uint
        {
            SimpleIDList = 0x00000100,
            NotParentRelative = 0x00000200,
            DefaultPath = 0x00000400,
            Init = 0x00000800,
            NoAlias = 0x00001000,
            DontUnexpand = 0x00002000,
            DontVerify = 0x00004000,
            Create = 0x00008000,
            NoAppcontainerRedirection = 0x00010000,
            AliasOnly = 0x80000000
        }
    }
    /// <summary>
    /// Standard folders registered with the system. These folders are installed with Windows Vista
    /// and later operating systems, and a computer will have only folders appropriate to it
    /// installed.
    /// </summary>
    public enum KnownFolder
    {
        Contacts,
        Desktop,
        Documents,
        Downloads,
        Favorites,
        Links,
        Music,
        Pictures,
        SavedGames,
        SavedSearches,
        Videos
    }


}
