﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Araizen_Share_Service
{
    class App_Download_Action
    {
        int numberOfFiles = 0;

        //List<App_Download> _downloadingAction;
        public void Start(List<App_Download> _downloading)
        {

            // for each upload item  start a thread
            foreach (App_Download item in _downloading)
            {

                Thread th = new Thread(() =>
                {
                    AppDownloadItemAction(item);
                });
                th.Priority = ThreadPriority.Normal;
                th.Start();

            }
        }

        public void DownloadItem(App_Download _downloading)
        {
            Thread th = new Thread(() =>
            {
                AppDownloadItemAction(_downloading);
            });
            th.Priority = ThreadPriority.Normal;
            th.Start();

        }

        #region AppDownloadItemAction
        private void AppDownloadItemAction(App_Download _downloading)
        {
           
            //get system paths
            App_System_Paths paths = new App_System_Paths();

            //download sql
            App_Download_Sql downloadSql = new App_Download_Sql();

            //combine the applictions download folder and the file name
            //create the directory  if does not exist 
            String pathToItemDownloadsFolder = Path.Combine(paths.ShareDownloadFolder(), _downloading.FileName);
            bool exists = Directory.Exists(pathToItemDownloadsFolder);
            if (!exists)
            {
                Directory.CreateDirectory(pathToItemDownloadsFolder);
            }


            long totalFileSize = Convert.ToInt32(_downloading.FileSize);
             numberOfFiles = (int)Math.Ceiling((double)totalFileSize / _downloading.FileEachItemSize);

          //   Console.WriteLine($"\n\n\n \\\\\\--------- file sizer {totalFileSize} each size {_downloading.FileEachItemSize }- number of files is  {numberOfFiles}    calculated from database number of files {_downloading.TotalNumberOfFiles} --------------------------///// \n\n\n\n");

            int percentage;
            int startingDownloadNumber;

            //if downloading is started in db and the file name folder is started 
            if (_downloading.IsStarted )
            {

                startingDownloadNumber = _downloading.CurrentDownloadItem;
                percentage = (startingDownloadNumber == 0 ? 1 : startingDownloadNumber) * 100 / numberOfFiles;

            }
            else
            {
              //   Console.WriteLine("download is started");

                startingDownloadNumber = 0;
                percentage = (startingDownloadNumber + 1) * 100 / numberOfFiles;

                //update upload just started
                downloadSql.UpdateDownloadFileItemsIsStartedDatabase(_downloading.FileName);

                //strted to server
                App_Download_Server downloadServer = new App_Download_Server();
                downloadServer.SetDownloadItemIsStarted(_downloading);

            }



            for (int currentDownloadingNumber = startingDownloadNumber; currentDownloadingNumber < numberOfFiles; currentDownloadingNumber++)
            {
                //download action
                DownloadAction(_downloading : _downloading, currentDownloadingNumber: currentDownloadingNumber, numberOfFiles: numberOfFiles, pathToItemDownloadsFolder : pathToItemDownloadsFolder);

            }


            //download is complete
            DownloadComplete(_downloading: _downloading, pathToItemDownloadsFolder: pathToItemDownloadsFolder);

        }
        #endregion

        #region DownloadAction
        private void DownloadAction(App_Download _downloading, int currentDownloadingNumber,int numberOfFiles,string pathToItemDownloadsFolder)
        {
            
            //download sql
            App_Download_Sql downloadSql = new App_Download_Sql();

          //   Console.WriteLine($"\n\n Number  {currentDownloadingNumber} / {numberOfFiles}  Download astion araizen service name: ");

            //download the file
            var x = App_Download_Server.Download(name: _downloading.FileName, email: _downloading.SenderEmail, datestr: _downloading.UploadDate, fileName: currentDownloadingNumber.ToString(), filePath: pathToItemDownloadsFolder);

            //update local sql
            downloadSql.UpdateDownloadFileItemsNumberDatabase(value: currentDownloadingNumber.ToString(), file_name: _downloading.FileName);

          //   Console.WriteLine("\n\n  === Updated locale database=== \n\n");

            Thread.Sleep(2500);

            App_Download_Update_Data dataDownloadData = new App_Download_Update_Data(
              itemkey: GetFileItemKey(unixTime: _downloading.UnixTimeFileItemReferenceId.ToString(), fileName: _downloading.FileName),
              progressValue: currentDownloadingNumber,
              progressPercentage: (currentDownloadingNumber * 100 / numberOfFiles).ToString()
              );

            string dataDownloadDatastr = JsonConvert.SerializeObject(dataDownloadData);
          
            //update ui
            ShareBootstrap.DownloadClientSendMessage(dataDownloadDatastr);

            Thread.Sleep(2500);

        }
        #endregion

        #region DownloadComplete
        private void DownloadComplete(App_Download _downloading,  string pathToItemDownloadsFolder)
        {
            //100% complete
            UpdateCompleteUi(_downloading, numberOfFiles);

            //download sql
            App_Download_Sql downloadSql = new App_Download_Sql();
            
          //   Console.WriteLine("update is complted unix {0}", _downloading.UnixTimeFileItemReferenceId.ToString());
            downloadSql.UpdateDownloadFileItemsIsCompletedDatabase(_downloading.FileName);


            //update server its completed
            App_Download_Server downloadServer = new App_Download_Server();
            downloadServer.SetDownloadItemIsComlplete(_downloading);

            //save to history
            App_Auth_File authFile = new App_Auth_File();
            App_Auth authData = authFile.GetAuthDetailsFile();
            App_History historyItem = new App_History(
                action: "Download",
                fileName: _downloading.FileName,
                completionDate: DateTime.Now,
                fileSize: _downloading.FileSize.ToString(),
                fileType: _downloading.FileType,
                unixTimeFileItemReferenceId: _downloading.UnixTimeFileItemReferenceId.ToString() ?? "download",
                sourceEmail: _downloading.SenderEmail,
                destinationEmail: authData.Email
             );

            App_History_Sql historySql = new App_History_Sql();
            historySql.InsertHistoryItemsDatabase(historyItem: historyItem);

            //paths
            App_System_Paths paths = new App_System_Paths();
            string destFileForMerging = System.IO.Path.Combine(pathToItemDownloadsFolder, _downloading.FileName + _downloading.FileType);
            string destFileForUserAfterMerging = Path.Combine(paths.ShareFolder(), _downloading.FileName + _downloading.FileType);

            Thread.Sleep(2500);
            //merge the different files
            App_Operations_File.CombineMultipleFilesIntoSingleFile(pathToItemDownloadsFolder, destFileForMerging);
           
            Thread.Sleep(2500);

            //check if file exist then delete it
            if (File.Exists(destFileForUserAfterMerging))
            {
                File.Delete(destFileForUserAfterMerging);
            }

            Thread.Sleep(2500);
            //move item to documents share folver
            File.Move(sourceFileName: destFileForMerging, destFileName: destFileForUserAfterMerging);


            //delete the original directory
            // If directory does not exist, don't even try   
            if (Directory.Exists(pathToItemDownloadsFolder))
            {
                Directory.Delete(pathToItemDownloadsFolder,true);
            }

            string messageBody = $"{_downloading.FileName} has been downloaded its in {paths.ShareFolder()} ";
            //alert user download complete
            App_System_Notification.ShowCustomMessage("Download Complete",messageBody );

        }
        #endregion

        #region UpdateCompleteUi
        private void UpdateCompleteUi(App_Download _downloading, int numberOfFiles)
        {
            App_Download_Update_Data dataDownloadData = new App_Download_Update_Data(
             itemkey: GetFileItemKey(unixTime: _downloading.UnixTimeFileItemReferenceId.ToString(), fileName: _downloading.FileName),
             progressValue: numberOfFiles,
             progressPercentage: (numberOfFiles * 100 / numberOfFiles).ToString()
             );

            string dataDownloadDatastr = JsonConvert.SerializeObject(dataDownloadData);

            //update ui
            ShareBootstrap.DownloadClientSendMessage(dataDownloadDatastr);
        }
        #endregion


        #region GetFileItemKey
        string GetFileItemKey(string unixTime, string fileName)
        {
            string originalName = string.Concat(fileName, unixTime);

            //valid name charsToRemoveForValidName
            string filterSpecialCharacters = new string((from c in originalName
                                                         where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                                         select c
          ).ToArray());


            string filterSpacesAndSpecialCharacters = filterSpecialCharacters.Replace(" ", string.Empty);

            return filterSpacesAndSpecialCharacters;

        }
        #endregion
    }
}

