﻿using NamedPipeWrapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Araizen_Share_Service
{
    class ShareBootstrap
    {

     public   void Start()
        {


            //System.Net.NetworkInformation.NetworkChange.NetworkAvailabilityChanged +=
            // new NetworkAvailabilityChangedEventHandler(OnNetworkAvailabilityChanged);
            //wait for file to be created in disk
            Thread.Sleep(2500);
            //slow io
            //have to separate creating files and creating tables
            CreateDatabaseTablesIfNecessary();


            //start comms with ui
            StartCommunicationServersThenUploadingAndDownloadingUserData();
           
           

           // Console.WriteLine("Listening for network availability changes. Press any key to exit.");

            


        }
         void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            if (e.IsAvailable)
            {
              //   Console.WriteLine("Network has become available");

                //slow io
                //have to separate creating files and creating tables
                CreateDatabaseTablesIfNecessary();


                //start comms with ui
                StartCommunicationServersThenUploadingAndDownloadingUserData();
               
            }
            else
            {
              //   Console.WriteLine("Network has become unavailable");
            }
        }

        static string pathToNotificationsDb = string.Empty;
        static string pathToUploadDb = string.Empty;
        static string pathToDownloadDb = string.Empty;
        static string pathToHistoryDb = string.Empty;

       
        #region CreateDatabaseTablesIfNecessary
        private static void CreateDatabaseTablesIfNecessary()
        {
            //create databases if not exist
            App_System_Paths paths = new App_System_Paths();

            Thread.Sleep(2500);
            try
            {

                //downloading db
                App_Download_Sql sql_downloads = new App_Download_Sql();
                sql_downloads.CreateDownloadItemsDatabase(path: paths.DownloadSQL());

                //uploading db
                App_Upload_Sql sql_upload = new App_Upload_Sql();
                sql_upload.CreateUploadItemsDatabase(paths: paths.UploadSQL());


                //history 
                App_History_Sql sql_history = new App_History_Sql();
                sql_history.CreateHistoryItemsDatabase(path: paths.UploadSQL());

                //notifications db
                //create table
                App_Notification_Sql sqlQuery = new App_Notification_Sql();
                sqlQuery.CreateNotificationItemsDatabase(path: paths.NotificationsSQL());

            }catch(Exception ex)
            {
              //   Console.WriteLine("erorr creating table in service \n {0}", ex);
            }

        }
        #endregion


        #region StartUploadingUserData
        private static void StartUploadingUserData()
        {
            App_Upload_Sql upload_sql = new App_Upload_Sql();
            List<App_Upload> uploadItems = upload_sql.GetAllUploadItemsDatabase();
            App_Upload_Action uploadAcion = new App_Upload_Action();
            uploadAcion.Start(uploadItems);
        }
        #endregion

        #region StartDownloadingUserData
        private static void StartDownloadingUserData()
        {
            App_Download_Sql download_sql = new App_Download_Sql();
            List<App_Download> downloadItems = download_sql.GetDownloadItemsDatabase();
            App_Download_Action downloadAction = new App_Download_Action();
            downloadAction.Start(downloadItems);
        }
        #endregion

        #region Stop service
        public void Stop()
        {

        }
        #endregion
        #region pause service
        public void Pause()
        {

        }
        #endregion
        #region resume service
        public void Resume()
        {

        }
        #endregion
        #region  shutdown service
        public void Shutdown()
        {

        }
        #endregion
        #region before install service
        public void BeforeInstall()
        {

        }
        #endregion
        #region after install services
        public void AfterInstall()
        {

        }
        #endregion
        #region before un install service
        public void BeforeUninstall()
        {

        }
        #endregion


        #region KeepRunning both upload and download servers
        private static bool KeepRunning
        {
            get
            {
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.Q)
                    return false;
                return true;
            }
        }
        #endregion

        /// <summary>
        ///  StartCommunicationServers
        /// </summary>
        private static NamedPipeServer<string> server1 = new NamedPipeServer<string>(App_Const.PipeNameUpload);
        private static NamedPipeServer<string> server2 = new NamedPipeServer<string>(App_Const.PipeNameDownload);

        #region StartCommunicationServersThenUploadingAndDownloadingUserData
        private static void StartCommunicationServersThenUploadingAndDownloadingUserData()
        {

          //   Console.WriteLine("\n ==> starting upload coms server \n");
            //string pipeNameUpload = App_Const.PipeNameUpload;
            //var server1 = new NamedPipeServer<string>(pipeNameUpload);
            server1.ClientConnected += OnUploadClientConnected;
            server1.ClientDisconnected += OnUploadClientDisconnected;
            server1.ClientMessage += OnUploadClientMessage;
            server1.Error += OnUploadClientError;
            
            server1.Start();

          //   Console.WriteLine("\n ==> starting Download coms server \n");
            //string pipeNameDownload = App_Const.PipeNameDownload;
            //var server2 = new NamedPipeServer<string>(pipeNameDownload);
            server2.ClientConnected += OnDownloadClientConnected;
            server2.ClientDisconnected += OnDownloadClientDisconnected;
            server2.ClientMessage += OnDownloadClientMessage;
            server2.Error += OnDownloadClientError;
            server2.Start();
       
            //then start other services 
            //start uploading stuff
            StartUploadingUserData();
            //start downloading stuff
            StartDownloadingUserData();


            while (KeepRunning)
            {
                // Do nothing - wait for user to press 'q' key
              //   Console.WriteLine("keep server running upload & download communication");
            }
            server1.Stop();
            server2.Stop();

        }
        #endregion

        #region OnUploadClientConnected
        private static void OnUploadClientConnected(NamedPipeConnection<string, string> connection)
        {
          //   Console.WriteLine("\n\n Upload communication Client {0} is now connected! \n\n", connection.Id);
            // connection.PushMessage("send a message ");
        }
        #endregion

        #region OnUploadClientDisconnected
        private static void OnUploadClientDisconnected(NamedPipeConnection<string, string> connection)
        {
          //   Console.WriteLine("\n\n Client {0} disconnected", connection.Id);
        }
        #endregion

        #region OnUploadClientMessage
        private static void OnUploadClientMessage(NamedPipeConnection<string, string> connection, string message)
        {
          //   Console.WriteLine("\n\nUpload Client {0} says: {1}", connection.Id, message);
            App_Upload uploadData = JsonConvert.DeserializeObject<App_Upload>(message);


            App_Upload_Action action = new App_Upload_Action();
            action.UploadItem(uploadData);

        }
        #endregion

        #region OnUploadClientError
        private static void OnUploadClientError(Exception exception)
        {
            Console.Error.WriteLine("ERROR: {0}", exception);
        }
        #endregion

        #region OnDownloadClientConnected
        private static void OnDownloadClientConnected(NamedPipeConnection<string, string> connection)
        {
          //   Console.WriteLine("\n\n App Download Client {0} is now connected!", connection.Id);
            //  connection.PushMessage("send a message ");
        }
        #endregion

        #region OnDownloadClientDisconnected
        private static void OnDownloadClientDisconnected(NamedPipeConnection<string, string> connection)
        {
          //   Console.WriteLine("Client {0} disconnected", connection.Id);
        }
        #endregion

        #region OnDownloadClientMessage
        private static void OnDownloadClientMessage(NamedPipeConnection<string, string> connection, string message)
        {
          //   Console.WriteLine("Download Client {0} says: {1}", connection.Id, message);
            
            App_Download uploadData = JsonConvert.DeserializeObject<App_Download>(message);


            App_Download_Action action = new App_Download_Action();
            action.DownloadItem(uploadData);

        }
        #endregion

        #region send message upload
        public static void UploadClientSendMessage(string message)
        {
            server1.PushMessage(message);
        }
        #endregion

        #region send message Download
        public static void DownloadClientSendMessage(string message)
        {
            server2.PushMessage(message);
        }
        #endregion

        #region OnDownloadClientError
        private static void OnDownloadClientError(Exception exception)
        {
            Console.Error.WriteLine("ERROR: {0}", exception);
        }
        #endregion



    }
}
