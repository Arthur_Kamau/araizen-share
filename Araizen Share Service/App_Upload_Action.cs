﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Araizen_Share_Service
{
    class App_Upload_Action
    {
        //List<App_Upload> _uploadingAction;
        App_Upload_Sql uploadSql = new App_Upload_Sql();

        public void Start(List<App_Upload> _uploading)
        {
            // for each upload item  start a thread
            foreach (App_Upload item in _uploading)
            {

                Thread th = new Thread(() =>
                 {
                     AppUploadItem(item);
                 });
                th.Priority = ThreadPriority.Normal;
                th.Start();
              
            }
        }

        public  void UploadItem(App_Upload _uploading)
        {
            Thread th = new Thread(() =>
            {
                AppUploadItem(_uploading);
            });
            th.Priority = ThreadPriority.Normal;
            th.Start();

        }

        

        private  void AppUploadItem( App_Upload itemUpload)
        {
          
            
            App_System_Paths paths = new App_System_Paths();
            string shareDownloads = paths.ShareUploadFolder();

            App_Upload_Server uploadServer = new App_Upload_Server();

            App_Auth_File authFile = new App_Auth_File();
            App_Auth authData = authFile.GetAuthDetailsFile();
            

            List<App_Upload_File_Item> itemFileUploadsList = itemUpload.FileItem;
            

            DateTime dateTimeToday = DateTime.Now;
            //get today convert to string and replace / with : 
            //to ease in file creation
           
            string dateToday = dateTimeToday.Date.ToString().Replace("/","-");
            int numberOfFiles = 0;
            for (int i = 0; i < itemFileUploadsList.Count; i++)
            {
                //NOT COMPLETED
                if (itemFileUploadsList[i].IsCompleted)
                {
                  //   Console.WriteLine("completed");
                    break;
                }


                string fileSizestr = itemFileUploadsList[i].FileSize.ToString();

                long totalFileSize = Convert.ToInt32(fileSizestr);

                numberOfFiles = (int)Math.Floor((double)totalFileSize / itemFileUploadsList[i].FileEachItemSize);
              //   Console.WriteLine("\n\n total number of files {0}", numberOfFiles);

                int percentage;

                int startingUploadNumber;

                //if started uploading 
                //get from database 
                //else default is 0
                if (itemFileUploadsList[i].IsStarted)
                {
                    startingUploadNumber = itemFileUploadsList[i].CurrentUploadItem;
                    percentage = (startingUploadNumber == 0 ? 1 : startingUploadNumber) * 100 / numberOfFiles;

                }
                else
                {
                    startingUploadNumber = 0;
                    percentage = (startingUploadNumber + 1)* 100 / numberOfFiles;

                  //   Console.WriteLine("update is started");
                    uploadSql.UpdateUploadFileItemsIsStartedDatabase(itemFileUploadsList[i].FileName);

                    //upload server
                    //item is started to upload
                    List<App_Upload_File_Item> uploadsFileToServerStarted = new List<App_Upload_File_Item>();
                    uploadsFileToServerStarted.Add(itemFileUploadsList[i]);

                    App_Upload updateServereTheUploadItemAndUploadFileItem = new App_Upload(

                         sourceEmail: itemUpload.SourceClientsEmail,
                         sourceName: itemUpload.SourceClientsName,
                        destinationemails: itemUpload.DestinationEmailClients,
                         description: itemUpload.Description,
                         fileMachineUpload: itemUpload.FileMachineUpload,
                         uploadTime: itemUpload.UploadDate,
                         unixFileTimeReferenceId: itemUpload.UnixTimeFileItemReferenceId,
                         fileItem: uploadsFileToServerStarted
                        );

                  
                    uploadServer.SetuploadItemIsStarted(updateServereTheUploadItemAndUploadFileItem);


                }

              //   Console.WriteLine("Entering for loop current number  in starting number {0} and total of {1}", startingUploadNumber, numberOfFiles);
                //bcs of zero index we add 1
                for (int currentUploadingNumber = startingUploadNumber; currentUploadingNumber < (numberOfFiles+1); currentUploadingNumber++)
                {
                  //   Console.WriteLine(" in for loop current number {0}  in starting number {1} and total of {2}", currentUploadingNumber, startingUploadNumber, numberOfFiles);


                    string currentUploadingNumberString = currentUploadingNumber.ToString();
                    string myFileUploadItem = System.IO.Path.Combine(shareDownloads, itemFileUploadsList[i].FileName, i.ToString());

                  //   Console.WriteLine("\n\n Upload astion araizen service name: {0},  email: {1},  date: {2},  fileName: {3} ,  filePath: {4}", itemFileUploadsList[i].FileName, authData.Email, itemFileUploadsList[i].UploadDate.ToString(), currentUploadingNumberString, myFileUploadItem);

                    //upload the file
                    var x =  App_Upload_Server.Upload(name: itemFileUploadsList[i].FileName, email: authData.Email, olddate: itemFileUploadsList[i].UploadDate.ToString(), fileName: currentUploadingNumberString,filePath: myFileUploadItem);
                   
                    ////todo
                    ////optimize this
                    App_Upload_Update_Data dataUpload = new App_Upload_Update_Data(
                        itemkey: GetFileItemKey(unixTime: itemFileUploadsList[i].UnixTimeFileItemReferenceId.ToString(), fileName: itemFileUploadsList[i].FileName),
                        progressValue: currentUploadingNumber,
                        progressPercentage: (currentUploadingNumber * 100 / numberOfFiles).ToString() 
                        );
                    string dataUploadstr = JsonConvert.SerializeObject(dataUpload);
                    //update ui

                    ShareBootstrap.UploadClientSendMessage(dataUploadstr);

                  //update db
                    uploadSql.UpdateUploadFileItemsNumberDatabase(currentUploadingNumber.ToString(), itemFileUploadsList[i].FileName);
                    Thread.Sleep(2500);

                   
                    //update completed
                    if (currentUploadingNumber == numberOfFiles)
                    {
                      //   Console.WriteLine("update is complted unix {0}", itemFileUploadsList[i].UnixTimeFileItemReferenceId.ToString());
                        //uploadSql.UpdateUploadFileItemsIsCompletedDatabase(itemFileUploadsList[i].FileName);

                        List<App_Upload_File_Item> uploadsFileToServerComplete = new List<App_Upload_File_Item>();
                        uploadsFileToServerComplete.Add(itemFileUploadsList[i]);

                        App_Upload updateServereTheUploadItemAndUploadFileItem = new App_Upload(

                             sourceEmail : itemUpload.SourceClientsEmail, 
                             sourceName : itemUpload.SourceClientsName, 
                            destinationemails : itemUpload.DestinationEmailClients,
                             description : itemUpload.Description, 
                             fileMachineUpload : itemUpload.FileMachineUpload,
                             uploadTime : itemUpload.UploadDate, 
                             unixFileTimeReferenceId : itemUpload.UnixTimeFileItemReferenceId, 
                             fileItem : uploadsFileToServerComplete
                            );
                        
                        uploadServer.SetuploadItemIsComlplete(updateServereTheUploadItemAndUploadFileItem);


                        //delet the files
                        //save to history
                        App_History historyItem = new App_History(
                            action: "upload",
                            fileName: itemFileUploadsList[i].FileName,
                            completionDate: DateTime.Now,
                            fileSize: itemFileUploadsList[i].FileSize.ToString(),
                            fileType: itemFileUploadsList[i].FileExt,
                            unixTimeFileItemReferenceId: itemFileUploadsList[i].UnixTimeFileItemReferenceId.ToString() ?? "unix",
                            sourceEmail: authData.Email,
                            destinationEmail: String.Join(",", itemUpload.DestinationEmailClients)
                         );
                        App_History_Sql historySql = new App_History_Sql();
                        historySql.InsertHistoryItemsDatabase(historyItem: historyItem);

                        //delete from upload db 
                        App_Upload_Sql upload_sql = new App_Upload_Sql();
                        upload_sql.DeleteUploadItemsDatabase(UnixTimeFileItemReferenceId: itemFileUploadsList[i].UnixTimeFileItemReferenceId.ToString());
                        upload_sql.DeleteFileItemsDatabase(UnixTimeFileItemReferenceId: itemFileUploadsList[i].UnixTimeFileItemReferenceId.ToString());

                        //delete the folder and subfolder
                        App_System_Paths ally_paths = new App_System_Paths();
                        string deleteFilePath = System.IO.Path.Combine(ally_paths.ShareUploadFolder(), itemFileUploadsList[i].FileName);
                        try
                        {
                            System.IO.Directory.Delete(deleteFilePath, true);
                        }
                        catch
                        {
                          //   Console.WriteLine("\n caught err file not found");
                        }

                        //alert user file succefuly shared
                        App_System_Notification.ShowCustomMessage("Download Complete", $"items to {itemUpload.DestinationEmailClients} have been successfuly sent");
                    }



                }

               
            }
         
         
            

        }
   


        string GetFileItemKey(string unixTime, string fileName)
        {
            string originalName = string.Concat(fileName, unixTime);

            //valid name charsToRemoveForValidName
            string filterSpecialCharacters = new string((from c in originalName
                                                         where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                                         select c
          ).ToArray());


            string filterSpacesAndSpecialCharacters = filterSpecialCharacters.Replace(" ", string.Empty);

            return filterSpacesAndSpecialCharacters;

        }

    }
}
