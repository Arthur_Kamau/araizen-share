﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share_Service
{
    class App_Url
    {
        /// <summary>
        /// Register path
        /// </summary>
        /// <returns></returns>
        #region
        public static string RegisterDetailsUrl()
        {
            return "https://www.araizen.com/share/comms/v2/user_register_details";
        }
        #endregion

        /// <summary>
        /// computer install details path
        /// </summary>
        /// <returns></returns>
        #region
        public static string ComputerDetailsUrl()
        {
            return "https://www.araizen.com/share/comms/v2/user_computer_details";
        }
        #endregion


        /// <summary>
        /// feedback details path
        /// </summary>
        /// <returns></returns>
        #region
        public static string FeedbacksUrl()
        {
             //return "https://www.araizen.com/share/comms/v2/feedback";


            return    "http://127.0.0.1:12000/share/feedback";
        }
        #endregion



        /// <summary>
        /// feedback details path
        /// </summary>
        /// <returns></returns>
        #region
        public static string PurchaaseDetailsUrl()
        {
            return "https://www.araizen.com/share/comms/v2/get_purchase_details";
        }
        #endregion




        /// <summary>
        /// confrim purchase details details path
        /// </summary>
        /// <returns></returns>
        #region
        public static string ConfirmPurchaseDetailsUrl()
        {
            return "https://www.araizen.com/share/comms/v2/confirm_purchase_details";
        }
        #endregion



        /// <summary>
        ///check new version path
        /// </summary>
        /// <returns></returns>
        #region
        public static string CheckNewVersionUrl()
        {
            return "https://www.araizen.com/share/comms/v2/new_version";
        }
        #endregion



       
        /// <summary>
        /// forgot password details details path
        /// </summary>
        /// <returns></returns>
        #region
        public static string ForgotPasswrdUrl()
        {
            //return "https://www.araizen.com/share/comms/v2/forgot_password";
            return "http://127.0.0.1:12000/share/forgot/password/email";
        }
        #endregion


        /// <summary>
        /// forgot password confirm code details path
        /// </summary>
        /// <returns></returns>
        #region
        public static string ForgotPasswrdConfirmCodeUrl()
        {
            // return "https://www.araizen.com/share/comms/v2/forgot_password_serial_confirm";

            return "http://127.0.0.1:12000/share/forgot/password/confirm_code";

        }
        #endregion

        /// <summary>
        /// forgot password confirm code details path
        /// </summary>
        /// <returns></returns>
        #region
        public static string ForgotPasswrdChangePasswordUrl()
        {
            // return "https://www.araizen.com/share/comms/v2/forgot_password_serial_confirm";

            return "http://127.0.0.1:12000/share/forgot/password/update";

        }
        #endregion
        


        /// <summary>
        /// previous_account_details path
        /// </summary>
        /// <returns></returns>
        #region
        public static string PreviousAccountDetailsUrl()
        {
            return "https://www.araizen.com/share/comms/v2/previous_account";
        }
        #endregion



        /// <summary>
        /// update_email path
        /// </summary>
        /// <returns></returns>
        #region
        public static string UpdateEmailUrl()
        {
            return "https://www.araizen.com/share/comms/v2/update_email";
        }
        #endregion



        /// <summary>
        /// block_application path
        /// </summary>
        /// <returns></returns>
        #region
        public static string BlockApplicationUrl()
        {
            return "https://www.araizen.com/share/comms/v2/block_application";
        }
        #endregion


        /// <summary>
        /// un block_application path
        /// </summary>
        /// <returns></returns>
        #region
        public static string UnBlockApplicationUrl()
        {
            return "https://www.araizen.com/share/comms/v2/unblock_application";
        }
        #endregion



        /// <summary>
        /// is app genuinbe path
        /// </summary>
        /// <returns></returns>
        #region
        public static string IsAppGenuineUrl()
        {
            return "https://www.araizen.com/share/comms/v2/is_app_genuine";
        }
        #endregion



        /// <summary>
        /// set purchase details to server
        /// </summary>
        /// <returns></returns>
        #region 
        public static string SetPurchaseDetailsServerUrl()
        {
            return "https://www.araizen.com/share/comms/v2/set_purchase_details";
        }
        #endregion

        /// <summary>
        /// get purchase details to server
        /// </summary>
        /// <returns></returns>
        #region 
        public static string GetPurchaseDetailsServerUrl()
        {
            return "https://www.araizen.com/share/comms/v2/get_purchase_details";
        }
        #endregion

        /// <summary>
        /// set purchase details to server
        /// </summary>
        /// <returns></returns>
        #region 
        public static string GetAuthDetailsServerUrl()
        {
            return "https://www.araizen.com/share/comms/v2/Get_auth_details";
        }
        #endregion


        /// <summary>
        /// set user  log details to server
        /// </summary>
        /// <returns></returns>
        #region 
        public static string SetUserLogServerUrl()
        {
            return "https://www.araizen.com/share/comms/v2/set_purchase_details";
        }
        #endregion

        /// <summary>
        /// set  info log details to server
        /// </summary>
        /// <returns></returns>
        #region 
        public static string SetInfoLogServerUrl()
        {
            return "https://www.araizen.com/share/comms/v2/set_purchase_details";
        }
        #endregion

        /// <summary>
        /// set  crush log details to server
        /// </summary>
        /// <returns></returns>
        #region 
        public static string SetCrushLogServerUrl()
        {
            return "https://www.araizen.com/share/comms/v2/set_purchase_details";
        }
        #endregion



        /// <summary>
        /// set  crush log details to server
        /// </summary>
        /// <returns></returns>
        #region 
        public static string GetAppInstallDetailsServerUrl()
        {
            return "https://www.araizen.com/share/comms/v2/set_purchase_details";
        }
        #endregion



        /// <summary>
        /// set install details to server
        /// </summary>
        /// <returns></returns>
        #region 
        public static string SetAppInstallDetailsServerUrl()
        {
            return "https://www.araizen.com/share/comms/v2/set_purchase_details";
        }
        #endregion





        /// <summary>
        /// set  login details
        /// </summary>
        /// <returns></returns>
        #region login
        public static string AppLoginServerUrl()
        {
            // return "https://www.araizen.com/share/login";
            return "http://127.0.0.1:12000/share/login";
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        #region 
        public static string GetAppRegisterDetailsServerUrl()
        {
            // return "https://www.araizen.com/share/comms/v2/set_purchase_details";
            return "http://127.0.0.1:12000/share/get/register";
        }
        #endregion



        /// <summary>
        /// set install details to server
        /// </summary>
        /// <returns></returns>
        #region 
        public static string SetAppRegisterDetailsServerUrl()
        {
            // return "https://www.araizen.com/share/comms/v2/set_purchase_details";
            return "http://127.0.0.1:12000/share/set/register";
        }
        #endregion


        /// <summary>
        /// change user email in server 
        /// </summary>
        /// <returns></returns>
        #region 
        public static string ChangeUserEmail()
        {
            // return "https://www.araizen.com/share/comms/v2/set_purchase_details";
            return "http://127.0.0.1:12000/share/set/register";
        }
        #endregion


        /// <summary>
        /// change user email in server 
        /// </summary>
        /// <returns></returns>
        #region 
        public static string ConfirmUserPassword()
        {
            return "http://127.0.0.1:12000/share/password/confirm";
        }
        #endregion



        /// <summary>
        /// change user email in server 
        /// </summary>
        /// <returns></returns>
        #region 
        public static string GetAppDownloadFromServer()
        {
            return "http://127.0.0.1:12000/share/download/data/";
        }
        #endregion
        

       
        /// <summary>
        /// change user email in server 
        /// </summary>
        /// <returns></returns>
        #region 
        public static string NotificationItemWebSocketConnectionURL()
        {
            return "ws://dragonsnest.far/Laputa";
        }
        #endregion



        #region UploadsUrlAddEmailDateFileName
        public static string UploadsUrlAddEmailDateFileName()
        {
            return "http://127.0.0.1:12000/share/upload/data/";
        }
        #endregion

        #region DownloadUrlAddEmailDateFileName
        public static string DownloadUrlAddEmailDateFileName()
        {
            return "http://127.0.0.1:12000/share/download/data/";
        }
        #endregion


        #region UpdateDownloadItemIsStarted
        public static string UpdateDownloadItemIsStarted()
        {
            return "http://127.0.0.1:12000/share/download/data/isStarted";
        }
        #endregion

        #region UpdateDownloadItemIsCompleted
        public static string UpdateDownloadItemIsCompleted()
        {
            return "http://127.0.0.1:12000/share/download/data/isCompleted";
        }
        #endregion

        #region UpdateUploadItemIsStarted
        public static string UpdateUploadItemIsStarted()
        {
            return "http://127.0.0.1:12000/share/upload/data/isStarted";
        }
        #endregion

        #region UpdateUploadItemIsCompleted
        public static string UpdateUploadItemIsCompleted()
        {
            return "http://127.0.0.1:12000/share/upload/data/isCompleted";
        }
        #endregion


    }
}