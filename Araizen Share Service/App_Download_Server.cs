﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share_Service
{
    class App_Download_Server
    {
        //set is started
        #region SetDownloadItemIsStarted
        public void  SetDownloadItemIsStarted(App_Download downloadItem)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.UpdateDownloadItemIsStarted());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(downloadItem);

              //   Console.WriteLine("SetDownloadItemIsStarted sending {0} ", json);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
              //   Console.WriteLine("the result is {0} ", result);

               
            }
        }
        #endregion

        //set is complted
        #region SetDownloadItemIsComlplete
        public void SetDownloadItemIsComlplete(App_Download downloadItem)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.UpdateDownloadItemIsCompleted());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(downloadItem);

              //   Console.WriteLine("SetDownloadItemIsComlplete sending {0} ", json);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
              //   Console.WriteLine("the result is {0} ", result);


            }
        }
        #endregion


        #region Download file
        public static async Task<byte[]> Download( string name, string email, string datestr, string fileName, string filePath)
        {
            string baseUrl = App_Url.DownloadUrlAddEmailDateFileName();
          //   Console.WriteLine("\n\n -------------->>>>>>>in download server vfilePath {0}  <<<<<---------------- \n\n", filePath);

            string date = datestr.Replace("/", "-").Replace(":", "-").Replace(" ","-");
            using (var client = new HttpClient())
            {


              //   Console.WriteLine($"\n\n Number  URL = ==>>>>>>>> { string.Format(baseUrl + "{0}/{1}/{2}/{3}", email, date, name, fileName)}");


                var message =
                    await client.GetAsync(string.Format(baseUrl + "{0}/{1}/{2}/{3}", email, date, name, fileName)); //http://127.0.0.1:12000/share/upload/sample-fil


                Task<byte[]> result = message.Content.ReadAsByteArrayAsync();

                var resultItem = await result;

              //   Console.WriteLine("download resul length --> {0} \n item {1}", resultItem.Length, Path.Combine(filePath, fileName));
                File.WriteAllBytes(Path.Combine(filePath, fileName), await result);

                return await result;


            }

        }
        #endregion
    }
}
