﻿using System;
using System.IO;

namespace Araizen_Share_Service
{
    class App_Operations_File
    {
        # region SplitFile
        public static void SplitFile(string inputFile, int chunkSize, string path)
        {
            const int BUFFER_SIZE = 20 * 1024;
            byte[] buffer = new byte[BUFFER_SIZE];

            using (Stream input = File.OpenRead(inputFile))
            {
                int index = 0;
                while (input.Position < input.Length)
                {
                    using (Stream output = File.Create(path + "\\" + index))
                    {
                        int remaining = chunkSize, bytesRead;
                        while (remaining > 0 && (bytesRead = input.Read(buffer, 0,
                                Math.Min(remaining, BUFFER_SIZE))) > 0)
                        {
                            output.Write(buffer, 0, bytesRead);
                            remaining -= bytesRead;
                        }
                    }
                    index++;
                   // Thread.Sleep(500); // experimental; perhaps try it
                }
            }
        }
        #endregion

        //merge
        #region merge
        public static void CombineMultipleFilesIntoSingleFile(string inputDirectoryPath, string outputFilePath)
        {
            string[] inputFilePaths = Directory.GetFiles(inputDirectoryPath);
          //   Console.WriteLine("Number of files: {0}.", inputFilePaths.Length);
            using (var outputStream = File.Create(outputFilePath))
            {
              //  int[] myInts = Array.ConvertAll(inputFilePaths, s => int.Parse(s));

                for(int i = 0; i <inputFilePaths.Length; i ++)
                { 
                
                    using (var inputStream = File.OpenRead(inputFilePaths[i]))
                    {
                        // Buffer size can be passed as the second argument.
                        inputStream.CopyTo(outputStream);
                    }
                  //   Console.WriteLine("The file {0} has been processed.", inputFilePaths[i]);
                }
            }
        }
        #endregion
    }
}
