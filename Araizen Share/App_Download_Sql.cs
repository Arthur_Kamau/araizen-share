﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace Araizen_Share
{
    class App_Download_Sql
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        #region CreateDownloadItemsDatabase
        public void CreateDownloadItemsDatabase(string path)
        {
            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + path))
            {
                sqlConnection.Open();



                string sql_item = @"
                    CREATE TABLE IF NOT EXISTS download_items 
                    (
                      id INTEGER PRIMARY KEY AUTOINCREMENT,

                      SenderName varchar(255) ,
                      SenderEmail varchar(255) ,
                      FileName varchar(255) ,
                      FileSize varchar(255) ,
                      FileType varchar(255) ,
                      IsComplete varchar(255) ,
                      IsStarted  varchar(255),
                      FileEachItemSize varchar(255) ,
                        TotalNumberOfFiles varchar(255) ,
                      CurrentDownloadItem varchar(255) ,
                      UnixTimeFileItemReferenceId varchar(255) ,
                      FileDownloadUrl varchar(255) ,
                      UploadDate varchar(255) ,
                      FileEachItemLocation varchar(255) 

                      
                    )";

                SQLiteCommand command_item = new SQLiteCommand(sql_item, sqlConnection);
                command_item.ExecuteNonQuery();

            }

        }
        #endregion


        /*
         *******************************************************************************************
         * 
         * insert functions
         * 
         * *******************************************************************************************
         */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="downloadItem"></param>
        #region InsertdownloadItemsDatabase
        public void InsertdownloadItemsDatabase(App_Download downloadItem)
        {
            App_System_Paths paths = new App_System_Paths();
            string downloadItemPath = paths.DownloadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + downloadItemPath))
            {

                //continue to insert
                sqlConnection.Open();

                //if it exist ignore

                string sql_query_select = string.Format("SELECT count(*) FROM download_items WHERE SenderName='{0}' AND SenderEmail ='{1}'", downloadItem.SenderName, downloadItem.SenderEmail);


                SQLiteCommand command = new SQLiteCommand(sql_query_select, sqlConnection);
                command.ExecuteNonQuery();

                int count = Convert.ToInt32(command.ExecuteScalar());



                if (count == 0)
                {


                    string sqlQuery = string.Format(
               @"INSERT INTO download_items
            (
                      SenderName  ,
                      SenderEmail  ,

                    IsComplete, 
                    IsStarted,

                      FileName  ,
                      FileSize  ,

                      FileType ,
                      FileEachItemSize  ,

                      CurrentDownloadItem  ,
                      UnixTimeFileItemReferenceId  ,

                      FileDownloadUrl  ,
                      UploadDate  ,

                      FileEachItemLocation ,
TotalNumberOfFiles
            )
            VALUES
            (
                '{0}', '{1}',
                '{2}', '{3}',
                '{4}', '{5}',
                '{6}', '{7}',
                '{8}' , '{9}',
                '{10}','{11}',
                '{12}', '{13}'
            ); 
            ",


                downloadItem.SenderName, downloadItem.SenderEmail,
                 downloadItem.IsComplete, downloadItem.IsStarted,
                downloadItem.FileName, downloadItem.FileSize,
                downloadItem.FileType, downloadItem.FileEachItemSize,
                downloadItem.CurrentDownloadItem, downloadItem.UnixTimeFileItemReferenceId,
                downloadItem.FileDownloadUrl, downloadItem.UploadDate,
                downloadItem.FileEachItemLocation, downloadItem.TotalNumberOfFiles


              );



                    Console.WriteLine("ui ------> \n\n --->> {0}", sqlQuery);
                    //sqlCommand = sqlConnection.CreateCommand();
                    SQLiteCommand command2 = new SQLiteCommand(sqlQuery, sqlConnection);
                    command2.ExecuteNonQuery();
                }//end of ==0
            }
        }
        #endregion


        /*
         *******************************************************************************************
         * 
         * Get functions
         * 
         * *******************************************************************************************
         */
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        #region GetDownloadItemsDatabase
        public List<App_Download> GetDownloadItemsDatabase()
        {
            App_System_Paths paths = new App_System_Paths();
            string downloadItemPath = paths.DownloadSQL();
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=" + downloadItemPath))
            {
                conn.Open();

                SQLiteCommand command = new SQLiteCommand(@"Select

                      SenderName  ,
                      SenderEmail  ,
                        IsComplete, 
                    IsStarted,
                      FileName  ,
                      FileSize  ,
                      FileType ,
                      FileEachItemSize  ,
                      CurrentDownloadItem  ,
                      UnixTimeFileItemReferenceId  ,
                      FileDownloadUrl  ,
                      UploadDate  ,
                      FileEachItemLocation  ,
                     TotalNumberOfFiles

                       from download_items order by id", conn);
                SQLiteDataReader reader = command.ExecuteReader();

                List<App_Download> download_items = new List<App_Download>();

                while (reader.Read())
                {
                    string userSenderName = reader["SenderName"].ToString();
                    string userSenderEmail = reader["SenderEmail"].ToString();

                    string userIsComplete = reader["IsComplete"].ToString().ToLower();
                    string userIsStarted = reader["IsStarted"].ToString().ToLower();


                    string userFileName = reader["FileName"].ToString();
                    long userFileSize = Convert.ToInt32(reader["FileSize"].ToString());
                    string userFileType = reader["FileType"].ToString();
                    int usertotalNumberOfFiles = Convert.ToInt32(reader["TotalNumberOfFiles"].ToString());
                    int userFileEachItemSize = Convert.ToInt32(reader["FileEachItemSize"].ToString());
                    int userCurrentDownloadItem = Convert.ToInt32(reader["CurrentDownloadItem"].ToString());
                    long userUnixTimeFileItemReferenceId = Convert.ToInt64(reader["UnixTimeFileItemReferenceId"].ToString());
                    string userFileDownloadUrl = reader["FileDownloadUrl"].ToString();
                    string userUploadDate = reader["UploadDate"].ToString();
                    string userFileEachItemLocation = reader["FileEachItemLocation"].ToString();

                    App_Download downloadFile = new App_Download(

                         senderEmail: userSenderEmail,
                         senderName: userSenderName,

                         isStarted: userIsStarted.Equals("1") || userIsStarted.ToLower().Equals("true") ? true : false,//Convert.ToBoolean(userIsStarted),
                         isComplete: userIsComplete.Equals("1") || userIsComplete.ToLower().Equals("true") ? true : false,// Convert.ToBoolean(userIsCompleted),

                         fileName: userFileName,
                         fileSize: userFileSize,
                         fileType: userFileType,
                         totalNumberOfFiles: usertotalNumberOfFiles,
                         fileEachItemSize: userFileEachItemSize,
                         currentDownloadItem: userCurrentDownloadItem,
                         fileDownloadUrl: userFileDownloadUrl,
                         uploadDate: userUploadDate,
                         fileEachItemLocation: userFileDownloadUrl,
                         unixTimeFileItemReferenceId: userUnixTimeFileItemReferenceId

                    );

                    download_items.Add(downloadFile);
                }
                reader.Close();

                return download_items;
            }
        }
        #endregion

        /*
       *******************************************************************************************
       * 
       * Update functions
       * 
       * *******************************************************************************************
       */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file_name"></param>
        #region UpdateDownloadFileItemsIsStartedDatabase
        public void UpdateDownloadFileItemsIsStartedDatabase(string file_name)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.DownloadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();

                string sqlQuery = string.Format(

                    @"UPDATE download_items 
                    SET IsStarted = 'true' 
                    WHERE FileName = '{0}'

                    ", file_name
                    );

                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();


            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="file_name"></param>
        #region UpdateDownloadFileItemsNumberDatabase
        public void UpdateDownloadFileItemsNumberDatabase(string value, string file_name)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.DownloadSQL();

            //    sqlConnection = new SQLiteConnection("Data Source="+uploadItemPath+";Version=3;");
            //    sqlConnection.Open();
            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();


                string sqlQuery = string.Format(

                    @"UPDATE download_items 
                    SET CurrentDownloadItem = '{0}' 
                    WHERE FileName = '{1}'

                    ", value, file_name
                    );

                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();


            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file_name"></param>
        #region UpdateDownloadFileItemsIsCompletedDatabase
        public void UpdateDownloadFileItemsIsCompletedDatabase(string file_name)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.DownloadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();

                string sqlQuery = string.Format(

                    @"UPDATE download_items 
                    SET IsComplete = 'true' 
                    WHERE FileName = '{0}'

                    ", file_name
                    );

                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();


            }
        }
        #endregion

        /*
         *******************************************************************************************
         * 
         * Delete functions
         * 
         * *******************************************************************************************
         */


        /// <summary>
        /// delette item
        /// </summary>
        /// <param name="UnixTimeFileItemReferenceId"></param>
        #region DeletedownloadItemsDatabase
        public void DeletedownloadItemsDatabase(string senderEmail, string fileName, string uploadDate)
        {
            App_System_Paths paths = new App_System_Paths();
            string downloadItemPath = paths.DownloadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + downloadItemPath))
            {
                sqlConnection.Open();
                //delete item

                string sqlQuery = string.Format(@"
DELETE FROM download_items WHERE 

                      SenderEmail  = '{0}' AND
                      FileName  = '{1}' AND
                 UploadDate = '{2}'

", senderEmail, fileName, uploadDate);
                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();
            }
        }
        #endregion

        /****************************************************************
           * check for duplicates and delete if items exist check by name
           *
           *
        */
    }
}
