﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{//Define event argument you want to send while raising event.
    public class App_Upload_EventArgs : EventArgs
    {
        public string Value { get; set; }

        public App_Upload_EventArgs(string value)
        {
            Value = value;
        }
    }


    //Define publisher class as Pub
    public class App_Upload_Pub
    {
        //OnChange property containing all the 
        //list of subscribers callback methods.

        //This is generic EventHandler delegate where 
        //we define the type of argument want to send 
        //while raising our event, App_Upload_EventArgs in our case.
        public event EventHandler<App_Upload_EventArgs> OnChange = delegate { };

        public void Raise(string args = "")
        {
            //Initialize App_Upload_EventArgs object with some random value
            App_Upload_EventArgs eventArgs = new App_Upload_EventArgs(args);

            //Create list of exception
            List<Exception> exceptions = new List<Exception>();

            //Invoke OnChange Action by iterating on all subscribers event handlers
            foreach (Delegate handler in OnChange.GetInvocationList())
            {
                try
                {
                    //pass sender object and eventArgs while
                    handler.DynamicInvoke(this, eventArgs);
                }
                catch (Exception e)
                {
                    //Add exception in exception list if occured any
                    exceptions.Add(e);
                }
            }

            //Check if any exception occured while 
            //invoking the subscribers event handlers
            if (exceptions.Any())
            {
                //Throw aggregate exception of all exceptions 
                //occured while invoking subscribers event handlers
                throw new AggregateException(exceptions);
            }
        }
    }

}