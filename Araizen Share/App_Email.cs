﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Email
    {


        #region change user Email
        public App_ChangeEmail_Result ChangeUserEmail(string oldEmail,string newEmail)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.ChangeUserEmail());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                App_ChangeEmail_Request afi = new App_ChangeEmail_Request(
                    newEmail: newEmail,
                    oldEmail: oldEmail
                    );

                string json = JsonConvert.SerializeObject(afi);

                Console.WriteLine("change email");
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);

                try
                {
                    App_ChangeEmail_Result items = JsonConvert.DeserializeObject<App_ChangeEmail_Result>(result);
                    return items;
                }
                catch
                {
                    return null;
                }

            }
        }
        #endregion


    }

}

public class App_ChangeEmail_Request
    {
    public string OldEmail { get; set; }
    public string NewEmail { get; set; }

    public App_ChangeEmail_Request(string oldEmail, string newEmail)
    {
        OldEmail = oldEmail;
        NewEmail = newEmail;
    }
}
    public class App_ChangeEmail_Result
    {
        public string Reason { get; set; }
        public bool IsOkay { get; set; }

        public App_ChangeEmail_Result(string reason, bool isOkay)
        {
            Reason = reason;
            IsOkay = isOkay;
        }
    }

