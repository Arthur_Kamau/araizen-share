﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_LockedSerial.xaml
    /// </summary>
    public partial class Ui_LockedSerial : MetroWindow
    {
        Boolean killOnExitPurchase = false;
        App_Protect_String ProtectString = new App_Protect_String();


        public Ui_LockedSerial(Boolean KillOnexit)
        {

            InitializeComponent();

            if (KillOnexit)
            {
                killOnExitPurchase = true;
            }

            ConstructorStuff();

          
            accounttype.Content = "unlock a key at https://araizen.com/dashboard/unlock";
            
        }



        private void ConstructorStuff()
        {

            Submit.IsEnabled = false;

            var th = new Task(() =>
            {
                while (true)
                {
                    var IsThereInterent = App_System_Wifi.CheckForInternetConnection();
                    Dispatcher.Invoke((Action)delegate
                    {
                        if (IsThereInterent)
                        {
                            Status.Content = "There Is Internet";
                            Submit.IsEnabled = true;
                        }
                        else
                        {
                            Status.Content = " No Internet";
                            Submit.IsEnabled = false;
                        }
                    });
                    Thread.Sleep(2000);
                }
            });
            th.Start();

        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            //String PurchaseSerialstr = PurchaseSerial.Text;


            //App_System_Communication asc = new App_System_Communication();

            //bool Commsresult = asc.ConfirmPurchaseDetails(user_purchase_key: PurchaseSerialstr);

            //if (Commsresult)
            //{


            //    //true ->Right purchase key
            //    result.Content = "Successful";


            //    String Htmlresult = asc.GetPurchaseDetails(user_purchase_key: PurchaseSerialstr);
            //    if (Htmlresult.Equals("err"))
            //    {
            //        result.Content = "Unable to fetch purchase details,Try again.";
            //    }
            //    else
            //    {
            //        //4grter than 
            //        if (Htmlresult.Length > 4)
            //        {
            //            Console.WriteLine("\n\n response from server = {0}", Htmlresult);
            //            string[] items = Htmlresult.Split('#');
            //            foreach (var i in items)
            //            {
            //                Console.WriteLine("item ->{0}", i);
            //            }
            //            //get objects from the response
            //            //Purchase_data_Json m = JsonConvert.DeserializeObject<Purchase_data_Json>();
            //            Console.WriteLine("Result of split = \n purchaseDay ->{0} \n purchasekey ->{1} \n licenceperiod - {2} ",
            //                items[0], items[1], items[2]);
            //            DateTime purchaseDay = DateTime.Parse(items[0]);
            //            String PurchaseKeyFromSvr = items[1];
            //            int PeriodLicence = Convert.ToInt32(items[2]);
            //            //string appkey = items[3];

            //            App_Registry reg = new App_Registry();

            //            reg.StorePurchaseKey(PurchaseSerialstr);
            //            reg.StorePurchasePeriod(PeriodLicence.ToString());
            //            reg.StorePurchaseDate(purchaseDay);

            //            App_Resource res = new App_Resource();
            //            res.SetPurchaseDetails(key: PurchaseSerialstr, purchaseTime: purchaseDay, period: PeriodLicence);

            //            this.Close();
            //        }
            //        else
            //        {
            //            result.Content = "wrong purchase key try again";
            //        }

            //    }

            //}
            //else
            //{
            //    //wrong purchase key
            //    result.Content = "Try again,If problem persist contact Araizen Personel";

            //}
        }

        private void UpdateAraizenShareLockDetails(String Serverstr)
        {

        }

        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            if (killOnExitPurchase)
            {
                Process.GetCurrentProcess().Kill();
            }
            else
            {
                this.Close();
            }
        }
    }
}
