﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for About_app.xaml
    /// </summary>
    public partial class Ui_AboutApp : MetroWindow
    {
        public Ui_AboutApp()
        {
            InitializeComponent();
            PopulateFields();
        }

        private void PopulateFields()
        {
           String Versionstr = App_Object.GetApp_Version(); 

            Boolean HasLicence;
            App_Protect_String ProtectString = new App_Protect_String();

            App_Resource res = new App_Resource();
            App_Registry reg = new App_Registry();

            App_Auth_File authFile = new App_Auth_File();
            App_Auth authDetails = authFile.GetAuthDetailsFile();

            String LicenceData = reg.GetPurchaseKey();
            if (string.IsNullOrEmpty(LicenceData)) { HasLicence = false; } else { HasLicence = true; }

            String userName = authDetails.Name;  

           

            //build special type
            bool isSpecial = App_Object.GetApp_Build_IsSpecial();
            if (isSpecial)
            {
                BuildSpecial.Content = "Normal Build";
            }
            else
            {
                BuildSpecial.Content = "Special Build";
            }

            //prelease
            bool prelease = App_Object.GetApp_Build_isPrelease();
            if (prelease)
            {
                BuildType.Content = "Prelease Version";
            }
            else
            {
                BuildType.Content = "Stable Version";
            }





            Version.Content = Versionstr;

            RegisteredTo.Content = ProtectString.DecryptString(userName);

            if (HasLicence)
                Type.Content = "Licenced";
            else
                Type.Content = "Not Licenced";

        }

        private void Okay_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}