﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Install_Register
    {
        App_System_Paths paths = new App_System_Paths();
        App_File files = new App_File();


        #region GetApp_InstallDetailsFile
        public App_Install GetApp_InstallDetailsFile()
        {

            string json_str = files.Get_data_from_file(paths.AppInstallDetails());
            Console.WriteLine(" GetRegisterDetails --------->{0}", json_str);
            App_Install appInstall = JsonConvert.DeserializeObject<App_Install>(json_str);

            if (appInstall != null)
            {
                return appInstall;
            }
            else
            {
                DateTime now = DateTime.Now;
                string mac = App_System_Details.GetMacAddress();
                App_Install App_Install_set = new App_Install(installTime: now, macAddress: mac);

                //store in file
                SetAppInstallFile(installTimePar: now, macAddressPar: mac);
                //return new object
                return App_Install_set;
            }

        }
        #endregion
        #region SetApp install File
        public void SetAppInstallFile(DateTime installTimePar, string macAddressPar)
        {
            App_Install installapp = new App_Install(installTime: installTimePar, macAddress: macAddressPar);

            string json_str = JsonConvert.SerializeObject(installapp);
            Console.WriteLine("\n SetRegisterDetails --------->{0}", json_str);
            files.Save_data_to_file(path: paths.AppInstallDetails(), data: json_str);

        }
        #endregion

        #region GetAppInstallDetailsServer
        public App_Install GetAppInstallDetailsServer()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.GetAppInstallDetailsServerUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                App_Auth_File authFile = new App_Auth_File();
                App_Auth authData = authFile.GetAuthDetailsFile();

                string json = JsonConvert.SerializeObject(authData);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);
                if (!result.Equals("err"))
                {
                    Console.WriteLine("Erroro in app install regsiter from server");
                    //caution returning null
                    return null;
                }
                else
                {
                    //desrialize resul
                    App_Install appInstall = JsonConvert.DeserializeObject<App_Install>(result);

                    return appInstall;
                }

            }
        }
        #endregion
        #region SetAppInstallServer
        public void SetAppInstallServer(App_Install installDetails)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.SetAppInstallDetailsServerUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(installDetails);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);
            }
        }
        #endregion


        #region GetApp_InstallDetailsFile
        public App_Register GetAppRegisterDetailsFile()
        {

            string json_str = files.Get_data_from_file(paths.UserRegister());
            Console.WriteLine(" GetAppRegisterDetailsFile --------->{0}", json_str);
            App_Register appInstall = JsonConvert.DeserializeObject<App_Register>(json_str);

           
                return appInstall;
           

        }
        #endregion
        #region SetApp install File
        public void SetAppRegisterDetailsFile(App_Register regsietr)
        {
          
            string json_str = JsonConvert.SerializeObject(regsietr);
            Console.WriteLine("\n SetAppRegisterDetailsFile --------->{0}", json_str);
            files.Save_data_to_file(path: paths.UserRegister(), data: json_str);

        }
        #endregion

        #region GetAppInstallDetailsServer
        public App_Register GetAppRegisterDetailsFileServer()
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.GetAppRegisterDetailsServerUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                App_Auth_File authFile = new App_Auth_File();
                App_Auth authData = authFile.GetAuthDetailsFile();

                string json = JsonConvert.SerializeObject(authData);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);
                if (result.Equals("err"))
                {
                    Console.WriteLine("Erroro in app register regsiter from server");
                    //caution returning null
                    return null;
                }
                else
                {
                    //desrialize resul
                    App_Register appInstall = JsonConvert.DeserializeObject<App_Register>(result);

                    return appInstall;
                }

            }
        }
        #endregion
        #region SetAppInstallServer
        public App_Register_Result SetAppRegisterDetailsServer(App_Register registerDetails)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.SetAppRegisterDetailsServerUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(registerDetails);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);
                if (result.Equals("err"))
                {
                    Console.WriteLine("Erroro in app register regsiter from server");
                    //caution returning null
                    return null;
                }
                else
                {
                    App_Register_Result reg = JsonConvert.DeserializeObject<App_Register_Result>(result);

                    return reg;
                }
            }
        }
        #endregion

    }


    /// <summary>
    /// app install details auto generated when app run 1st time
    /// </summary>
    #region App_Install
    class App_Install
    {
        public App_Install() { }
        public App_Install(DateTime installTime, string macAddress)
        {
            InstallTime = installTime;
            MacAddress = macAddress;
        }

        public DateTime InstallTime { get; set; }
        public string MacAddress { get; set; }
    }
    #endregion

    /// <summary>
    /// register details dname and email 
    /// </summary>
    #region App_Register
    class App_Register
    {
        public App_Register() { }

        public App_Register(string name, string email, string macAddress,  string appVersion, DateTime registerTime, string accountType,string password)
        {
            Name = name;
            Email = email;
            MacAddress = macAddress;
            Password = password;
            AppVersion = appVersion;
            RegisterTime = registerTime;
            AccountType = accountType;
        }

        public String Name { get; set; }
        public String Email { get; set; }
        public String MacAddress { get; set; }
        public String AppVersion { get; set; }
        public String AccountType { get; set; }
        public String Password { get; set; }
        public DateTime RegisterTime { get; set; }
    }
    #endregion

    /// <summary>
    /// register details dname and email 
    /// </summary>
    #region App_Register_Result
    class App_Register_Result
    {
      public  bool isOkay;
      public  string reason;

        public App_Register_Result(bool isOkay, string reason)
        {
            this.isOkay = isOkay;
            this.reason = reason;
        }
    }
    #endregion

}
