﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Management;

namespace Araizen_Share
{
    class App_System_Details
    {
        public static string DotNetVersion()
        {
            return System.Environment.Version.ToString();
        }
        public static string OsVersion()
        {
            return user_os_version();
        }

        public static string GetComputerName()
        {
            return System.Environment.MachineName;
        }
        public static string GetHardDiskSize()
        {
            ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"c:\"");
            disk.Get();
            return disk["Size"].ToString(); //in bytes
        }
        public static string GetNetBiosName()
        {
            //Retrieve the NetBIOS name. 
            string result = System.Environment.MachineName;

            //Display the results to the console window.
            Console.WriteLine("NetBIOS Name = {0}", result);
            return result;
        }
        public static Boolean GetInternetActive()
        {
            Boolean available = NetworkInterface.GetIsNetworkAvailable();


            return available;
        }
        public static string GetMacAddress()
        {
            String mac = String.Empty;

            mac = (from nic in NetworkInterface.GetAllNetworkInterfaces() select nic.GetPhysicalAddress().ToString()).FirstOrDefault();
            return mac;
        }
        public static string GetMacAddress_1()
        {
            NetworkInterface[] nic = NetworkInterface.GetAllNetworkInterfaces();
            string smac = string.Empty;

            foreach (NetworkInterface adapter in nic)
            {
                if (smac == string.Empty)
                {
                    smac = adapter.GetPhysicalAddress().ToString();
                }
            }
            return smac;
        }
       

        //get the os version
        private static string user_os_version()
        {
            string os_version = FriendlyName();
            if (os_version.Length == 0)
            {
                return Environment.OSVersion.ToString();
            }
            else
            {
                return os_version;
            }

        }

        private static string HKLM_GetString(string path, string key)
        {
            try
            {
                RegistryKey rk = Registry.LocalMachine.OpenSubKey(path);
                if (rk == null) return "";
                return (string)rk.GetValue(key);
            }
            catch (System.Exception)
            {
                return "";
            }
        }

        private static string FriendlyName()
        {
            string ProductName = HKLM_GetString(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName");
            string CSDVersion = HKLM_GetString(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CSDVersion");

            if (ProductName != "")
            {
                return (ProductName.StartsWith("Microsoft") ? "" : "Microsoft") + ProductName +
                (CSDVersion != "" ? "" + CSDVersion : "");
            }
            return "";
        }
    }
}
