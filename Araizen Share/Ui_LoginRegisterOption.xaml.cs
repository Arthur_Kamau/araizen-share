﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for LoginRegisterOption.xaml
    /// </summary>
    public partial class Ui_LoginRegisterOption : MetroWindow
    {
        public Ui_LoginRegisterOption()
        {
            InitializeComponent();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            Ui_Login logIn = new Ui_Login();
            logIn.Show();
            this.Close();

        }

        private void Register_Click(object sender, RoutedEventArgs e)
        {
            Ui_Register reg = new Ui_Register(KillOnexit:true);
            reg.Show();
            this.Close();

        }

        private void Window_Closed(object sender, EventArgs e)
        {
          //  Process.GetCurrentProcess().Kill();
        }
    }
}
