﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Inbox_File
    {
        App_File file = new App_File();
        App_System_Paths paths = new App_System_Paths();

        #region get app  download items
        public List<App_Inbox> GetInboxItemsContents()
        {

            string json_str = file.Get_data_from_file(paths.ShareInboxJsonFile());
            Console.WriteLine(" GetDownloadingItemsContents ---------> \n {0} \n", json_str);
            List<App_Inbox> items = JsonConvert.DeserializeObject<List<App_Inbox>>(json_str);

            return items;
        }
        #endregion

        #region save app data items into json file 
        public void SaveInboxItemsContents(List<App_Inbox> data)
        {

            string json_str = JsonConvert.SerializeObject(data);
            Console.WriteLine("\n SaveDownloadingItemsContents ---------> \n {0} \n", json_str);
            file.Save_data_to_file(paths.ShareInboxJsonFile(), json_str);
        }
        #endregion

    }
}
