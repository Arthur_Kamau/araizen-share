﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    interface App_Observer
    {
        void Update(int count);
    }
}
