﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for GeneralData.xaml
    /// </summary>
    public partial class Ui_GeneralData : Page
    {
       static public event EventHandler openHistory;
        static public event EventHandler openNotifications;
        static public event EventHandler openAccount;

        App_Auth_File authFile = new App_Auth_File();
        App_Auth authdata;

        public Ui_GeneralData()
        {
            authdata = authFile.GetAuthDetailsFile();

            InitializeComponent();

          if(authdata != null)
            {
                userName.Content = authdata.Name;
            }
        }

        private void User_account_Click(object sender, RoutedEventArgs e)
        {
            if (openAccount != null)
            {
                openAccount(this, e);
            }
        }

        private void User_inbox_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow m = new MainWindow("inbox.xaml");
            //m.content_container.Source = new Uri("inbox.xaml", UriKind.RelativeOrAbsolute);
         //   attach_file m = new attach_file();
          //  m.ShowDialog();
        }

       

        private void User_history_Click(object sender, RoutedEventArgs e)
        {
            if (openHistory != null)
            {
                openHistory(this, e);
            }
           
        }

        private void User_report_bug_Click(object sender, RoutedEventArgs e)
        {
            Ui_Feedback m = new Ui_Feedback();
            m.ShowDialog();
        }



        private void User_Notifications_Click(object sender, RoutedEventArgs e)
        {
            if (openNotifications != null)
            {
                openNotifications(this, e);
            }
        }
    }
}
