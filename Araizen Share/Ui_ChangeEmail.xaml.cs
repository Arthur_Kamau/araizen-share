﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_ChangeEmail.xaml
    /// </summary>
    public partial class Ui_ChangeEmail : MetroWindow
    {
        //change auth file
        App_Auth_File authFile = new App_Auth_File();
        App_Auth authData;

        public Ui_ChangeEmail()
        {
            InitializeComponent();

            authData = authFile.GetAuthDetailsFile();

            NewMail.IsEnabled = false;
            var th = new Task(() =>
            {
                while (true)
                {
                    var IsThereInterent = App_System_Wifi.CheckForInternetConnection();
                    try
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            if (IsThereInterent)
                            {
                              //  Submit.IsEnabled = true;
                                //Internetstatus.Foreground = new SolidColorBrush(Colors.Green);
                               // Internetstatus.Content = "There Is Internet,Proceed.";
                            }
                            else
                            {
                                Submit.IsEnabled = false;
                                InternetStatus.Foreground = new SolidColorBrush(Colors.Red);
                                InternetStatus.Content = "Connect to Internet to proceed";
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("internal error");
                    }
                    Thread.Sleep(1500);
                }
            });
            th.Start();

        }

        string  newEmail = string.Empty;

        public string TheNewEmail
        {
            get { return newEmail; }
        }

       
        private void OldMail_KeyUp(object sender, KeyEventArgs e)
        {
            string oldMail = authData.Email;
            string oldMailUserInput = OldMail.Text;
            if (oldMail.Equals(oldMailUserInput))
            {
                NewMail.IsEnabled = true;
                InternetStatus.Content = "";
            }
            else
            {
                InternetStatus.Content = "Old mail does not match";
            }
        }

        private void NewMail_KeyUp(object sender, KeyEventArgs e)
        {


            bool isValid = IsValidEmail(NewMail.Text);

            if (isValid)
            {
                Submit.IsEnabled = true;
                InternetStatus.Content = "";
            }
            else
            {
                InternetStatus.Content = "New Email not a valid match";
            }

        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            //uensure retuen value equals new mail
            

            string oldMail = authData.Email;

            //first check if mail exist in server
            //if not change all the neccessary  fields then 
            //proccedd to change localy in the files
            App_Email emailFile = new App_Email();
            App_ChangeEmail_Result result = emailFile.ChangeUserEmail(

                oldEmail : oldMail, 
                newEmail : NewMail.Text
                );

           if (result != null) {

               if(result.IsOkay)
                {
                    //change auth file

                    App_Auth authData = authFile.GetAuthDetailsFile();

                    authData.Email = newEmail;

                    authFile.SetAuthDetailsFile(

                         userInstallAppDate: authData.InstallTime,
                         logInTime: Convert.ToDateTime(authData.LogInTime),
                         name: authData.Name,
                         accountType: authData.AccountType,
                         token: authData.Token,
                         email: authData.Email
                     );



                    this.Close();
                }
                else
                {
                    InternetStatus.Content = result.Reason;
                }
            }
            else
            {

                InternetStatus.Content = "Error try again later";
            }
        }
    }
}
