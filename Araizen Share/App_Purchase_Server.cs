﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Purchase_Server
    {

        #region SetPurchaseDetailsServer
        private void SetPurchaseDetailsServer(App_Purchase pur)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.SetPurchaseDetailsServerUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(pur);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);
            }
        }
        #endregion

        #region GetPurchaseDetailsServer
        public App_PurchaseDetailsContainer GetPurchaseDetailsServer(App_Auth auth)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.GetPurchaseDetailsServerUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(auth);

                Console.WriteLine($"\n ---- GetPurchaseDetailsServer {json} \n --- {App_Url.GetPurchaseDetailsServerUrl()} ");

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0} ", result);
                App_PurchaseDetailsContainer pur = JsonConvert.DeserializeObject<App_PurchaseDetailsContainer>(result);

                return pur;
            }
        }
        #endregion


        #region AuthPurchaseDetails
        public bool AuthPurchaseDetails(AuthPurchaseDetails authPurchase)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.AuthPuchaseDetails());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(authPurchase);

                Console.WriteLine($"\n ---- AuthPurchaseDetails {json} \n --- {App_Url.AuthPuchaseDetails()} "); 
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0} ", result);

                if (result.ToString().ToLower().Equals("true") || result.ToString().ToLower().Equals("ok") || result.ToString().ToLower().Equals("okay"))
                {
                    return true;
                }
                else
                {
                    return false;
                }

               
            }
        }
        #endregion
    }
}
