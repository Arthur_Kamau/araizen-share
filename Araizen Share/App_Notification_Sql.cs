﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace Araizen_Share
{
    class App_Notification_Sql
    {
        #region CreateNotificationItemsDatabase
        public void CreateNotificationItemsDatabase(string path)
        {
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=" + path))
            {
                conn.Open();

                
        string sqlQuery = @"
                    CREATE TABLE IF NOT EXISTS Notification_items 
                    (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,

                        notfication_from_name varchar(255),
                        notification_from_email varchar(255),
                        notification_dest_email varchar(255),

                        notification_type varchar(255),  
                        notification_description varchar(255),
                        notification_title varchar(255),

                        notification_seen varchar(255) ,
                        notification_expired varchar(255) ,
                        notification_date varchar(255),

                        notification_foreign_key_for_download varchar(255),  
                        notofication_url varchar(255)
                      
                    )";

                SQLiteCommand command = new SQLiteCommand(sqlQuery, conn);
                command.ExecuteNonQuery();
            }

        }
        #endregion


        /*
        *******************************************************************************************
        * 
        * insert functions
        * 
        * *******************************************************************************************
        */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationItem"></param>
        #region InsertnotificationItemsDatabase
        public void InsertnotificationItemsDatabase(App_Notification notificationItem)
        {
            App_System_Paths paths = new App_System_Paths();
            string notificationItemPath = paths.NotificationsSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + notificationItemPath))
            {

                //if it exist ignore


                //changed from select * to notification_type
                //continue to insert
                sqlConnection.Open();
                string sqlQuery = string.Format(
           @"
           
                INSERT INTO Notification_items
                (
                          notfication_from_name  ,
                            notification_from_email  ,
                            notification_dest_email ,

                            notification_type  ,  
                            notification_description  ,
                            notification_title ,


                            notification_seen   ,
                            notification_expired,
                            notification_date  ,

                            notification_foreign_key_for_download  ,  
                            notofication_url  
                )
                VALUES
                (
                    '{0}', '{1}','{2}',
                    '{3}', '{4}', '{5}',
                    '{6}','{7}','{8}',
                    '{9}','{10}'
                ); 
          
            ",
            //for insert
            notificationItem.Notfication_from_name, notificationItem.Notfication_from_email,notificationItem.Notfication_dest_email,
            notificationItem.Notification_type, notificationItem.Notification_description, notificationItem.Notification_title,
            notificationItem.Notification_seen  ,  notificationItem.Notification_expired, notificationItem.Notification_date,
            notificationItem.Notification_foreign_key_for_download, notificationItem.Notofication_url
          


          );



                Console.WriteLine("\n\n --->> {0}", sqlQuery);
                //sqlCommand = sqlConnection.CreateCommand();
                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();
            }
        }
        #endregion


        /*
         *******************************************************************************************
         * 
         * Get functions
         * 
         * *******************************************************************************************
         */
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        #region GetAllnotificationItemsDatabase
        public List<App_Notification> GetAllnotificationItemsDatabase()
        {
            App_System_Paths paths = new App_System_Paths();
            string notificationItemPath = paths.NotificationsSQL();
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=" + notificationItemPath))
            {
                conn.Open();

                SQLiteCommand command = new SQLiteCommand(@"Select

                        notfication_from_name  ,
                        notification_from_email  ,
                        notification_dest_email,

                        notification_type  ,  
                        notification_description  ,
                        notification_title,

                        notification_seen   ,
                        notification_expired,
                        notification_date  ,

                        notification_foreign_key_for_download  ,  
                        notofication_url  

                       from Notification_items order by id", conn);
                SQLiteDataReader reader = command.ExecuteReader();

                List<App_Notification> Notification_items = new List<App_Notification>();

                try
                {
                    while (reader.Read())
                    {
                        string user_notfication_from_name = reader["notfication_from_name"].ToString();
                        string user_notification_from_email = reader["notification_from_email"].ToString();
                        string user_notification_dest_email = reader["notification_dest_email"].ToString();

                        string user_notification_type = reader["notification_type"].ToString();
                        string user_notification_description = reader["notification_description"].ToString();

                        string user_notification_title = reader["notification_title"].ToString();
                        string user_notification_seen = reader["notification_seen"].ToString(); // reader["notification_seen"].ToString().Equals("true") || reader["notification_seen"].ToString().Equals('1') ? true : false;
                        string user_notification_date = reader["notification_date"].ToString();

                        string user_Notification_expired = reader["notification_expired"].ToString();
                        int user_notification_foreign_key_for_download = Convert.ToInt32( reader["notification_foreign_key_for_download"].ToString());
                        string user_notofication_url = reader["notofication_url"].ToString();

                        App_Notification notificationItem = new App_Notification(

                              notfication_from_name: user_notfication_from_name,
                              notfication_from_email: user_notification_from_email,
                           notfication_dest_email:   user_notification_dest_email,
                              notification_type: user_notification_type,
                              notification_description: user_notification_description,
                              notification_title: user_notification_title,
                             notification_seen: user_notification_seen,
                             notification_expired : user_Notification_expired,
                              notification_date: user_notification_date,
                              notification_foreign_key_for_download: user_notification_foreign_key_for_download,
                              notofication_url: user_notofication_url


                        );

                        Notification_items.Add(notificationItem);
                    }
                    reader.Close();
                }catch(Exception eps)
                {
                    Console.WriteLine("\n\n Error {0}\n\n",eps);
                }
                return Notification_items;
            }
        }
        #endregion



        #region GetnotificationItemsDatabase
        public App_Notification GetLastnotificationItemFromDatabase()
        {
            App_System_Paths paths = new App_System_Paths();
            string notificationItemPath = paths.NotificationsSQL();
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=" + notificationItemPath))
            {
                conn.Open();

                SQLiteCommand command = new SQLiteCommand(@"Select

                      notfication_from_name  ,
                        notification_from_email  ,
                        notfication_dest_email ,

                        notification_type  ,  
                        notification_description  ,
                        notification_title,

                        notification_seen   ,
                        notification_expired  ,
                        notification_date  ,

                        notification_foreign_key_for_download  ,  
                        notofication_url  

                       from Notification_items order by id DESC  limit 1", conn);
                SQLiteDataReader reader = command.ExecuteReader();
                //try
                //{
                //read the row
                while (reader.Read())
                {

                    string user_notfication_from_name = reader["notfication_from_name"].ToString();
                    string user_notification_from_email = reader["notification_from_email"].ToString();
                    string user_notfication_dest_email = reader["notfication_dest_email"].ToString();

                    string user_notification_type = reader["notification_type"].ToString();
                    string user_notification_description = reader["notification_description"].ToString();

                    string user_notification_title = reader["notification_title"].ToString();
                    string user_notification_seen = reader["notification_seen"].ToString(); //reader["notification_seen"].ToString().Equals("true") || reader["notification_seen"].ToString().Equals('1') ? true : false;
                    string user_notification_date = reader["notification_date"].ToString();

                    string user_notification_expired = reader["notification_expired"].ToString();

                    int user_notification_foreign_key_for_download = Convert.ToInt32(reader["notification_foreign_key_for_download"].ToString());
                    string user_notofication_url = reader["notofication_url"].ToString();

                    App_Notification notificationItem = new App_Notification(

                          notfication_from_name: user_notfication_from_name,
                          notfication_from_email: user_notification_from_email,
                          notfication_dest_email : user_notfication_dest_email,
                          notification_type: user_notification_type,
                          notification_description: user_notification_description,
                          notification_title: user_notification_title,
                         notification_seen: user_notification_seen,
                         notification_expired : user_notification_expired,
                          notification_date: user_notification_date,
                          notification_foreign_key_for_download: user_notification_foreign_key_for_download,
                          notofication_url: user_notofication_url


                    );

                    reader.Close();
                    return notificationItem;

                }

                    
                //}
                //catch(Exception eps)
                //{
                //    Console.WriteLine("error in console app notifications");
                    
                //}
                return null;
              
            }
        }
        #endregion
        /*
       *******************************************************************************************
       * 
       * Update functions
       * 
       * *******************************************************************************************
       */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file_name"></param>
        #region UpdatenotificationItemItemsIsSeenDatabase
        public void UpdateNotificationItemsIsSeenDatabase(string notfication_from_name, string notification_from_email)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.UploadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();

                string sqlQuery = string.Format(

                    @"UPDATE Notification_items 
                    SET notification_seen = 'true',                
                    WHERE notfication_from_name = '{0}' and notification_from_email = '{1}'

                    ", notfication_from_name,notification_from_email
                    );

                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();


            }
        }
        #endregion

      

        /*
         *******************************************************************************************
         * 
         * Delete functions
         * 
         * *******************************************************************************************
         */


        /// <summary>
        /// delette item
        /// </summary>
        /// <param name="notfication_from_name"></param>
        /// <param name="notification_from_email "></param>
        #region DeletenotificationItemsDatabase
        public void DeletenotificationItemsDatabase(string notfication_from_name,string notification_from_email )
        {
            App_System_Paths paths = new App_System_Paths();
            string notificationItemPath = paths.NotificationsSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + notificationItemPath))
            {
                sqlConnection.Open();
                //delete item
                string sqlQuery = string.Format(@"DELETE FROM Notification_items WHERE  notfication_from_name = '{0}' and notification_from_email = '{1}'", notfication_from_name, notification_from_email);
                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();
            }
        }
        #endregion


        /// <summary>
        /// delette item
        /// </summary>
        /// <param name="notfication_from_name"></param>
        /// <param name="notification_from_email "></param>
        #region DeletenotificationItemsDatabase
        public void DeleteAllFromnotificationItemsDatabase()
        {
            App_System_Paths paths = new App_System_Paths();
            string notificationItemPath = paths.NotificationsSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + notificationItemPath))
            {
                sqlConnection.Open();
                //delete item
                string sqlQuery = string.Format(@"DELETE FROM Notification_items");
                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}