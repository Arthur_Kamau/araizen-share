﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
  public  class App_Upload
    {
        public string SourceClientsEmail { get; set; }
        public String SourceClientsName { get; set; }
        public List<string> DestinationEmailClients { get; set; }
        public string Description  { get; set; }
        public string UploadDate { get; set; }
        public long UnixTimeFileItemReferenceId { get; set; }
        public string FileMachineUpload { get; set; }
        public List<App_Upload_File_Item> FileItem { get; set; }

        public App_Upload(string sourceEmail, string sourceName, List<string> destinationemails, string description, string fileMachineUpload, string uploadTime, long unixFileTimeReferenceId, List<App_Upload_File_Item> fileItem)
        {
            SourceClientsEmail = sourceEmail;
            SourceClientsName = sourceName;
            DestinationEmailClients = destinationemails;
            Description = description;
            FileItem = fileItem;
         UploadDate=   uploadTime;
            UnixTimeFileItemReferenceId = unixFileTimeReferenceId;
                FileMachineUpload = fileMachineUpload;
        }
    }

 public   class App_Upload_File_Item
    {
        public String FileName { get; set; }
        public long FileSize { get; set; }
        public String FileExt { get; set; }
        public int FileEachItemSize { get; set; }
        public int TotalUploadItems { get; set; }
        public int CurrentUploadItem { get; set; }
        public String FileDownloadUrl { get; set; }
        public string UploadDate { get; set; }
        public long UnixTimeFileItemReferenceId { get; set; }
        public String FileLocation { get; set; }
        public Boolean IsStarted { get; set; }
        public Boolean IsCompleted { get; set; }
        public String FileEachItemLocation { get; set; }

        public App_Upload_File_Item(string fileName, long fileSize, string fileExt, int fileEachItemSize, int totalUploadItems, int currentUploadItem, string fileDownloadUrl, string uploadDate, long unixTimeFileItemReferenceId, string fileLocation, bool isStarted, bool isCompleted, string fileEachItemLocation)
        {
            FileName = fileName;
            FileSize = fileSize;
            FileExt = fileExt;
            FileEachItemSize = fileEachItemSize;
            TotalUploadItems = totalUploadItems;
            CurrentUploadItem = currentUploadItem;
            FileDownloadUrl = fileDownloadUrl;
            UploadDate = uploadDate;
            UnixTimeFileItemReferenceId = unixTimeFileItemReferenceId;
            FileLocation = fileLocation;
            IsStarted = isStarted;
            IsCompleted = isCompleted;
            FileEachItemLocation = fileEachItemLocation;
        }
    }

    class App_Upload_Update_Data
    {
        public string Itemkey { get; set; }
        public int ProgressValue { get; set; }
        public string ProgressPercentage { get; set; }

        public App_Upload_Update_Data(string itemkey, int progressValue, string progressPercentage)
        {
            Itemkey = itemkey;
            ProgressValue = progressValue;
            ProgressPercentage = progressPercentage;
        }
    }
}
