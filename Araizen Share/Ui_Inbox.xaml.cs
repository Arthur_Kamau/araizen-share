﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for inbox.xaml
    /// </summary>
    public partial class Ui_Inbox : Page
    {
        List<string> item_list = new List<string>();
        
        App_System_Paths path = new App_System_Paths();

        public Ui_Inbox()
        {
            InitializeComponent();
            CurrentDownload();
        }

        #region CurrentDownload()
        private void CurrentDownload()
        {


            //select folder
            String path_to_documents = path.MyDocuments();
            string share_folder = path.ShareFolder(); //string.Concat(path_to_documents, "\\Araizen Crypto");

            Console.WriteLine("path to  documnets  {0} /n path to  sambaza {1}", path_to_documents, @share_folder);
            //File.Create(share_folder);
            bool exists = Directory.Exists(share_folder);
            if (!exists)
            {
                Directory.CreateDirectory(share_folder);
            }

            String[] share_folder_contents = Directory.GetFiles(share_folder);
            if (share_folder_contents.Count() == 0)
            {

                content_infomation.Content = "No items currently being sent";
                Console.WriteLine("Folde item empty");
            }
            else
            {
                content_infomation.Opacity = 0;
                int counter = 0;
                foreach (string file_item in share_folder_contents)
                {
                    Console.WriteLine("Folde item : {0}", file_item);

                    String owner = App_System_Details.GetComputerName();
                    long length = new System.IO.FileInfo(file_item).Length;
                    String ext = System.IO.Path.GetExtension(file_item);
                    String name_without_ext = System.IO.Path.GetFileNameWithoutExtension(file_item);

                    Add_items_to_ui(name: name_without_ext, owner: owner, type: ext, size: length, file_path: file_item, counterInt: counter);
                    item_list.Add(file_item);
                    counter++;
                }

            }
        }
        #endregion

        #region add folder items to ui  -->Add_items_to_ui(String name,String owner,String type,long size)
        private void Add_items_to_ui(String name, String owner, String type, long size, string file_path, int counterInt)
        {
            App_System_Paths asps = new App_System_Paths();

            StackPanel item_grid = new StackPanel();
            StackPanel image_and_comment = new StackPanel();
            StackPanel comments = new StackPanel();

            StackPanel item_details = new StackPanel();
            StackPanel item_details_description = new StackPanel();
            StackPanel download_stream = new StackPanel();



            //who is hosting the content
            Label item_parent_label = new Label
            {
                Content = string.Concat("Owner \t", owner) 
            };
            //name of the content
            Label item_name_label = new Label
            {
                Content = string.Concat("Name \t", name)
            };
            //size
            Label item_size_label = new Label
            {
                Content = string.Concat("Size ", Format_length(size))
            };
            //type -> word video music document
            Label item_type_label = new Label();

            Button uploaded_percentage_Button = new Button();
            StackPanel uploaded_percentage_Button_StackPanel = new StackPanel();
            Image uploaded_percentage_Button_StackaPanel_Image = new Image();
            Label uploaded_percentage_Label = new Label();


            uploaded_percentage_Button_StackPanel.Orientation = Orientation.Horizontal;

            uploaded_percentage_Label.Content = "  Download";
            uploaded_percentage_Button_StackaPanel_Image.Source = new BitmapImage(new Uri(@"/resources/images/ic_arrow_downward_black_18dp.png", UriKind.Relative));
            uploaded_percentage_Button_StackaPanel_Image.Height = 25;
            uploaded_percentage_Button_StackaPanel_Image.Width = 25;


            uploaded_percentage_Button_StackPanel.Children.Add(uploaded_percentage_Button_StackaPanel_Image);
            uploaded_percentage_Button_StackPanel.Children.Add(uploaded_percentage_Label);
            uploaded_percentage_Button.Content = uploaded_percentage_Button_StackPanel;
            uploaded_percentage_Button.Margin  = new Thickness(15, 0, 0, 0);
            uploaded_percentage_Button.Height = 40;
            uploaded_percentage_Button.Width = 120;
            uploaded_percentage_Button.Background = new SolidColorBrush(Colors.Transparent);
            uploaded_percentage_Button.BorderBrush = new SolidColorBrush(Colors.Transparent);
            uploaded_percentage_Button.Foreground = new SolidColorBrush(Colors.Black);
            uploaded_percentage_Button.BorderThickness = new Thickness(0, 0, 0, 0);

            Style style = this.Resources["MyButton"] as Style;
            uploaded_percentage_Button.Style = style;



            FileInfo nf = new FileInfo(file_path);
            if (nf.Attributes.Equals("Directory"))
            {
                type = "Folder";
            }

            item_type_label.Content = string.Concat("\t\t Type :  ", type," \t");


            //progress indicator
            MetroProgressBar upload_progress = new MetroProgressBar();
            upload_progress.Minimum = 0;
            upload_progress.Maximum = 100;
            upload_progress.MaxHeight = 4;
            upload_progress.Height = 3;
            upload_progress.Value = 20;

            upload_progress.Foreground = new System.Windows.Media.SolidColorBrush(Colors.Indigo);
            //         Foreground = "{DynamicResource AccentColorBrush}"

            //like it or not 
            Button comment_button = new Button();
            Button thumbs_up_button = new Button();
            Button thumbs_down_button = new Button();

            Image item_image = new Image();
            // item_image.Height = 70;
            // item_image.Width = 80;

            //Image comment_image = new Image();
            //Image thumbs_up_image = new Image();
            //Image thumbs_down_image = new Image();



            item_grid.Orientation = Orientation.Horizontal;
            item_grid.Width = inbox_contents.Width;
            item_grid.Height = 100;

            image_and_comment.Orientation = Orientation.Vertical;
            //comments.Orientation = Orientation.Horizontal;

            item_image.Width = 100; item_image.Height = 100;


            comment_button.Height = 25; comment_button.Width = 25;
            thumbs_up_button.Height = 25; thumbs_up_button.Width = 25;
            thumbs_down_button.Height = 25; thumbs_down_button.Width = 25;

            comments.Height = 30; comments.Width = 90;
            //comment_image.Width = 20; comment_image.Height = 20;
            //thumbs_up_image.Width = 20; thumbs_up_image.Height = 20;
            //thumbs_down_image.Width = 20; thumbs_up_image.Height = 20;


            comment_button.FindResource(ToolBar.ButtonStyleKey);
            comment_button.BorderThickness = new Thickness(0, 0, 0, 0);
            //  comment_button.Background = new SolidColorBrush(Colors.White);
            //comment_button.Content = comment_image;

            thumbs_down_button.FindResource(ToolBar.ButtonStyleKey);
            thumbs_down_button.BorderThickness = new Thickness(0, 0, 0, 0);
            //thumbs_down_button.Content = thumbs_down_image;

            thumbs_up_button.FindResource(ToolBar.ButtonStyleKey);
            thumbs_up_button.BorderThickness = new Thickness(0, 0, 0, 0);
            //thumbs_up_button.Content = thumbs_up_image;

            comments.Children.Add(comment_button);
            comments.Children.Add(thumbs_up_button);
            comments.Children.Add(thumbs_down_button);

            image_and_comment.Children.Add(item_image);
            // image_and_comment.Children.Add(comments);

            //item details
            item_details.Width = 500;
            item_details.Height = 100;
            item_details.Children.Add(item_name_label);
            item_details.Children.Add(item_parent_label);

            item_details_description.Orientation = Orientation.Horizontal;
            item_details_description.Children.Add(item_size_label);
            item_details_description.Children.Add(item_type_label);
            item_details_description.Children.Add(uploaded_percentage_Button);

            //add item_details_description into item_details
            item_details.Children.Add(item_details_description);

            download_stream.Orientation = Orientation.Vertical;




            List<string> video_ext = new List<string>() { ".mp4", ".mov", ".wmv", ".avi", ".mkv", ".flv" };
            List<string> image_ext = new List<string>() { ".png", ".jpeg", ".gif", ".bmp", ".tiff", ".psd", ".webp" };
            List<string> music_ext = new List<string>() { ".mp3", ".ogg", ".aac", ".atrac", ".aiff", ".wma", ".vorbis", ".opus" };
            List<string> wordDocument_ext = new List<string>() { ".doc", ".docx", ".dotx", ".rtf", ".dot" };
            List<string> textFile_ext = new List<string>() { ".txt", ".log" };
            List<string> webFile_ext = new List<string>() { ".html", ".htm" };



            FileAttributes attr = File.GetAttributes(file_path);
            //detect whether its a directory or file
            if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/folder_icon.jpg", UriKind.Relative));

            }

            if (video_ext.Contains(type.ToLower()))
            {
              
                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/video_file.png", UriKind.Relative));


            }
            else if (wordDocument_ext.Contains(type.ToLower()))
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/WinWordLogoSmall.scale-140.png", UriKind.Relative));


            }

            else if (type.ToLower().Equals(".pdf"))
            {


                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/pdf.png", UriKind.Relative));


            }
            else if (type.ToLower().Equals(".xml"))
            {
                //String resourceDir = asps.Curr_Resources_Dir();
                //System.Uri resourceImageUri = new System.Uri(System.IO.Path.Combine(resourceDir, "images/file.jpg"));

                //Console.WriteLine("->Resource Dir ={0} \n resource image uri ={1}", asps.Curr_Resources_Dir(), resourceImageUri);

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/file.jpg", UriKind.Relative));

            }

            else if (music_ext.Contains(type.ToLower()))
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/audio_file_2.png", UriKind.Relative));

            }
            else if (image_ext.Contains(type.ToLower()))
            {

                System.Uri resourceImageUri = new System.Uri(file_path);

                item_image.Source = new BitmapImage();
            }
            else
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/files.png", UriKind.Relative));

            }





            //comment_image.Source = new BitmapImage(new Uri(@"C:\Users\Arthur-Kamau\source\repos\sambaza-desktop-ui\sambaza-desktop-ui\resources\ic_comment_black_24dp.png"));
            //thumbs_down_image.Source = new BitmapImage(new Uri(@"C:\Users\Arthur-Kamau\source\repos\sambaza-desktop-ui\sambaza-desktop-ui\resources\ic_thumb_down_black_24dp_1x.png"));

            //thumbs_up_image.Source = new BitmapImage(new Uri(@"C:\Users\Arthur-Kamau\source\repos\sambaza-desktop-ui\sambaza-desktop-ui\resources\ic_thumb_up_black_24dp_1x.png"));



            item_grid.Children.Add(image_and_comment);
            item_grid.Children.Add(item_details);
            item_grid.Children.Add(download_stream);
         //   item_details.Children.Add(upload_progress);


            item_grid.Margin = new Thickness(0, 10, 0, 0);


            inbox_contents.Children.Add(item_grid);


            /**
             * set selcetced item in combiobox
             */

        }
        #endregion

        #region formt length
        private string Format_length(long len)
        {
            string[] sizes = { "b", "KB", "MB", "GB", "TB" };
            int order = 0;
            while (len >= 1024 && order < sizes.Length - 1)
            {
                order++;
                len = len / 1024;
            }
            String result = string.Format("{0:0.##} {1}", len, sizes[order]);
            Console.WriteLine("-->{0}", result);
            return result;
        }
        #endregion

    }
}