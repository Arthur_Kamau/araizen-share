﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Inbox
    {
        public String FileName { get; set; }
        public String FileSize { get; set; }
        public String FileType { get; set; }
        public int FileEachItemSize { get; set; }
        public int CurrentUploadItem { get; set; }
        public String FileDownloadUrl { get; set; }
        public DateTime UploadDate { get; set; }

        public App_Inbox(string fileName, string fileSize, string fileType, int fileEachItemSize, int currentUploadItem, string fileDownloadUrl, DateTime uploadDate)
        {
            FileName = fileName;
            FileSize = fileSize;
            FileType = fileType;
            FileEachItemSize = fileEachItemSize;
            CurrentUploadItem = currentUploadItem;
            FileDownloadUrl = fileDownloadUrl;
            UploadDate = uploadDate;
        }
    }
}
