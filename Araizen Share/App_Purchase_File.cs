﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Purchase_File
    {
        App_System_Paths paths = new App_System_Paths();
        App_File files = new App_File();

        #region GetPurchaseDetailsFile
        public App_Purchase GetPurchaseDetailsFile()
        {
            string json_str = files.Get_data_from_file(paths.PurchaseFile());
            Console.WriteLine(" GetPurchaseDetails --------->{0}", json_str);
            App_Purchase pur = JsonConvert.DeserializeObject<App_Purchase>(json_str);
            return pur;

        }
        #endregion


        #region SetPurchaseDetailsFile
        public void SetPurchaseDetailsFile(App_Purchase pur)
        {

            // App_Purchase pur = new App_Purchase(key: key, dateTime: purchaseTime, thePeriod: period);


            string json_str = JsonConvert.SerializeObject(pur);
            Console.WriteLine("\n purchase details --------->{0}", json_str);
            files.Save_data_to_file(path: paths.PurchaseFile(), data: json_str);

        }
        #endregion

    }
}
