﻿using Squirrel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for splashscreen.xaml
    /// </summary>
    public partial class Ui_SplashScreen : Window
    {
        DispatcherTimer dt = new DispatcherTimer();
        DispatcherTimer prog = new DispatcherTimer();
        Main_Window mainwindow = new Main_Window();


        App_Registry reg = new App_Registry();
        App_Resource res = new App_Resource();

        public Ui_SplashScreen()
        {

           

            InitializeComponent();

            LoadUiComponents();

            CreateDatabaseFiles();
           

            //show splash scree for some time
            dt.Tick += new EventHandler(SplashTimer);
            dt.Interval = new TimeSpan(0, 0, 12);
            dt.Start();


            //set progress bar
            prog.Tick += new EventHandler(ProgressTimer);
            prog.Interval = new TimeSpan(0, 0, 1);
            prog.Start();


            

        }

        private void Close_Stop_Timer() { dt.Stop(); this.Close(); }
        private int incriments = 0;


        string pathToNotificationsDb = string.Empty;
        string pathToUploadDb = string.Empty;
        string pathToDownloadDb = string.Empty;
        string pathToHistoryDb = string.Empty;

        /// <summary>
        ///create the databse file
        /// </summary>
        #region  CreateDatabaseFiles
        private void CreateDatabaseFiles()
        {
            //create databases if not exist

            //notifications db
            App_System_Paths paths = new App_System_Paths();

             pathToNotificationsDb = paths.NotificationsSQL();

            //downloading db
             pathToDownloadDb = paths.DownloadSQL();

            //uploading db
             pathToUploadDb = paths.UploadSQL();

            //history db
            pathToHistoryDb = paths.HistorySQL();

            Console.Write("paths created pathToNotificationsDb = {0} and  pathToDownloadDb = {1}  and   pathToUploadDb = {2}", pathToNotificationsDb, pathToDownloadDb, pathToUploadDb);



        }
        #endregion

        //splash timer
        #region SplashTimer
        private void SplashTimer(object sender, EventArgs e)
        {
            //stop timer
            dt.Stop();
            CheckUpdateThenProceed(); 
        }
        #endregion

        /// <summary>
        /// first check for update
        /// if there is an update proceed
        /// if there is none start authentification procedure
        /// </summary>
        #region CheckUpdateThenProceed
        private async void CheckUpdateThenProceed()
        {

            var IsThereInterent = App_System_Wifi.CheckForInternetConnection();
            if (IsThereInterent)
            {
                await   UpdatApp();

            }
            
            CreateDatabaseTables();

                //app is not locked
                //check if lincence has eneded if not 
                //open mainwindow or login
           StartAuthProcedureThenOpenHome();
            
        }
        #endregion

        /// <summary>
        /// Update  app method
        /// </summary>
        /// <returns></returns>
        #region UpdatApp
        private async Task UpdatApp()
        {

            //get verison chosen by user
            string userPlatform = Araizen_Share.Properties.Settings.Default.PlatformType;
            string url = string.Empty;

            if (string.IsNullOrEmpty(userPlatform))
            {
                //default stable
                url = Araizen_Share.Properties.Settings.Default.UpdatePathStable;
            }
            else
            {
                if (userPlatform == "PlatformStable")
                {
                    //unstable
                    url = Araizen_Share.Properties.Settings.Default.UpdatePathLatest;
                }
                else
                {
                    //stable
                    url = Araizen_Share.Properties.Settings.Default.UpdatePathStable;
                }
            }

            Console.WriteLine($"download url is {url}");
            try
            {
                var path = @"C:\Users\Arthur-Kamau\Araizen Tools Update\AraizenShare\Stable";
                using (var manager = new UpdateManager(path))
                {
                    await manager.UpdateApp();
                }
                //using (var mgr = UpdateManager.GitHubUpdateManager(url)) //"https://github.com/myuser/myapp"
                //{
                //    await mgr.Result.UpdateApp();
                //}
            }
            catch (Exception ex)
            {
                string title = "Update Error";
                string content = ex.ToString();//"Please report this, error.(Click okay to continue to the application)";

                Ui_Error errs = new Ui_Error(title: title, body: content);
                errs.ShowDialog();

                Console.WriteLine($"error in splash screen updater {ex}");

            }


        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        #region CreateDatabaseTables
        private void CreateDatabaseTables()
        {
            //create databases if not exist

            if (!string.IsNullOrEmpty(pathToNotificationsDb) && !string.IsNullOrEmpty(pathToDownloadDb) && !string.IsNullOrEmpty(pathToUploadDb))
            {
                //notifications db
                //create table
                App_Notification_Sql sqlQuery = new App_Notification_Sql();
                sqlQuery.CreateNotificationItemsDatabase(pathToNotificationsDb);


                //downloading db
                App_Download_Sql sql_downloads = new App_Download_Sql();
                sql_downloads.CreateDownloadItemsDatabase(pathToDownloadDb);

                //uploading db
                App_Upload_Sql sql_upload = new App_Upload_Sql();
                sql_upload.CreateUploadItemsDatabase(pathToUploadDb);


                //history 
                App_History_Sql sql_history = new App_History_Sql();
                sql_history.CreateHistoryItemsDatabase(pathToHistoryDb);
            }
            else
            {

                Console.Write("\n\n --------A path is empty------- ");
            }


            Console.Write("paths created pathToNotificationsDb = {0} and  pathToDownloadDb = {1}  and   pathToUploadDb = {2}", pathToNotificationsDb, pathToDownloadDb, pathToUploadDb);

        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        #region ProgressTimer
        private void ProgressTimer(object sender, EventArgs e)
        {
            incriments++;
            progress_bar.Value = incriments;
            Console.WriteLine(" value -{0}", incriments);
            if (incriments.Equals(10) || incriments == 10 || incriments > 10)
            {
                prog.Stop();
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        #region IsUserBlocked
        private bool IsUserBlocked()
        {
            App_Blocked_Server blockedServer = new App_Blocked_Server();
            App_Auth_File authFile = new App_Auth_File();
            App_Auth authData = authFile.GetAuthDetailsFile();

            var IsThereInterent = App_System_Wifi.CheckForInternetConnection();
            try
            {
                if (IsThereInterent)
                {


                    try
                    {
                        bool isTheuserBlocked = blockedServer.IsUserBlockedServer(authData.Email);
                        if (isTheuserBlocked)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch
                    {
                        return false;
                    }

                }
                else
                {
                    return false;

                }
            }
            catch
            {
                return false;
            }
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        #region StartAuthProcedureThenOpenHome
        private void StartAuthProcedureThenOpenHome()
        {
            /**
             *first we check if the user is blocked and show bloced screen
             */
            if (IsUserBlocked())
            {
                Console.WriteLine("\n\n App is  locked \n\n");
                Ui_Locked locks = new Ui_Locked();
                locks.Show();
                this.Close();
            }
            else
            {
                Console.WriteLine("\n\n App Not is  locked \n\n");
                //check if user is logged in
                if (IsUserLoggedIn())
                {
                    Console.WriteLine("\n\n User is  logged in \n\n");
                    //user is logged in
                    //is licence expired\
                    if (IsTrialOrPurchaseLicenceExpired())
                    {
                       
                        Ui_Purchase pur = new Ui_Purchase(KillOnexit: true);
                        pur.Show();
                        this.Close();
                    }
                    else
                    {
                        //chow main page
                        Main_Window main_window = new Main_Window();
                        main_window.Show();
                        this.Close();
                    }

                }
                else
                {
                    Console.WriteLine("\n\n App Not logged in \n\n");
                    //user is NOT logged in
                    Ui_LoginRegisterOption loginRegister = new Ui_LoginRegisterOption();
                    loginRegister.Show();
                    this.Close();


                }

            }
            
           

        }
        #endregion
        
        
        /// <summary>
        /// chack locked str
        /// if not empty user bloced 
        /// //if empty or null user is not blocked
        /// 
        /// get if user is blocked locally
        /// </summary>
        /// <returns></returns>
        #region IsUserBlocked
        //private bool IsUserBlocked()
        //{
        //    String IsAppLockedstr = String.Empty;
        //    try
        //    {

        //        IsAppLockedstr = reg.GetAppIsLockedOrNot();
        //        Console.WriteLine("\n\n\n get app is locked {0} \n\n", IsAppLockedstr);
        //    }
        //    catch (Exception errs) { Console.WriteLine($"Error {errs}"); }

        //    if (string.IsNullOrWhiteSpace(IsAppLockedstr))
        //    {
        //        // IsAppLocked = false;
        //        return false;
        //    }
        //    else if (IsAppLockedstr.ToLower().Equals(true))
        //    {
        //       // IsAppLocked = true;
        //        return true;
        //    }
        //    else
        //    {
        //        //IsAppLocked = false;
        //        return false;
        //    }
        // }
        #endregion

        /// <summary>
        /// first check if user is under trial licence or purchase licence
        ///  then get / caluclate the expiery  dates
        ///  if past today return true
        ///  open purchase page
        /// </summary>
        #region IsTrialOrPurchaseLicenceExpired
        private bool   IsTrialOrPurchaseLicenceExpired()
        {
            Console.WriteLine("\n in function IsTrialOrPurchaseLicenceExpired");
            //is purchaseed
            if (IsUserPurchasedLicence())
            {
                Console.WriteLine("\n 1) in  IsUserPurchasedLicence = true function IsUserPurchaseLicenceExpired ={0}", IsUserPurchaseLicenceExpired());
                //user is purchase
                return IsUserPurchaseLicenceExpired();
            }
            else
            {
                Console.WriteLine("\n 2)in  IsUserPurchasedLicence = false function IsUserTrialLicenceExpired ={0}", IsUserTrialLicenceExpired());
                //user is trial
                return IsUserTrialLicenceExpired();
            }
          


        }
        #endregion


        /// <summary>
        /// check auth detail from file
        /// then check token time stamp if expired return false (not logged in)
        /// the empty the auth file
        /// </summary>
        /// <returns></returns>
        #region  isUserLoggedIn
        private bool IsUserLoggedIn()
        {
            App_Auth_File auth_file = new App_Auth_File();
            App_Auth auth_data = auth_file.GetAuthDetailsFile();

            Console.WriteLine("\n\n check if user is logged in  \n\n");
            if (auth_data == null) 
            {
                Console.WriteLine("\n\n is logged in false null in file --> not logged in\n\n");
                return   false ;
            }else
            {
                //user is log in token is it expired
                var expired= IsUserLoggedInTokenExpired(Convert.ToDateTime(auth_data.LogInTime));
                //it is expired
                if (expired)
                {
                    Console.WriteLine("\n\n is logged in token expired \n\n");
                    return false;
                }
                else
                {
                    Console.WriteLine("\n\n  is logged in token not expired and not null in file \n\n");
                    //user is logged in and the log in token is not expired
                    return true;
                }
            }

        }
        #endregion

        /// <summary>
        /// check login token if expired
        /// </summary>
        /// <param name="loginTime"></param>
        /// <returns></returns>
        #region  isUserLoggedInTokenExpired
        private bool IsUserLoggedInTokenExpired(DateTime loginTime)
        {
            Console.WriteLine("\n\n ckecing login token on time {0} \n\n", loginTime);
            DateTime now = DateTime.Now;
            TimeSpan difference = now.Subtract(loginTime);

            //the number of hours is past the maximum amount of set time
            if (difference.Hours > App_Const.MaxLoggedInHours)
            {
                EmptyAuthFile();
                return true;
            }
            else
            {
                return false;
            }

        }
        #endregion


        /// <summary>
        /// empty auth file data
        /// </summary>
        #region empty auth file
        private void EmptyAuthFile()
        {
            App_System_Paths paths = new App_System_Paths();
            FileStream fileStream = File.Open( paths.AuthFile(), FileMode.Open);

            /* 
             * Set the length of filestream to 0 and flush it to the physical file.
             *
             * Flushing the stream is important because this ensures that
             * the changes to the stream trickle down to the physical file.
             * 
             */
            fileStream.SetLength(0);
            fileStream.Close(); // This flushes the content, too.
        }
        #endregion


        /// <summary>
        /// check if user has a purchase licence
        /// </summary>
        /// <returns></returns>
        #region IsUserPurchasedLicence
        private bool  IsUserPurchasedLicence()
        {
            App_Registry registry = new App_Registry(); 
            App_Purchase_File purchase_data = new App_Purchase_File();

            App_Purchase purchaseFile = purchase_data.GetPurchaseDetailsFile();

           if(purchaseFile != null)
            {
                String purchaseKeyRegistry = registry.GetPurchaseKey();
                String purchaseKeyFile = purchaseFile.SerialKey;

                if (String.IsNullOrEmpty(purchaseKeyRegistry) || String.IsNullOrEmpty(purchaseKeyFile))
                {
                    return false;
                }
                else
                {
                    if (purchaseKeyFile.Equals(purchaseKeyRegistry))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }
        #endregion

        /// <summary>
        /// chek if licence has passed its due date
        /// </summary>
        /// <returns></returns>
        #region 
        private bool IsUserPurchaseLicenceExpired()
        {
            App_Registry registry = new App_Registry();

            DateTime purchaseDateRegistry = registry.GetPurchaseDate();
            int purchaseDays = Convert.ToInt32( registry.GetPurchasePeriod());

            DateTime licenecEndsOn = purchaseDateRegistry.AddDays(purchaseDays);
          


            if(licenecEndsOn < DateTime.Now)
            {
                //not expired
                return false;
            }
            else if(licenecEndsOn == DateTime.Now)
            {
                return false;
            }
            else
            {
                return true;
            }  
        }
        #endregion


        /// <summary>
        /// chek if licence has passed its due date
        /// UNDER HEASY ASSUMPTION USER IS LOGGED IN
        /// LOADS UNDER FILE
        /// </summary>
        /// <returns></returns>
        #region  IsUserTrialLicenceExpired
        private bool IsUserTrialLicenceExpired()
        {
            App_Install_Register instReg = new App_Install_Register();
            App_Install installData = instReg.GetApp_InstallDetailsFile();

            DateTime installDate = installData.InstallTime;

            if (installDate.AddDays(App_Const.MaxTrialDays) > DateTime.Now)
            {
                //not expired
                return false;
            }
            else if (installDate.AddDays(App_Const.MaxTrialDays) == DateTime.Now)
            {
                return false;
            }
            else
            {
                return true;
            }


        }
        #endregion


        /// <summary>
        /// load the ui details
        /// </summary>
        #region LoadUiComponents
        private void LoadUiComponents()
        {
            App_Auth_File auth_file = new App_Auth_File();
            App_Auth authData = auth_file.GetAuthDetailsFile();


            // String Versionstr = App_Object.GetApp_Version();
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            share_version.Content = versionInfo.FileVersion;

            if (authData != null && !string.IsNullOrEmpty(authData.Name))
            {
                //licenced to
                licence_to.Content = authData.Name;
                //licence type
                account_type.Content = authData.AccountType;
            }
            else
            {
                licence_to.Content = "Un known";
                account_type.Content = "Un known";
            }

           //purchase data ui
            App_Registry reg = new App_Registry();
            string purchaseKey = reg.GetPurchaseKey();
            DateTime purchaseDay = reg.GetPurchaseDate();
            string purchasePeriod = reg.GetPurchasePeriod();

           
            if (!string.IsNullOrEmpty(purchaseKey) && !string.IsNullOrEmpty(purchasePeriod))
            {
                Console.WriteLine("write this the purchase period => {0}",purchasePeriod );
                int purchasePeriodInt = Convert.ToInt32(reg.GetPurchasePeriod());

                //  account_type.Content = "Purchase";
                licence_type.Content = "Purchase";
                licence_end.Content = purchaseDay.AddDays(purchasePeriodInt).ToShortDateString();
            }
            else
            {
                //    licence_type.Content = "Un Purchased";
                App_Install_Register ireg = new App_Install_Register();
                App_Install install = ireg.GetApp_InstallDetailsFile();
                DateTime licenceEnds = install.InstallTime.AddDays(App_Const.MaxTrialDays);
                licence_type.Content = "Trial";
                licence_end.Content = licenceEnds.ToShortDateString(); ;
            }
        }
        #endregion

   
    }
}
