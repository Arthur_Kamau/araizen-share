﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for attach_file.xaml
    /// </summary>
    public partial class Ui_AttachFile : Page
    {
        static public event EventHandler openUploading;
        string itemSelectedFromList = string.Empty;
        List<string> AttachedItemsForUpload = new List<string>();

        
        public Ui_AttachFile()
        {
            InitializeComponent();

            //initial client text box
            InitialTextBox();
        }

        #region formt length
        private string Format_length(long len)
        {
            string[] sizes = { "b", "KB", "MB", "GB", "TB" };
            int order = 0;
            while (len >= 1024 && order < sizes.Length - 1)
            {
                order++;
                len = len / 1024;
            }
            String result = string.Format("{0:0.##} {1}", len, sizes[order]);
            Console.WriteLine("-->{0}", result);
            return result;
        }
        #endregion

        private void Itemsattched_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {

            var item = ItemsControl.ContainerFromElement(sender as System.Windows.Controls.ListBox, e.OriginalSource as DependencyObject) as ListBoxItem;
            if (item != null)
            {
                itemSelectedFromList = string.Empty;
                // ListBox item clicked - do some cool things here
                Console.WriteLine($"You clicked {item} --- \t Content {item.Content} ");
                ////set ui
                ///
                long length = new System.IO.FileInfo(item.Content.ToString()).Length;
                ItemType.Content = System.IO.Path.GetExtension(item.Content.ToString());
                ItemSize.Content = string.Concat("Size ", Format_length(length));

                ItemsattchedUi.SelectedItem = item;


                itemSelectedFromList = item.Content.ToString();

                System.Windows.Controls.ContextMenu cm = this.FindResource("cmButton") as System.Windows.Controls.ContextMenu;
                cm.PlacementTarget = sender as System.Windows.Controls.ListBox;//Button;
                cm.IsOpen = true;

                e.Handled = true;
            }

        }

        private void Itemsattched_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(sender as System.Windows.Controls.ListBox, e.OriginalSource as DependencyObject) as ListBoxItem;
            if (item != null)
            {
                itemSelectedFromList = string.Empty;
                // ListBox item clicked - do some cool things here
                Console.WriteLine($"You clicked {item} --- \t Content {item.Content} ");


                ItemsattchedUi.SelectedItem = item;


                itemSelectedFromList = item.Content.ToString();

                System.Windows.Controls.ContextMenu cm = this.FindResource("cmButton") as System.Windows.Controls.ContextMenu;
                cm.PlacementTarget = sender as System.Windows.Controls.ListBox;//Button;
                cm.IsOpen = true;

                e.Handled = true;
            }
        }

        private void RemoveItemFormList_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine($"You clicked remove from item list {itemSelectedFromList} ");
            //remove to ui
            ItemsattchedUi.Items.Remove(itemSelectedFromList);
            //remove to backend list
            AttachedItemsForUpload.Remove(itemSelectedFromList);

            TotalItemsWorkingOn.Content = AttachedItemsForUpload.Count;
        }

        private void ReomveAll_Click(object sender, RoutedEventArgs e)
        {
            //clear to ui
            ItemsattchedUi.Items.Clear();
            //clear to backend list
            AttachedItemsForUpload.Clear();
            TotalItemsWorkingOn.Content = "";
        }

        private void SelectFolder_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);

                    // System.Windows.Forms.MessageBox.Show("Files found: " + files.Length.ToString(), "Message");
                    foreach (var item in files)
                    {
                        //chaeck to ensure file does not exist
                        if (AttachedItemsForUpload.Count > 0 && ItemsattchedUi.Items.Count > 0)
                        {
                            if (!AttachedItemsForUpload.Contains(item))
                            {
                                //add to ui
                                ItemsattchedUi.Items.Add(item);
                                //add to backend list
                                AttachedItemsForUpload.Add(item);
                            }
                            else
                            {
                                ShowLabelError("Some files were similar to items already selected");
                            }

                        }
                        else
                        {
                            //add to ui
                            ItemsattchedUi.Items.Add(item);
                            //add to backend list
                            AttachedItemsForUpload.Add(item);


                        }

                    }
                    TotalItemsWorkingOn.Content = AttachedItemsForUpload.Count;
                }
            }
        }

        private void ShowLabelError(String itemErrText, int period = 5)
        {
            ItemsWorkingOn.Content = itemErrText;
            DispatcherTimer dt = new DispatcherTimer();

            dt.Tick += new EventHandler(HideLabel);
            dt.Interval = new TimeSpan(0, 0, period);
            dt.Start();
        }

        private void HideLabel(object sender, EventArgs e)
        {
            ItemsWorkingOn.Content = "";
        }

        private void SelectFile_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new Microsoft.Win32.OpenFileDialog() { Filter = "All Files (*.)|*.*|MP4 Files (*.mp4)|*.mp4|MP3 Files (*.mp3)|*.mp3|JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif|All Files (*.)|*.*" };
            var result = ofd.ShowDialog();
            if (result == false) return;

            //chaeck to ensure file does not exist
            if (AttachedItemsForUpload.Count > 0 && ItemsattchedUi.Items.Count > 0)
            {
                if (!AttachedItemsForUpload.Contains(ofd.FileName))
                {
                    //add to ui
                    ItemsattchedUi.Items.Add(ofd.FileName);
                    //add to backend list
                    AttachedItemsForUpload.Add(ofd.FileName);
                }
                else
                {
                    ShowLabelError("Some files were similar to items already selected");
                }


                TotalItemsWorkingOn.Content = AttachedItemsForUpload.Count;
            }
            else
            {
                //add to ui
                ItemsattchedUi.Items.Add(ofd.FileName);
                //add to backend list
                AttachedItemsForUpload.Add(ofd.FileName);

                TotalItemsWorkingOn.Content = AttachedItemsForUpload.Count;
            }
        }
        List<String> emailDestinations = new List<string>();

        private void Send_ButtonClick(object sender, RoutedEventArgs e)
        {

            IEnumerable<System.Windows.Controls.TextBox> collection = uploading_target.Children.OfType<System.Windows.Controls.TextBox>();

            List<String> targetEmailClients = new List<string>();

            foreach (var emailClient in collection)
            {
                Console.WriteLine("\n\n\n email client {0} and assumed text {1} \n\n\n",emailClient, emailClient.Text );
                //if email client is not null or empty add to destination list
                if (! string.IsNullOrEmpty(emailClient.Text))
                {
                    targetEmailClients.Add(emailClient.Text);
                }
            }


            //get my emai
            App_Auth_File authFile = new App_Auth_File();
            App_Auth authData = authFile.GetAuthDetailsFile();

            string myEmail = authData.Email;


            if (targetEmailClients.Contains(myEmail))
            {
                string errorText = "    Oopps snap,it looks like the file item sender and item destination is the same,This will be ignored.";
                Ui_AttachFile_Error_Dialogue errDialogue = new Ui_AttachFile_Error_Dialogue(errorText: errorText);
                errDialogue.ShowDialog();
            }

            targetEmailClients.Remove(myEmail);

          
            if(targetEmailClients.Count >0)
            {
                //show dialogue
                //does the heavy work
                //copies the files split it and add to database
                //the closes when done
                Ui_AttachFile_Dialogue d = new Ui_AttachFile_Dialogue(uploadItems: AttachedItemsForUpload, description: description.Text, target_mail: targetEmailClients.ToArray());
                d.ShowDialog();

                if (openUploading != null)
                {
                    openUploading(this, e);
                }
            }
            else
            {
                string errorText = "    Oopps no destination email specified,cannot send item to void.";
                Ui_AttachFile_Error_Dialogue errDialogue = new Ui_AttachFile_Error_Dialogue(errorText: errorText);
                errDialogue.ShowDialog();
            }
        }

       

        private void InitialTextBox()
        {
            System.Windows.Controls.TextBox textBoxItem = new System.Windows.Controls.TextBox();
           
            textBoxItem.Width = 264;
            textBoxItem.Height = 30;
            textBoxItem.Margin = new Thickness(5, 10, 0, 0);
            string textboxName = "textBox" +  1.ToString();
            textBoxItem.Name = textboxName;

            uploading_target.Children.Add(textBoxItem);

            //add to list
            emailDestinations.Add(textboxName);
        }


        private void AddEmailButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.TextBox textBoxItem = new System.Windows.Controls.TextBox();
 
            textBoxItem.Width = 264;
            textBoxItem.Height = 30;
            textBoxItem.Margin = new Thickness(5, 10, 0, 0);

            string  textboxName =  "textBox"+(emailDestinations.Count + 1.ToString());
            textBoxItem.Name=textboxName ;

            uploading_target.Children.Add(textBoxItem);

            //add to list
            emailDestinations.Add(textboxName);
        }


      
    }
}
