﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    #region App_Blocked
    public class App_Blocked
    {

    }
    #endregion

    #region BlockUser
    public class BlockUser
    {
        public string Email { get; set; }
        public string Reason { get; set; }
        public string BlockDate { get; set; }
        public int BlockdDays { get; set; }

        public BlockUser(string email, string reason, string blockDate, int blockdDays)
        {
            Email = email;
            Reason = reason;
            BlockDate = blockDate;
            BlockdDays = blockdDays;
        }
    }
    #endregion

    #region UnBlockUser
    public class UnBlockUser
    {

        public string UserEmail { get; set; }
        public string UserBlockKey { get; set; }

        public UnBlockUser(string userEmail, string userBlockKey)
        {
            UserEmail = userEmail;
            UserBlockKey = userBlockKey;
        }
    }
    #endregion

    #region BlockUserDataContainer
    public class BlockUserDataContainer
    {
        public bool IsOkay { get; set; }
        public string Reason { get; set; }
        public BlockUserData Data { get; set; }

        public BlockUserDataContainer(bool isOkay, string reason, BlockUserData data)
        {
            IsOkay = isOkay;
            Reason = reason;
            Data = data;
        }
    }
    #endregion

    #region BlockUserData
    public class BlockUserData
    {

        public string Email { get; set; }
        public string UnBlockKey { get; set; }
        public string Reason { get; set; }
        public string BlockDate { get; set; }
        public int BlockDays { get; set; }

        public BlockUserData(string email, string unBlockKey, string reason, string blockDate, int blockDays)
        {
            Email = email;
            UnBlockKey = unBlockKey;
            Reason = reason;
            BlockDate = blockDate;
            BlockDays = blockDays;
        }
    }
    #endregion

}
