﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_Login_ForgotPassword_GoTo_Login.xaml
    /// </summary>
    public partial class Ui_Login_ForgotPassword_GoTo_Login : MetroWindow
    {
        public Ui_Login_ForgotPassword_GoTo_Login()
        {
            InitializeComponent();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            Ui_Login proceeedToLogin = new Ui_Login();
            proceeedToLogin.Show();
            this.Close();
        }
    }
}
