﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NamedPipeWrapper;
using WebSocketSharp;
using Newtonsoft.Json;
using System.Threading;
using System.Media;

//this could be added in future versions
//<Button Name = "Inbox" Click="Inbox_Click"  Margin="0,10,0,15" Style="{StaticResource {x:Static ToolBar.ButtonStyleKey}}" Height="42">
//                   <StackPanel Orientation = "Horizontal" >
//                       < Image Height="25" Width="25" Source="./resources/images/baseline_folder_black_18dp.png"></Image>
//                       <Label Content = "Inbox" Width="80" Foreground="Black"></Label>
//                   </StackPanel>
//               </Button>

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Main_Window.xaml
    /// </summary>
    public partial class Main_Window : MetroWindow
    {
        private System.Windows.Forms.NotifyIcon MyNotifyIcon;


        App_Auth authData;
        public Main_Window()
        {
            InitializeComponent();
            //min action
            minAction();
            //default page
            content_container.Source = new Uri("Ui_GeneralData.xaml", UriKind.RelativeOrAbsolute);

            App_Auth_File authFile = new App_Auth_File();
            authData = authFile.GetAuthDetailsFile();

            Ui_GeneralData.openHistory += new EventHandler(historyButton_Click);
            Ui_GeneralData.openNotifications += new EventHandler(openNotifications_Click);
            Ui_GeneralData.openAccount += new EventHandler(openUserAccount_Click);
            Ui_AttachFile.openUploading += new EventHandler(openUploadingButton_Click);
            Ui_Settings.openLoginRegisterOption += new EventHandler(openLoginRegisterButton_Click);
            Ui_Settings.openHistoryFromSettings += new EventHandler(historyButton_Click);
            Ui_Settings.openDocumetationFromSettings += new EventHandler(openDocumentationHistory_Click);

            Ui_ShareNotifications.openDowloadsFromNotification += new EventHandler(openDownloading_Click);
        }


        private void openUserAccount_Click(object sender, EventArgs e)
        {

            content_container.Source = new Uri("Ui_Account.xaml", UriKind.RelativeOrAbsolute);

        }
        private void openDownloading_Click(object sender, EventArgs e)
        {
          
            content_container.Source = new Uri("Ui_Downloading.xaml", UriKind.RelativeOrAbsolute);

        }
        private void openDocumentationHistory_Click(object sender, EventArgs e)
        {

            content_container.Source = new Uri("Ui_HelpPage.xaml", UriKind.RelativeOrAbsolute);

        }
        private void openLoginRegisterButton_Click(object sender, EventArgs e)
        {

            this.Close();
            //  Process.GetCurrentProcess().Kill();
            //Ui_LoginRegisterOption logreg = new Ui_LoginRegisterOption();
            //logreg.Show();

        }
        private void openUploadingButton_Click(object sender, EventArgs e)
        {

            content_container.Source = new Uri("Ui_Sending.xaml", UriKind.RelativeOrAbsolute);

        }

        private void historyButton_Click(object sender, EventArgs e)
        {

            content_container.Source = new Uri("Ui_UserHistory.xaml", UriKind.RelativeOrAbsolute);

        }

        private void openNotifications_Click(object sender, EventArgs e)
        {

            content_container.Source = new Uri("Ui_ShareNotifications.xaml", UriKind.RelativeOrAbsolute);

        }


        public Main_Window(string xmlfile)
        {
            InitializeComponent();
            //min action
            minAction();
            //default page
            content_container.Source = new Uri(xmlfile, UriKind.RelativeOrAbsolute);

        }
        private void minAction()
        {
            //    MyNotifyIcon = new System.Windows.Forms.NotifyIcon();

            //    MyNotifyIcon.Icon = new System.Drawing.Icon(@"C:\Users\Arthur-Kamau\Downloads\link.ico");
            //    // MyNotifyIcon.Visible = true;
            //    MyNotifyIcon.DoubleClick +=
            //        delegate (object sender, EventArgs args)
            //        {
            //            this.Show();
            //            this.WindowState = WindowState.Normal;
            //        };
            //    MyNotifyIcon.Click += delegate (object sender, EventArgs args)
            //    {
            //        this.Show();
            //        this.WindowState = WindowState.Normal;
            //    };

        }
        protected override void OnStateChanged(EventArgs e)
        {

            // this.Hide();
            //if (this.WindowState == WindowState.Minimized)
            //{
            //    this.ShowInTaskbar = false;
            //    MyNotifyIcon.BalloonTipTitle = "Minimize Sucessful";
            //    MyNotifyIcon.BalloonTipText = "Minimized the app ";
            //    MyNotifyIcon.ShowBalloonTip(400);
            //    MyNotifyIcon.Visible = true;
            //}
            //else if (this.WindowState == WindowState.Normal)
            //{
            //    MyNotifyIcon.Visible = false;
            //    this.ShowInTaskbar = true;
            //    this.Show();
            //}

        }




        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            content_container.Source = new Uri("Ui_Settings.xaml", UriKind.RelativeOrAbsolute);
        }




        private void LibraryButton_Click(object sender, RoutedEventArgs e)
        {
            content_container.Source = new Uri("Ui_GeneralData.xaml", UriKind.RelativeOrAbsolute);
        }

        private void UploadButton_Click(object sender, RoutedEventArgs e)
        {
            content_container.Source = new Uri("Ui_Sending.xaml", UriKind.RelativeOrAbsolute);
        }

        private void DownloadButton_Click(object sender, RoutedEventArgs e)
        {
            content_container.Source = new Uri("Ui_Downloading.xaml", UriKind.RelativeOrAbsolute);
        }



        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            content_container.Source = new Uri("Ui_GeneralData.xaml", UriKind.RelativeOrAbsolute);
        }



        private void Help_Click(object sender, RoutedEventArgs e)
        {

            content_container.Source = new Uri("Ui_HelpPage.xaml", UriKind.RelativeOrAbsolute);
        }

        private void UserAccount_Click(object sender, RoutedEventArgs e)
        {
            content_container.Source = new Uri("Ui_Account.xaml", UriKind.RelativeOrAbsolute);
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            content_container.Source = new Uri("Ui_AttachFile.xaml", UriKind.RelativeOrAbsolute);
        }

        private void Inbox_Click(object sender, RoutedEventArgs e)
        {
            content_container.Source = new Uri("Ui_Inbox.xaml", UriKind.RelativeOrAbsolute);
        }

   


        private readonly NamedPipeClient<string> AraizenServicePipeNotifications = new NamedPipeClient<string>(App_Const.PipeNameNotification);
       

        WebSocket userNotification = new WebSocket(App_Url.NotificationItemWebSocketConnectionURL());

        private bool isWebscketRunning = false;
        #region MetroWindow_Loaded
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //start the client for comms with araizen service
          
            //notification comms
            AraizenServicePipeNotifications.ServerMessage += OnAraizenServiceNotificationConnectionMessage;
            AraizenServicePipeNotifications.Disconnected += OnAraizenServiceNotficationConnectionDisconnected;
            AraizenServicePipeNotifications.Start();
     
            
           

            var th = new Task(() =>
            {
                while (true)
                {
                    var IsThereInterent = App_System_Wifi.CheckForInternetConnection();
                    try
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            if (IsThereInterent)
                            {
                                //if websocket is not running
                                if (isWebscketRunning == false)
                                {
                                    //start websocket connection
                                    StartWebsocketConnection();
                                }
                                //primitive post data then expect response
                                try
                                {
                                    PostLastNotificationExpectNullOrnewNotification();
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("eror in PostLastNotificationExpectNullOrnewNotification " + ex);
                                }
                            }
                            else
                            {
                                Console.WriteLine("No internet connetion");
                            }
                        });
                    }
                    catch (Exception err)
                    {
                        Console.WriteLine("internal error"+ err);
                    }
                    Thread.Sleep(10500);
                }
            });
            th.Start();




           
        }
        #endregion

        #region MetroWindow_Closed
        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            userNotification.Close();
            System.Windows.Application.Current.Shutdown();
        }
        #endregion

        #region StartWebsocketConnection
        private void StartWebsocketConnection()
        {
            isWebscketRunning = true;
            userNotification.OnOpen += (sender, e) =>
            {
                //todo
                App_Notification_Sql notification_Sql = new App_Notification_Sql();
                App_Notification note = notification_Sql.GetLastnotificationItemFromDatabase();

                string json_str = JsonConvert.SerializeObject(note);
                userNotification.Send(json_str);
            };

            userNotification.OnMessage += (sender, e) =>
                {
                    Console.WriteLine("Message  says: " + e.Data);

                    try
                    {
                        App_Notification  notify = JsonConvert.DeserializeObject<App_Notification>(e.Data);

                        App_Notification_Sql notifySql = new App_Notification_Sql();
                        notifySql.InsertnotificationItemsDatabase(notify);


                    }
                    catch
                    {
                        Console.WriteLine("\n\n\n\n-------------Erroro decoding websocket  data-----------------------\n\n\n");
                       
                    }
                };

            
            userNotification.OnError += (sender, e) =>
            {
                AttemptReconnection();
                isWebscketRunning = false;
            };

          //  userNotification.Connect();  
            

        }
        #endregion

        #region AttemptReconnection
        private void AttemptReconnection()
        {
            var th = new Task(() =>
            {
                while (true)
                {
                    var IsThereInterent = App_System_Wifi.CheckForInternetConnection();
                    try
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            if (IsThereInterent)
                            {
                                //if websocket is not running
                                if (isWebscketRunning == false)
                                {
                                    //start websocket connection
                                    StartWebsocketConnection();
                                }
                            }
                            else
                            {
                                Console.WriteLine("No internet connetion");
                            }
                        });
                    }
                    catch (Exception err)
                    {
                        Console.WriteLine("internal error" + err);
                    }
                    Thread.Sleep(4000);
                }
            });
            th.Start();
        }
        #endregion

       

     

        #region PostLastNotificationExpectNullOrnewNotification
        private void PostLastNotificationExpectNullOrnewNotification()
        {
            //fetch last notification
            App_Notification_Sql notificationSQL = new App_Notification_Sql();
            //var lastNotification =   notificationSQL.GetLastnotificationItemFromDatabase();

            App_Notifications_Server notificationServer = new App_Notifications_Server();
            List<App_Notification> newNotification = null ;

            try
            {
                newNotification = notificationServer.GetLatestNotificationFromServer(authData.Email, authData.Name);
            }catch (Exception err)
            {
                Console.WriteLine("Unable to contact araize share server {0}", err);
            }

            //check if null
            if(newNotification != null)
            {
                //insert new notification
                //TODO play a sound
                var soundPath = new Uri(@"/Resources/images/ic_delete_black_18dp.png", UriKind.Relative);
                SoundPlayer player = new SoundPlayer(soundPath.ToString());
                player.Load();
                player.Play();

                foreach (var newItemNotification in newNotification)
                {
                    notificationSQL.InsertnotificationItemsDatabase(newItemNotification);
                }
            }


        }
        #endregion

        #region
        private void PlayNotificationSound()
        {
            Console.WriteLine("\n\n------------Play the sound --------------\n\n");
            Uri uri = new Uri(@"pack://application:,,,/Resources/Sounds/message_sound_1.mp3");
            Console.WriteLine("\n\n------------Play the sound ------------{0}--\n\n", uri);
            var player = new MediaPlayer();
            player.Open(uri);
            player.Play();
        }
        #endregion
        /**
         * 
         * connection between ui and araizen service
         * 
         * starting with 
         *      1) Notification system for araizen
         *      2) Upload system for araizen
         *      3)Download system for araizen
         */


        #region Notifcation comms components
        #region OnAraizenServiceNotificationConnectionMessage
        private void OnAraizenServiceNotificationConnectionMessage(NamedPipeConnection<string, string> connection, string message)
        {
            Console.WriteLine("file message {0}", message);
           
        }
        #endregion

        #region OnAraizenServiceNotficationConnectionDisconnected
        private void OnAraizenServiceNotficationConnectionDisconnected(NamedPipeConnection<string, string> connection)
        {
            Console.WriteLine("<b>Disconnected from server</b>");

        }
        #endregion

        #region AraizenServiceNotificationSendMessage
        public void AraizenServiceNotificationSendMessage(App_Notification notification)
        {
            string json_str = JsonConvert.SerializeObject(notification);
            AraizenServicePipeNotifications.PushMessage(json_str);

        }
        #endregion
        #endregion



       
    }
}
