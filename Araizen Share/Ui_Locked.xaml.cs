﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_Locked.xaml
    /// </summary>
    public partial class Ui_Locked : Window
    {
        public Ui_Locked()
        {
            InitializeComponent();
            Reason.Text = Araizen_Share.Properties.Settings.Default.Reason_Locked;
        }
        public Ui_Locked(string reasonstr)
        {
            InitializeComponent();

            Reason.Text = reasonstr;
        }

        private void okay_Click(object sender, RoutedEventArgs e)
        {

            System.Windows.Application.Current.Shutdown();
        }

        private void Unlock_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Ui_LockedSerial sh = new Ui_LockedSerial(KillOnexit:true);
            sh.Show();
        }
    }
}
