﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for logout_dialogue.xaml
    /// </summary>
    public partial class Ui_LogoutDialogue : MetroWindow
    {
       
        public Ui_LogoutDialogue()
        {
            InitializeComponent();           
        }
      
        bool Proceed = true;

        public bool DoWeProceedToLogOut
        {
            get { return Proceed; }
        }

        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Proceed = false;
            this.Close();
        }

        private void Procced_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
