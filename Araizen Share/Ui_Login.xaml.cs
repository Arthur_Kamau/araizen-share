﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for login.xaml
    /// </summary>
    public partial class Ui_Login : MetroWindow
    {
        
        int attempts = 0;
      

        public Ui_Login()
        {

            //get if app is registered 
            //redudancy from splash screen
            App_Resource res = new App_Resource();
            // App_Register regDetails = res.GetRegisterDetails();
            App_Registry reg = new App_Registry();

            ///get if is blocked b4 regoster or login
            string app_blocked = string.Empty;
            try
            {
                app_blocked = reg.GetAppIsLockedOrNot();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            if (0 < app_blocked.Length)
            {
                this.Close();
                Ui_Locked locks = new Ui_Locked();
                locks.Show();

            }
            else
            {
                

                InitializeComponent();
                var th = new Task(() =>
                {
                    while (true)
                    {
                        var IsThereInterent = App_System_Wifi.CheckForInternetConnection();
                        try
                        {
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                if (IsThereInterent)
                                {
                                    Submit.IsEnabled = true;
                                    //Status.Foreground = new SolidColorBrush(Colors.Green);
                                   // Internetstatus.Content = "There Is Internet,Proceed.";
                                }
                                else
                                {
                                    Submit.IsEnabled = false;
                                    Status.Foreground = new SolidColorBrush(Colors.Red);
                                    Status.Content = "Connect to Internet to proceed";
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("internal error");
                        }
                        Thread.Sleep(1500);
                    }
                });
                th.Start();


            }
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            LoadingProgressring.Opacity = 1;
            contentsContainer.Opacity = 0;

            string emailstr = email.Text;
            string passwordstr = password.Password.ToString();
            App_Auth_Credential credentials = new App_Auth_Credential(password:passwordstr,email:emailstr);

            App_Auth_File auth = new App_Auth_File();
            App_Auth_Credential_Result  detail= auth.GetAuthDetailsServer(credentials);
            App_Install_Register ireg = new App_Install_Register();



            if (detail == null)
            {

                Status.Content = "Invalid email password combination,try again";

                LoadingProgressring.Opacity = 0;
                contentsContainer.Opacity = 1;
            }
            else
            {
                Console.WriteLine("\n\n Ui ------> the result is authenticated {0} ", detail.isAuthenticated);
                if (detail.isAuthenticated == true)
                {

                    auth.SetAuthDetailsFile(
                    userInstallAppDate: detail.AuthData.InstallTime,
                    logInTime: DateTime.Now,
                    name: detail.AuthData.Name,
                    accountType: detail.AuthData.AccountType,
                    token: detail.AuthData.Token,
                    email: detail.AuthData.Email
               );



                    ireg.SetAppInstallFile(
                         installTimePar: detail.AuthData.InstallTime,
                         macAddressPar: App_System_Details.GetMacAddress()
                        );


                    Main_Window main_window = new Main_Window();
                    main_window.Show();
                    this.Close();
                }
                else
                {
                    if (detail.attempt > 5)
                    {
                        System.Windows.Application.Current.Shutdown();
                    }
                    Status.Content = "Invalid email password combination,try again";
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        #region 
        private bool IsUserTrialLicenceExpired()
        {
            //get install details from file
            //if null query araizen server
            //if days pas installed past set time
            //return true

            App_Install_Register instReg = new App_Install_Register();
            App_Install installData = instReg.GetAppInstallDetailsServer();

            DateTime installDate = installData.InstallTime;

            if (installDate.AddDays(App_Const.MaxTrialDays) < DateTime.Now)
            {
                //not expired
                return false;
            }
            else if (installDate.AddDays(App_Const.MaxTrialDays) == DateTime.Now)
            {
                return false;
            }
            else
            {
                return true;
            }


        }
        #endregion



        #region OpenMainWindow
        private void OpenMainWindow()
        {
           Main_Window mainWindow = new Main_Window();
            mainWindow.Show();
            this.Close();
            
        }
        #endregion

       

        #region Forgot Password
        private void ForgotPassword_Click(object sender, RoutedEventArgs e)
        {

            Console.WriteLine("ForgotPassword_Click");
            //Ui_Log mfe = new Ui_ForgotPassword_EnterEmail();
            Ui_Login_ForgotPassword mfe = new Ui_Login_ForgotPassword();
            mfe.Show();

            this.Close();
        }
        #endregion

        #region window closing
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

          //  System.Windows.Application.Current.Shutdown();
        }
        #endregion

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Ui_LoginRegisterOption loginRegister = new Ui_LoginRegisterOption();
            loginRegister.Show();
            this.Close();

        }
    }
}