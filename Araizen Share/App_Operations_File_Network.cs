﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Operations_File_Network : App_Observer_Subject
    {


        public void DownloadFile(String uriString,string fileNamePath)
        {
            using (var client = new WebClient())
            {
                client.DownloadFile( uriString, fileNamePath);
            }
        }
        public void DownloadFileProgressTracker(String uriString, string fileNamePath)
        {
         
            using (WebClient wc = new WebClient())
            {
                wc.DownloadProgressChanged += wc_DownloadProgressChanged;
                wc.DownloadFileAsync(
                    // Param1 = Link of file
                    new System.Uri(uriString),
                   // Param2 = Path to save
                   fileNamePath
                );
            }
        }

        // Event to track the progress
        void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            //progressBar.Value = e.ProgressPercentage;
            Console.WriteLine("Progress"+ e.ProgressPercentage);
        }


        public void UploadFile(String uriString, string fileNamePath)
        {
            Console.Write("\n the URI to post data to : "+uriString);
           

            // Create a new WebClient instance.
            WebClient myWebClient = new WebClient();

            Console.WriteLine("\nPlease enter the fully qualified path of the file to be uploaded to the URI");
           
            Console.WriteLine("Uploading {0} to {1} ...", fileNamePath, uriString);

            // Upload the file to the URI.
            // The 'UploadFile(uriString,fileNamePath)' method implicitly uses HTTP POST method.
            byte[] responseArray = myWebClient.UploadFile(uriString, fileNamePath);

            // Decode and display the response.
            Console.WriteLine("\nResponse Received.The contents of the file uploaded are:\n{0}",
                System.Text.Encoding.ASCII.GetString(responseArray));
        }

        public void RegisterObserver(App_Observer observer)
        {
            throw new NotImplementedException();
        }

        public void UnregisterObserver(App_Observer observer)
        {
            throw new NotImplementedException();
        }

        public void NotifyObservers(int count)
        {
            throw new NotImplementedException();
        }
    }
}
