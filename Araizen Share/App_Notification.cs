﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
  public   class App_Notification
    {

        /**
         * unseen =1
         * seen =0
         * 
 
         * Notification_type 
         * 
         * document
         * url
         * alert
         * danger
         * other
         * 
         */

        public string Notfication_from_name { get; set; }
        public string Notfication_from_email { get; set; }
        public string Notfication_dest_email { get; set; }
        public string Notification_type { get; set; }
        public string Notification_title { get; set; }
        public string Notification_description { get; set; }
        public string Notification_seen { get; set; }
        public string Notification_expired { get; set; }
        public string Notification_date { get; set; }
        public int Notification_foreign_key_for_download { get; set; }
        public string Notofication_url { get; set; }

        public App_Notification(string notfication_from_name, string notfication_from_email, string notfication_dest_email, string notification_type, string notification_title, string notification_description, string notification_seen, string notification_expired, string notification_date, int notification_foreign_key_for_download, string notofication_url)
        {
            Notfication_from_name = notfication_from_name;
            Notfication_from_email = notfication_from_email;
            Notfication_dest_email = notfication_dest_email;
            Notification_type = notification_type;
            Notification_title = notification_title;
            Notification_description = notification_description;
            Notification_seen = notification_seen;
            Notification_expired = notification_expired;
            Notification_date = notification_date;
            Notification_foreign_key_for_download = notification_foreign_key_for_download;
            Notofication_url = notofication_url;
        }
    }

    public class App_Download_Data_From_Notification
    {
        public bool IsOkay { get; set; }
        public string Reason { get; set; }
        public App_Download Data { get; set; }

        public App_Download_Data_From_Notification(bool isOkay, string reason, App_Download data)
        {
            IsOkay = isOkay;
            Reason = reason;
            Data = data;
        }
    }
}
