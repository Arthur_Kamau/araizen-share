﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            System.Windows.MessageBox.Show("Critical error \n Restart The Application \n \n " + e.Exception.ToString());

            //restart the application

            System.Windows.Application.Current.Shutdown();
            //System.Windows.Forms.Application.Restart();
        }
    }
}
