﻿using MahApps.Metro.Controls;
using NamedPipeWrapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_AttachFile_Dialogue.xaml
    /// </summary>
    public partial class Ui_AttachFile_Dialogue : MetroWindow
    {
        List<string> uploadUserSelectedFilesList = new List<string>();
        int loopCounter = 0;
        int totalCounts = 0;

        List<App_Upload_File_Item> file_items_for_upload = new List<App_Upload_File_Item>();

        String userDescription = String.Empty;
       // List<string> userTargetMail = new List<string>();
        string[] userTargetMailArray;
        long unixTime;

        private readonly NamedPipeClient<string> AraizenServicePipeUploads = new NamedPipeClient<string>(App_Const.PipeNameUpload);

        string uploadDate = DateTime.Now.Date.ToString();

        public Ui_AttachFile_Dialogue(List<String> uploadItems, String description, string[] target_mail)
        {
            InitializeComponent();

            //comms with araizen service 
            // to send upload objec

            AraizenServicePipeUploads.ServerMessage += OnAraizenServiceUploadConnectionMessage;
            AraizenServicePipeUploads.Disconnected += OnAraizenServiceUploadConnectionDisconnected;
            AraizenServicePipeUploads.Start();


            //GetHashCode unix timestamp 
            long epochTicks = new DateTime(1970, 1, 1).Ticks;
            unixTime = ((DateTime.UtcNow.Ticks - epochTicks) / TimeSpan.TicksPerSecond);

            //insert into class list
            uploadUserSelectedFilesList.AddRange(uploadItems);

            totalCounts = uploadItems.Count;

            //user data description and target emails
            userDescription = description;
            userTargetMailArray = target_mail;



            if (uploadUserSelectedFilesList != null) { 
                Thread th = new Thread(() =>
                {
                    PrepareForUploadFiles(uploadUserSelectedFilesList);
                });
            th.Priority = ThreadPriority.Highest;
            th.Start();
            }
            else
            {
                this.Close();
                string errorText = "    Woops an error occured,try again or restart app.";
                Ui_AttachFile_Error_Dialogue errDialogue = new Ui_AttachFile_Error_Dialogue(errorText: errorText);
                this.Show();
            }
           

            

        }
        private void PrepareForUploadFiles(List<string> uploadUserSelectedFilesList)
        {

            CopySelectedFilesThenSplitSelectedFiles(uploadUserSelectedFilesList);
            SaveInDatabaseAndSendInfoToServer(uploadUserSelectedFilesList);

            //close ui
            Dispatcher.Invoke((Action)delegate
            {
                //open change pass
                this.Close();
            });

        }

        private void CopySelectedFilesThenSplitSelectedFiles(List<string> uploadUserSelectedFilesList)
        {
            //get path to araizen share operations
            App_System_Paths ally_paths = new App_System_Paths();
            string shareOperations = ally_paths.ShareUploadFolder();

            int counter = 0;
            int totalFiles = uploadUserSelectedFilesList.Count;

            Dispatcher.Invoke((Action)delegate
            {
                //set minumum and maximum of the files selected
                progress_bar.Minimum = 0;
               progress_bar.Maximum = totalFiles;
            });


           
            foreach (string file_item in uploadUserSelectedFilesList)
                {

               
                
                    //get operation path + file name
                    String name = System.IO.Path.GetFileName(file_item);
                    String nameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(file_item);
                    string specificFolder = System.IO.Path.Combine(shareOperations, nameWithoutExtension);

                
                    //update ui working on
                    Dispatcher.Invoke((Action)delegate
                    {
                        //set minumum and maximum of the files selected
                        ItemName.Content = name;

                    });

                    //file properties
                    long Filelength = new System.IO.FileInfo(file_item).Length;
                    String ext = System.IO.Path.GetExtension(file_item);

                   
                    //file name + path which is to be split
                    String fileCopiedWhichIsToBeSplitThenDeleted = System.IO.Path.Combine(specificFolder, name);

                    //check if a folder exist in araizen share uplodas folder
                    // with the name of the file(without extensntion) if folder does not exxist 
                    //create folder
                    bool exists = Directory.Exists(specificFolder);
                    if (!exists)
                    {
                        Directory.CreateDirectory(specificFolder);
                    }

                    //check if file exists
                    //in araizen share /uploads/file name folder
                    //if a file exist with the same name as the one user has selected delete it 
                    // or any folders in the folder with the same name as the one user has selected
                    // delete them all
                    //then copy the one the user has selected

                    if (File.Exists(System.IO.Path.Combine(specificFolder, name)))
                    {

                        System.IO.DirectoryInfo di = new DirectoryInfo(System.IO.Path.Combine(specificFolder, name));
                        //delete all files
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                        //delete all dir
                        foreach (DirectoryInfo dir in di.GetDirectories())
                        {
                            dir.Delete(true);
                        }
                    }


                    //if not exit copy it
                    //coppy file to the specific folder
                    Console.WriteLine("\n\n ++++++++++ copy this  {0} to upload center {1} \n\n +++++++++", file_item, System.IO.Path.Combine(specificFolder, name));
                    File.Copy(file_item, System.IO.Path.Combine(specificFolder, name));

                    Console.WriteLine("\n\n split {0} to chanks {1}  and {2}", fileCopiedWhichIsToBeSplitThenDeleted, (((totalCounts == 0 ? 1 : totalCounts) * 100) / (loopCounter == 0 ? 1 : loopCounter)), name + "is being splite");


                //check if the file is less than the minimum size
                //if less than minimum size do not split
                //just name it to 0
                //else split 
                int numberOfFiles;
                if (Filelength < App_Const.FileSplitSize())
                {
                    numberOfFiles = 1;

                    System.IO.File.Move(fileCopiedWhichIsToBeSplitThenDeleted, System.IO.Path.Combine(specificFolder,"0"));
                }
                {
                    //number of split files
                     numberOfFiles = (int)Math.Ceiling((double)Filelength / App_Const.FileSplitSize());
                    App_Operations_File.SplitFile(inputFile: fileCopiedWhichIsToBeSplitThenDeleted, path: specificFolder, chunkSize: App_Const.FileSplitSize());

                    //delete copied file file
                    try
                    {
                        File.Delete(path: fileCopiedWhichIsToBeSplitThenDeleted);
                    }
                    catch
                    {
                        Console.WriteLine("An error occured while deleting file that was split in araizen share folder");
                    }

                }



               


                    //add to list
                    App_Upload_File_Item a_file_for_upload = new App_Upload_File_Item(
                         fileName: nameWithoutExtension, fileSize: Filelength, fileExt: ext,
                         fileEachItemSize: App_Const.FileSplitSize(), totalUploadItems : numberOfFiles, currentUploadItem: numberOfFiles, fileDownloadUrl: "",
                         uploadDate: uploadDate, unixTimeFileItemReferenceId: unixTime, fileLocation: specificFolder, isStarted: false, isCompleted: false, fileEachItemLocation: specificFolder
                        );

                    file_items_for_upload.Add(a_file_for_upload);

                    Console.WriteLine("\n\n list container of all selected files {0} \n\n\n", file_items_for_upload.Count);

                Dispatcher.Invoke((Action)delegate
                {
                    //set minumum and maximum of the files selected
                    progress_bar.Value = counter;
                    ItemNameNumber.Content = string.Format("{0}/{1}", counter , totalFiles);
                });

                counter++;
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadUserSelectedFilesList"></param>
        #region SaveInDatabaseAndSendInfoToServer
        private void SaveInDatabaseAndSendInfoToServer(List<string> uploadUserSelectedFilesList)
        {
            App_Auth_File authFile = new App_Auth_File();
            App_Resource res = new App_Resource();
            String machine = App_System_Details.GetComputerName();
            App_Auth authDetails = authFile.GetAuthDetailsFile();


            //create final upload object
            App_Upload file_for_upload = new App_Upload(
                sourceEmail: authDetails.Email,
                sourceName: authDetails.Name,
                destinationemails: userTargetMailArray.ToList<string>(),
                description: userDescription,
                uploadTime: uploadDate,
                unixFileTimeReferenceId: unixTime,
                fileMachineUpload: machine,
                fileItem: file_items_for_upload
                );


            //save  upload object to databse
            App_Upload_Sql uploadsql = new App_Upload_Sql();
            uploadsql.InsertUploadItemsDatabase(uploadItem: file_for_upload);

            Console.WriteLine("\n\n ++++++++++ inserted to sql \n\n +++++++++");
            //send item to server
            App_Upload_Server_Communication uploadServer = new App_Upload_Server_Communication();
            string responseFromServer = uploadServer.UploadFileShareObject(uploadObject: file_for_upload);

            Console.WriteLine("\n\n ++++++++++ upload server reposnse {0}  \n\n +++++++++", responseFromServer);
            //send to service
            //send meesage
            AraizenServiceUploadSendMessage(file_for_upload);
        }
        #endregion


      
       

      
        #region Upload comms components
        #region OnAraizenServiceNotificationConnectionMessage
        private void OnAraizenServiceUploadConnectionMessage(NamedPipeConnection<string, string> connection, string message)
        {
            Console.WriteLine("file message {0}", message);


        }
        #endregion

        #region OnAraizenServiceUploadConnectionDisconnected
        private void OnAraizenServiceUploadConnectionDisconnected(NamedPipeConnection<string, string> connection)
        {
            Console.WriteLine("<b>Disconnected from server</b>");

        }
        #endregion

        #region AraizenServiceUploadSendMessage
        public void AraizenServiceUploadSendMessage(App_Upload uploadItem)
        {

            string json_str = JsonConvert.SerializeObject(uploadItem);

            Console.WriteLine("\n\n ++++++++++ sending json str to service {0}  \n\n +++++++++", json_str);
            AraizenServicePipeUploads.PushMessage(json_str);


        }
        #endregion
        #endregion
    }
}
