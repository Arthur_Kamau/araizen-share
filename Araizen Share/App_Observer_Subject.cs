﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    interface App_Observer_Subject
    {
        void RegisterObserver(App_Observer observer);
        void UnregisterObserver(App_Observer observer);
        void NotifyObservers(int count);
    }
}
