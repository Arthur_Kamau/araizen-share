﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_AttachFile_Error_Dialogue.xaml
    /// </summary>
    public partial class Ui_AttachFile_Error_Dialogue : MetroWindow
    {
        public Ui_AttachFile_Error_Dialogue(string errorText)
        {
            InitializeComponent();

            error_text_block.Text = errorText;

        }
        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
