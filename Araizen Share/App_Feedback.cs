﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Feedback
    {

        #region SetAppInstallServer
        public bool SendAppUserFeedBack(App_Feedback_Items feedBack)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.FeedbacksUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(feedBack);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);
                if (result.Equals("ërr"))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        #endregion
    }

   public class App_Feedback_Items
    {
        public String FeedbackText  { get; set; }
        public String UserEmail { get; set; }
        public DateTime FeedbackTime { get; set; }

        public App_Feedback_Items(string feedbackText, string userEmail, DateTime feedbackTime)
        {
            FeedbackText = feedbackText;
            UserEmail = userEmail;
            FeedbackTime = feedbackTime;
        }
    }
}
