﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_Error.xaml
    /// </summary>
    public partial class Ui_Error : MetroWindow
    {
        public Ui_Error(string title,string body)
        {
            InitializeComponent();


            //set the contents

            errorTitle.Content = title;
            error_text_block.Text = body;
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
