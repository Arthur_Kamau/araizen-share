﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace Araizen_Share
{
    class App_History_Sql
    { /// <summary>
      /// 
      /// </summary>
      /// <param name="path"></param>
        #region CreateHistoryItemsDatabase
        public void CreateHistoryItemsDatabase(string path)
        {

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + path))
            {
                sqlConnection.Open();

              
        string sql_item = @"
                    CREATE TABLE IF NOT EXISTS history_items 
                    (
                      id INTEGER PRIMARY KEY AUTOINCREMENT,

                      Action varchar(255) ,
                      FileName varchar(255) ,

                      FileType varchar(255) ,
                      FileSize varchar(255) ,

                      DestinationEmail varchar(255) ,
                      SourceEmail varchar(255) ,
                      CompletionDate varchar(255) ,
                      UnixTimeFileItemReferenceId varchar(255) 
  
                    )";

                SQLiteCommand command_item = new SQLiteCommand(sql_item, sqlConnection);
                command_item.ExecuteNonQuery();

            }

        }
        #endregion


        /*
         *******************************************************************************************
         * 
         * insert functions
         * 
         * *******************************************************************************************
         */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="historyItem"></param>
        #region InsertHistoryItemsDatabase
        public void InsertHistoryItemsDatabase(App_History historyItem)
        {

            App_System_Paths paths = new App_System_Paths();
            string historyItemPath = paths.HistorySQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + historyItemPath))
            {

                //if it exist ignore
                Console.WriteLine("\n\n UnixTimeFileItemReferenceId --->> {0}", historyItem.UnixTimeFileItemReferenceId);


                //continue to insert
                sqlConnection.Open();
                string sqlQuery = string.Format(
           @"INSERT INTO history_items
            (
                      Action   ,
                      FileName   ,

                      FileType   ,
                      FileSize   ,

                      DestinationEmail   ,
                      SourceEmail   ,
                      CompletionDate   ,
                      UnixTimeFileItemReferenceId    
            )
            VALUES
            (
                '{0}', '{1}',
                '{2}', '{3}',
                '{4}', '{5}',
                '{6}', '{7}'
            ); 
            ",
            historyItem.Action, historyItem.FileName,
             historyItem.FileType, historyItem.FileSize,
            historyItem.DestinationEmail,historyItem.SourceEmail,
            historyItem.CompletionDate, historyItem.UnixTimeFileItemReferenceId
            


          );



                Console.WriteLine("\n\n --->> {0}", sqlQuery);
                //sqlCommand = sqlConnection.CreateCommand();
                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();
            }
        }
        #endregion


        /*
         *******************************************************************************************
         * 
         * Get functions
         * 
         * *******************************************************************************************
         */
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        #region GetHistoryItemsDatabase
        public List<App_History> GetHistoryItemsDatabase()
        {
            App_System_Paths paths = new App_System_Paths();
            string historyItemPath = paths.HistorySQL();
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=" + historyItemPath))
            {
                conn.Open();

                SQLiteCommand command = new SQLiteCommand(@"Select    
                      Action   ,
                      FileName   ,
                      FileType   ,
                      FileSize   ,
                      DestinationEmail   ,
                      SourceEmail   ,
                      CompletionDate   ,
                      UnixTimeFileItemReferenceId    

                       from history_items order by id", conn);
                SQLiteDataReader reader = command.ExecuteReader();

                List<App_History> history_items = new List<App_History>();

                while (reader.Read())
                {
                    string userAction = reader["Action"].ToString();
                    string userFileName = reader["FileName"].ToString();
                    string userFileType = reader["FileType"].ToString();
                    string userFileSize = reader["FileSize"].ToString();
                    string userDestinationEmail = reader["DestinationEmail"].ToString();
                    string userSourceEmail = reader["SourceEmail"].ToString();
                    DateTime userCompletionDate = Convert.ToDateTime(reader["CompletionDate"].ToString());
                    string userUnixTimeFileItemReferenceId = reader["UnixTimeFileItemReferenceId"].ToString();


                    App_History downloadFile = new App_History(

                      action: userAction,
                      fileName:  userFileName ,
                      fileType: userFileType ,
                      fileSize: userFileSize ,
                      destinationEmail: userDestinationEmail  ,
                      sourceEmail: userSourceEmail ,
                      completionDate: userCompletionDate ,
                      unixTimeFileItemReferenceId :   userUnixTimeFileItemReferenceId

                    );

                    history_items.Add(downloadFile);
                }
                reader.Close();

                return history_items;
            }
        }
        #endregion

        /*
       *******************************************************************************************
       * 
       * Update functions
       * 
       * *******************************************************************************************
       */
      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="file_name"></param>
        #region UpdateDownloadFileItemsNumberDatabase
        public void UpdateDownloadFileItemsNumberDatabase(string value, string file_name)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.UploadSQL();

            //    sqlConnection = new SQLiteConnection("Data Source="+uploadItemPath+";Version=3;");
            //    sqlConnection.Open();
            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();


                string sqlQuery = string.Format(

                    @"UPDATE history_items 
                    SET CurrentUploadItem = '{0}',  
                    WHERE FileName = '{1}'

                    ", value, file_name
                    );

                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();


            }
        }
        #endregion

        /*
         *******************************************************************************************
         * 
         * Delete functions
         * 
         * *******************************************************************************************
         */


        /// <summary>
        /// delette item
        /// </summary>
        /// <param name="UnixTimeFileItemReferenceId"></param>
        #region DeleteHistoryItemsDatabase
        public void DeleteHistoryItemsDatabase(string fileName,string senderEmail,string recepientEmail)
        {
            App_System_Paths paths = new App_System_Paths();
            string historyItemPath = paths.HistorySQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + historyItemPath))
            {
                sqlConnection.Open();
                //delete item
                string sqlQuery = string.Format(
                    @"DELETE FROM history_items WHERE 


                      FileName = '{0}' AND

                      DestinationEmail  = '{1}' AND
                      SourceEmail  = '{2}' 
",
                    fileName,recepientEmail,senderEmail);
                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();
            }
        }
        #endregion

        
    }
}
