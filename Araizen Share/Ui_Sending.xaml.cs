﻿using MahApps.Metro.Controls;
using NamedPipeWrapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Sharing.xaml
    /// </summary>
    public partial class Ui_Sending : Page
    {
        //List<string> item_list = new List<string>();


        //App_System_Communication ac = new App_System_Communication();
        //App_System_Paths path = new App_System_Paths();
        App_Upload_Sql upload_sql = new App_Upload_Sql();
        List<App_Upload> uploadingItems;
        List<String> filesInAraizenShareFolder;
        //List<String> uploadDirItems;
        //List<String> itemsInUploadDirNotInJson;

        App_System_Paths paths = new App_System_Paths();
        //String pathToUploadDb = string.Empty;
        string [] charsToRemoveForValidName = new string[] { "@","#", ",", ".", ";", "'","-","_","\\", "/","  "};
        App_System_Paths ally_paths = new App_System_Paths();

        //pub  model
        App_Upload_Pub app_upload_pub = new App_Upload_Pub();

        private readonly NamedPipeClient<string> AraizenServicePipeUploads = new NamedPipeClient<string>(App_Const.PipeNameUpload);
        //use this constructr for default page loading
        public Ui_Sending()
        {
            Console.WriteLine("\n\n\n  ----------------->>>>>> constructor ui_sending  \n\n\n");

           InitializeComponent();

            //populate items ui
           ParseSendingObjectsToUi();

            //upload comms
            AraizenServicePipeUploads.ServerMessage += OnAraizenServiceUploadConnectionMessage;
            AraizenServicePipeUploads.Disconnected += OnAraizenServiceUploadConnectionDisconnected;
            AraizenServicePipeUploads.Start();


        }
       

        private List<String> getAllFilesinAraizenShareFolder()
        {
            //    //upload dir  items(children
            List<String> folderNames = new List<string>();

            string shareOperations = ally_paths.ShareUploadFolder();



            String[] share_folder_contents = Directory.GetDirectories(shareOperations);

            List<string> folder_paths=  share_folder_contents.ToList<string>();

            foreach (string folderPath in folder_paths)
            {
                DirectoryInfo dir = new DirectoryInfo(folderPath);
 

                Console.WriteLine("\n\n getting path {0} get the name {1} \n\n ",folderPath,dir.Name);
                folderNames.Add(dir.Name);
            }

            return folderNames;
        }

        private void ParseSendingObjectsToUi()
        {
            //initializethe functions
            //path to db
           // pathToUploadDb = paths.UploadSQL();

            //list from db
            uploadingItems = upload_sql.GetAllUploadItemsDatabase();

            //update ui that show empty 
            if (uploadingItems != null && uploadingItems.Count > 0)
            {
                content_infomation.Opacity = 0;
            }

            filesInAraizenShareFolder = getAllFilesinAraizenShareFolder();
            Console.WriteLine("result from database count = {0}", uploadingItems.Count);
           
           for(int i =0;i <uploadingItems.Count; i++)
            {
                    Console.WriteLine("\n\n App_Upload loop line 110  destiation {0} \n  in loop {1} \n\n ", uploadingItems[i].DestinationEmailClients,i);
                    SortParsedSendingFileItemsToUi(uploadObjFileItem: uploadingItems[i].FileItem, destinationEmail : uploadingItems[i].DestinationEmailClients);

            }
           

        }

        private void SortParsedSendingFileItemsToUi(List<App_Upload_File_Item> uploadObjFileItem, List<string> destinationEmail)
        {
           
            foreach (App_Upload_File_Item fileItem  in uploadObjFileItem)
            {
                Console.WriteLine("\n\n in SortParsedSendingFileItemsToUi loop \n\n ");
                RenderUi(
                    name: fileItem.FileName,
                    destination: destinationEmail,
                    type: fileItem.FileExt,
                    size: fileItem.FileSize,
                    current_upload_item_counterInt: fileItem.CurrentUploadItem,
                    fileItemKey : GetFileItemKey(fileItem.UnixTimeFileItemReferenceId.ToString(), fileItem.FileName),
                    unixTimeFileItemReferenceId : fileItem.UnixTimeFileItemReferenceId.ToString(),
                      isStarted : fileItem.IsStarted, 
                      isCompleted : fileItem.IsCompleted
                    );
            }
        }

        string GetFileItemKey(string unixTime, string fileName)
        {
           string originalName=   string.Concat(fileName, unixTime);

            //valid name charsToRemoveForValidName
         string filterSpecialCharacters=   new string((from c in originalName
                                                       where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                                        select c
       ).ToArray());


            string filterSpacesAndSpecialCharacters = filterSpecialCharacters.Replace(" ", string.Empty);

            return filterSpacesAndSpecialCharacters;

        }

        /// <summary>
        /// add folder items to ui  -->Add_items_to_ui(String name,String owner,String type,long size)
        /// </summary>
        /// <param name="name"></param>
        /// <param name="owner"></param>
        /// <param name="type"></param>
        /// <param name="size"></param>
        /// <param name="target_mail"></param>
        /// <param name="counterInt"></param>
        /// <param name="unixTimeFileItemReferenceId"></param> 
        #region Add_items_to_ui
        private void RenderUi(String name, List<String> destination, String type, long size, int current_upload_item_counterInt, string fileItemKey,string unixTimeFileItemReferenceId,bool  isStarted, bool isCompleted)
        {

            Console.WriteLine("\n\n in RenderUi loop \n\n ");
            App_System_Paths asps = new App_System_Paths();

            Grid item_grid_container = new Grid();
            StackPanel item_grid = new StackPanel();
            StackPanel image_and_comment = new StackPanel();
            StackPanel comments = new StackPanel();

            StackPanel item_details = new StackPanel();
            StackPanel item_details_description = new StackPanel();
            StackPanel download_stream = new StackPanel();

            string destinationString = string.Empty;
            foreach (var item in destination)
            {
                Console.WriteLine("\n\n destination string is item is {0} \n\n", item);
                destinationString= destinationString + item + ", ";
            }


            //who is hosting the content
            Label item_parent_label = new Label
            //
            {

                Content = "Destination emails:"+destinationString //destination.ToString()
                //Content = "Targe mail : ke@mai.com"
            };
            //name of the content
            Label item_name_label = new Label
            {
                Content = string.Concat("Name \t", name)
            };
            //size
            Label item_size_label = new Label
            {
                Content = string.Concat("Size ", Format_length(size))
            };
            //type -> word video music document
            Label item_type_label = new Label();

            int numberOfFiles = (int)Math.Ceiling((double) size / App_Const.FileSplitSize());
            string percentage = ((current_upload_item_counterInt / numberOfFiles) * 100).ToString();  // (int)Math.Ceiling((double)(current_upload_item_counterInt / numberOfFiles) * 100);

            StackPanel  uploaded_percentage = new  StackPanel();
            uploaded_percentage.Orientation = Orientation.Horizontal;
            uploaded_percentage.Width = 120;
            uploaded_percentage.Height = 33;

            
            uploaded_percentage.Margin = new Thickness(18, 0, 0, 0);


            Label uploaded_percentage_staticText = new Label();
            Label uploaded_percentage_dynamicText = new Label();
            uploaded_percentage_staticText.Foreground = new System.Windows.Media.SolidColorBrush(Colors.Black);
            uploaded_percentage_dynamicText.Foreground = new System.Windows.Media.SolidColorBrush(Colors.Black);

            uploaded_percentage_staticText.Content = "Upload: ";
            //set unique name for ui updating

            Console.WriteLine("\n \n Percentage ======= {0} ===== curent item {1} and total {2}", current_upload_item_counterInt*100/numberOfFiles, current_upload_item_counterInt , numberOfFiles);
            uploaded_percentage_dynamicText.Name = fileItemKey + "uploadValue";
            uploaded_percentage_dynamicText.Content = (isStarted == true ? current_upload_item_counterInt * 100 / numberOfFiles+  "%" : "Starting" ) ;


            uploaded_percentage.Children.Add(uploaded_percentage_staticText);
            uploaded_percentage.Children.Add(uploaded_percentage_dynamicText);
            

            item_type_label.Content = string.Concat("\t\t\t Type  :", type);


            //progress indicator
            MetroProgressBar upload_progress = new MetroProgressBar();
            upload_progress.Minimum = 0;
            upload_progress.Maximum = numberOfFiles;
            upload_progress.MaxHeight = 4;
            upload_progress.Height = 3;
            upload_progress.Value = isStarted == true ?   current_upload_item_counterInt : 0;
            //set unique name for ui updating
            upload_progress.Name = fileItemKey + "uploadProgressBarValue";
            
            
            upload_progress.Foreground = new System.Windows.Media.SolidColorBrush(Colors.Indigo);
            //         Foreground = "{DynamicResource AccentColorBrush}"


            app_upload_pub.OnChange += (sender, e) =>
            {
                Console.WriteLine("\n\n =========>>>>>   subscriber upload data with  => " + e.Value);

                App_Download_Update_Data upload_data = JsonConvert.DeserializeObject<App_Download_Update_Data>(e.Value);

                if (upload_data.Itemkey.Equals(fileItemKey))
                {
                
                    Dispatcher.Invoke(() =>
                    {
                        uploaded_percentage_dynamicText.Content = upload_data.ProgressPercentage + " %";
                        upload_progress.Value = upload_data.ProgressValue;
                    });


                    if (upload_data.ProgressPercentage.Equals("100"))
                    {
                        Dispatcher.Invoke(() =>
                        {
                            //clean ui.
                           // uploading_contents.Children.Clear();

                            //recall parser to fill ui
                           // ParseSendingObjectsToUi();
                        });
                       
                    }
                }

            };

            Image item_image = new Image();
            
            item_grid.Orientation = Orientation.Horizontal;
            item_grid.Width = uploading_contents.Width;
            item_grid.Height = 100;

            image_and_comment.Orientation = Orientation.Vertical;
            //comments.Orientation = Orientation.Horizontal;

            item_image.Width = 100; item_image.Height = 100;


            //comment_button.Height = 25; comment_button.Width = 25;
            //thumbs_up_button.Height = 25; thumbs_up_button.Width = 25;
            //thumbs_down_button.Height = 25; thumbs_down_button.Width = 25;

            comments.Height = 30; comments.Width = 90;
     

            image_and_comment.Children.Add(item_image);
            // image_and_comment.Children.Add(comments);

            //item details
            item_details.Width = 500;
            item_details.Height = 100;
            item_details.Children.Add(item_name_label);
            item_details.Children.Add(item_parent_label);

            item_details_description.Orientation = Orientation.Horizontal;
            item_details_description.Children.Add(item_size_label);
            item_details_description.Children.Add(item_type_label);
            item_details_description.Children.Add(uploaded_percentage);

            //add item_details_description into item_details
            item_details.Children.Add(item_details_description);

            download_stream.Orientation = Orientation.Vertical;




            List<string> video_ext = new List<string>() { ".mp4", ".mov", ".wmv", ".avi", ".mkv", ".flv" };
            List<string> image_ext = new List<string>() { ".png", ".jpeg", ".gif", ".bmp", ".tiff", ".psd", ".webp" };
            List<string> music_ext = new List<string>() { ".mp3", ".ogg", ".aac", ".atrac", ".aiff", ".wma", ".vorbis", ".opus" };
            List<string> wordDocument_ext = new List<string>() { ".doc", ".docx", ".dotx", ".rtf", ".dot" };
            List<string> textFile_ext = new List<string>() { ".txt", ".log" };
            List<string> webFile_ext = new List<string>() { ".html", ".htm" };




            if (video_ext.Contains(type.ToLower()))
            {
                
                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/video_file.png", UriKind.Relative));


            }
            else if (wordDocument_ext.Contains(type.ToLower()))
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/WinWordLogoSmall.scale-140.png", UriKind.Relative));


            }

            else if (type.ToLower().Equals(".pdf"))
            {


                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/pdf.png", UriKind.Relative));


            }
            else if (type.ToLower().Equals(".xml"))
            {
                //String resourceDir = asps.Curr_Resources_Dir();
                //System.Uri resourceImageUri = new System.Uri(System.IO.Path.Combine(resourceDir, "images/file.jpg"));

                //Console.WriteLine("->Resource Dir ={0} \n resource image uri ={1}", asps.Curr_Resources_Dir(), resourceImageUri);

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/file.jpg", UriKind.Relative));

            }

            else if (music_ext.Contains(type.ToLower()))
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/audio_file_2.png", UriKind.Relative));

            }
            else if (image_ext.Contains(type.ToLower()))
            {

                //System.Uri resourceImageUri = new System.Uri(file_path);

                //item_image.Source = new BitmapImage();

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/files.png", UriKind.Relative));

            }
            else
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/files.png", UriKind.Relative));

            }

            item_grid.Children.Add(image_and_comment);
            item_grid.Children.Add(item_details);
            item_grid.Children.Add(download_stream);
            item_details.Children.Add(upload_progress);


            item_grid.Margin = new Thickness(0, 10, 0, 0);


            ///end of for debugging
            if ( !filesInAraizenShareFolder.Contains(name))
            {
                Console.WriteLine("oops araizen folder does not contain {0}",name);
                //controls grid
                StackPanel errorStack = new StackPanel();

                //delete label
                Label labelErro = new Label()
                {
                    Content = "Oops the file does not seem to exist",
                    Foreground = new System.Windows.Media.SolidColorBrush(Colors.Red),

                 };

                   //delete button
                Button error_button = new Button();
                error_button.FindResource(ToolBar.ButtonStyleKey);
                error_button.BorderThickness = new Thickness(0, 0, 0, 0);

                //delete icon
                Image error_item_image = new Image
                {
                    Width = 25,
                    Height = 25,
                    Source = new BitmapImage(new Uri(@"/Resources/images/ic_delete_black_18dp.png", UriKind.Relative))
                };

                error_button.Content = error_item_image;

                error_button.Click += (object sender, RoutedEventArgs e) =>
                {
                    Console.WriteLine("\n\n\n  ----------------->>>>>> delete item not exist \n\n\n");
                    try
                    {
                        upload_sql.DeleteUploadItemsDatabase(UnixTimeFileItemReferenceId: unixTimeFileItemReferenceId);
                        upload_sql.DeleteFileItemsDatabase(UnixTimeFileItemReferenceId: unixTimeFileItemReferenceId);

                    }
                    catch
                    {
                        Console.WriteLine("\n\n\n  --error deleting user selercted");
                    }
                    //clean ui.
                    uploading_contents.Children.Clear();

                    //recall parser to fill ui
                    ParseSendingObjectsToUi();

                };

               
                
                error_button.Height = 45;
                error_button.Width = 45;
                Style styles = this.FindResource("buttonstyle") as Style;
                error_button.Style = styles;

                errorStack.Children.Add(error_button);
                errorStack.Children.Add(labelErro);
                errorStack.HorizontalAlignment = HorizontalAlignment.Center;
                errorStack.VerticalAlignment = VerticalAlignment.Center;

                errorStack.Opacity = 1;

               //add to ui 
                 item_grid.Opacity = 0.167;

                //use the special item_grid_container
                item_grid_container.Children.Add(item_grid);
                //add erro stack later
                //to be able to access the click function
                item_grid_container.Children.Add(errorStack);
       

                uploading_contents.Children.Add(item_grid_container);
             
            }
            else
            {
                //controls grid
                StackPanel errorStack = new StackPanel();

               
                //delete button
                Button error_button = new Button();
                error_button.FindResource(ToolBar.ButtonStyleKey);
                error_button.BorderThickness = new Thickness(0, 0, 0, 0);

                //delete icon
                Image error_item_image = new Image
                {
                    Width = 25,
                    Height = 25,
                    Source = new BitmapImage(new Uri(@"/Resources/images/ic_delete_black_18dp.png", UriKind.Relative))
                };

                error_button.Content = error_item_image;

                error_button.Click += (object sender, RoutedEventArgs e) =>
                {
                    Console.WriteLine("\n\n\n  ----------------->>>>>> delete by user choice item folder {0} \n\n\n", System.IO.Path.Combine(ally_paths.ShareUploadFolder(), name));
                    upload_sql.DeleteUploadItemsDatabase(UnixTimeFileItemReferenceId: unixTimeFileItemReferenceId);
                    upload_sql.DeleteFileItemsDatabase(UnixTimeFileItemReferenceId: unixTimeFileItemReferenceId);

                    //delet the file
                    try
                    {
                        string deleteFilePath = System.IO.Path.Combine(ally_paths.ShareUploadFolder(), name);
                        System.IO.Directory.Delete(deleteFilePath, true);

                    }catch
                    {
                        Console.WriteLine("erro deleting file use initiated");
                    }
                    //clean ui.
                    uploading_contents.Children.Clear();

                    //recall parser to fill ui
                    ParseSendingObjectsToUi();

                };

                error_button.Height = 35;
                error_button.Width = 35;
                Style style = this.FindResource("buttonstyle") as Style;
                error_button.Style = style;

                errorStack.Children.Add(error_button);
               
                errorStack.HorizontalAlignment = HorizontalAlignment.Right;
                errorStack.VerticalAlignment = VerticalAlignment.Top;

                errorStack.Opacity = 1;

                //add to ui 
               // item_grid.Opacity = 0.167;

                //use the special item_grid_container
                item_grid_container.Children.Add(item_grid);
                //add erro stack later
                //to be able to access the click function
                item_grid_container.Children.Add(errorStack);

                uploading_contents.Children.Add(item_grid_container);

            };

    
        }
        #endregion


        #region CallRefreshUi
        private void CallRefreshUi(string uploadData)
        {
            Console.WriteLine("publisher refresh ui upload data with  => "+ uploadData);
            app_upload_pub.Raise(uploadData);
        }

        
        #endregion



        #region formt length
        private string Format_length(long len)
        {
            string[] sizes = { "b", "KB", "MB", "GB", "TB" };
            int order = 0;
            while (len >= 1024 && order < sizes.Length - 1)
            {
                order++;
                len = len / 1024;
            }
            String result = string.Format("{0:0.##} {1}", len, sizes[order]);
            Console.WriteLine(" Format length -->{0}", result);
            return result;
        }
        #endregion

        #region Page_Loaded
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
           

        }
        #endregion


        #region Upload comms components
        #region OnAraizenServiceNotificationConnectionMessage
        private void OnAraizenServiceUploadConnectionMessage(NamedPipeConnection<string, string> connection, string message)
        {
            Console.WriteLine("\n\n call refresh ui file message {0}", message);

            CallRefreshUi(message);

        }
        #endregion

        #region OnAraizenServiceUploadConnectionDisconnected
        private void OnAraizenServiceUploadConnectionDisconnected(NamedPipeConnection<string, string> connection)
        {
            Console.WriteLine("<b>Disconnected from server</b>");

        }
        #endregion

        #region AraizenServiceUploadSendMessage
        public void AraizenServiceUploadSendMessage(App_Upload uploadItem)
        { 
      
            string json_str = JsonConvert.SerializeObject(uploadItem);
            AraizenServicePipeUploads.PushMessage(json_str);


        }
        #endregion
        #endregion
    }
}
