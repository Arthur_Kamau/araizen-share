﻿using NamedPipeWrapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_Notifications.xaml
    /// </summary>
    public partial class Ui_ShareNotifications : Page
    {

        App_Notification_Sql notifications_sql = new App_Notification_Sql();
        List<App_Notification> notificationItems;
        List<String> filesInAraizenShareFolder;
        //List<String> uploadDirItems;
        //List<String> itemsInUploadDirNotInJson;

        App_System_Paths paths = new App_System_Paths();
        String pathToUploadDb = string.Empty;
        string[] charsToRemoveForValidName = new string[] { "@", "#", ",", ".", ";", "'", "-", "_", "\\", "/", "  " };

       
        static public event EventHandler openDowloadsFromNotification;

        private readonly NamedPipeClient<string> AraizenServicePipeDownloads = new NamedPipeClient<string>(App_Const.PipeNameDownload);

        public Ui_ShareNotifications()
        {
            InitializeComponent();

            AraizenServicePipeDownloads.ServerMessage += OnAraizenServiceDownloadConnectionMessage;
            AraizenServicePipeDownloads.Disconnected += OnAraizenServiceDownloadConnectionDisconnected;
            AraizenServicePipeDownloads.Start();


            //populate items ui
            ParsenNotificationObjectsToUi();
        }

      

        private void ParsenNotificationObjectsToUi()
        {
            //initialize the render functions
           
            //list from db
            notificationItems = notifications_sql.GetAllnotificationItemsDatabase();

            //update ui that show empty 
            if (notificationItems != null && notificationItems.Count >= 0)
            {
                content_infomation.Opacity = 0;
            }

            foreach (App_Notification notificationObj in notificationItems)
            {
               
                RenderUi(
                    notificationObj
               );
            }
        }

        string GetFileItemKey(string unixTime, string fileName)
        {
            string originalName = string.Concat(fileName, unixTime);

            //valid name charsToRemoveForValidName
            string filterSpecialCharacters = new string((from c in originalName
                                                         where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                                         select c
          ).ToArray());


            string filterSpacesAndSpecialCharacters = filterSpecialCharacters.Replace(" ", string.Empty);

            return filterSpacesAndSpecialCharacters;

        }

     

        #region Add_items_to_ui
        private void RenderUi(App_Notification notificationObj )
        {
            App_System_Paths asps = new App_System_Paths();

            Grid item_grid_container = new Grid();
            StackPanel item_grid = new StackPanel();
            StackPanel image_and_comment = new StackPanel();
            StackPanel comments = new StackPanel();

            StackPanel item_details = new StackPanel();
            StackPanel item_details_description = new StackPanel();

            Label notification_type_label = new Label
            {
                Content = string.Concat("Notification : ", notificationObj.Notification_type)
            };
            
            Label notification_from_label = new Label
            //
            {
                Content = "From :  "+ notificationObj.Notfication_from_email //Notification_title
            };

            Label notification_title_label = new Label
            //
            {
                Content = "Name :  " + notificationObj.Notification_title //Notification_title
            };


            Label notification_date_label = new Label();
            //size
            if (!string.IsNullOrEmpty(notificationObj.Notification_date))
            {
                int indexOfSpace = notificationObj.Notification_date.IndexOf(' ');
                string dateOnly = notificationObj.Notification_date.Substring(0, indexOfSpace);

                notification_date_label.Content = string.Concat("Notification Date  : ", dateOnly);
                
            }
            else
            {
                notification_date_label.Content = string.Concat("Notification Date  :   ");
            }

          
            Label from_text = new Label();
            from_text.Foreground = new System.Windows.Media.SolidColorBrush(Colors.Black);
           
            //from_text.Content = "Name : " + notificationObj.Notification_title;
            //set unique name for ui updating


            //progress indicator
            Grid notifications_button_container = new Grid();
            notifications_button_container.Width = 150;
            
            // notifications_button_container.Height = 80;
            Button notifications_button = new Button();
            Label notifications_button_label = new Label();

           
            


            Image item_image = new Image();
            //set image base on the type of alert


            /*
             * * Notification_type 
             * 
             * document
             * url
             * alert
             * danger
             * other
             */

            if (notificationObj.Notification_type.ToLower().Equals("document") || notificationObj.Notification_type.ToLower().Equals("download"))
            {
                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/file.jpg", UriKind.Relative));

                //check if expired
                if (notificationObj.Notification_expired.ToLower().Equals("true"))
                {
                    notifications_button_label.Content = "expired";
                    notifications_button_label.IsEnabled = false;

                }
                else
                {
                    //not expired

                    notifications_button_label.Content = "download";
                    //get data from server then open
                    //open downloads 
                    notifications_button.Click += (object sender, RoutedEventArgs e) =>
                    {
                    //get download data from server
                    App_Notifications_Server notify_server = new App_Notifications_Server();

                        App_Download_Data_From_Notification downloadItemData = notify_server.GetDownloadShareObject(notificationObj);

                        if (downloadItemData.IsOkay)
                        {

                            App_Download downloadItem = downloadItemData.Data;

                            var test_json_str = JsonConvert.SerializeObject(downloadItem);

                            Console.WriteLine("\n\n --------> we got from the server insert download data from notification {0} \n\n", test_json_str);

                        //insert int database
                        App_Download_Sql download_Sql = new App_Download_Sql();
                            download_Sql.InsertdownloadItemsDatabase(downloadItem);


                        //send item to service
                        AraizenServiceDownloadSendMessage(downloadItem);


                            if (openDowloadsFromNotification != null)
                            {
                                openDowloadsFromNotification(this, e);
                            }
                        }
                        else
                        {
                            string errorTitle = "Oops !!! the Notification like seems brocken";
                            string errorBody = "Possible solution , Delete this notification and ask sender to resend the data, Restart the app if error persist contact Araizen Support Center";
                            Ui_Error err = new Ui_Error(title: errorTitle, body: errorBody);
                            err.ShowDialog();

                        }
                    };
                }
            }


            else if (notificationObj.Notification_type.ToLower().Equals("url") )
            {
                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/file.jpg", UriKind.Relative));

                notifications_button.Click += (object sender, RoutedEventArgs e) =>
                {
                    App_System_Wifi.Open_in_browser(url: notificationObj.Notofication_url);
                };
                notifications_button_label.Content = "website";
            }
            else if (notificationObj.Notification_type.ToLower().Equals("alert"))
            {
                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/search_item.png", UriKind.Relative));

                notifications_button.Click += (object sender, RoutedEventArgs e) =>
                {
                    Ui_ShareNotifications_Danger_Dialogue dialog = new Ui_ShareNotifications_Danger_Dialogue(notificationObj);

                    dialog.ShowDialog();
                };
                notifications_button_label.Content = "alert";
            }
            else if (notificationObj.Notification_type.ToLower().Equals("danger"))
            {
                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/file_item.png", UriKind.Relative));

                notifications_button.Click += (object sender, RoutedEventArgs e) =>
                {
                    Ui_ShareNotifications_Error_Dialogue dialog = new Ui_ShareNotifications_Error_Dialogue(notificationObj);

                    dialog.ShowDialog();
                };

                notifications_button_label.Content = "Caution";
            }
            else
            {
                notifications_button.Opacity = 0.0;
            }



            notifications_button.Content = notifications_button_label;
            notifications_button.FindResource(ToolBar.ButtonStyleKey);
            notifications_button.BorderThickness = new Thickness(0, 0, 0, 0);

            Style styles = this.FindResource("buttonstyle") as Style;
            notifications_button.Style = styles;
            notifications_button.Width = 90;
            notifications_button.Height = 35;
            notifications_button.Margin = new Thickness(0, 0, 10, 0);

            notifications_button.HorizontalAlignment = HorizontalAlignment.Right;
            notifications_button.VerticalAlignment = VerticalAlignment.Top;

            notifications_button_container.Children.Add(notifications_button);


            item_grid.Orientation = Orientation.Horizontal;
            item_grid.Width = notifications_contents.Width;
            item_grid.Height = 120;

            image_and_comment.Orientation = Orientation.Vertical;
            //comments.Orientation = Orientation.Horizontal;

            item_image.Width = 110; item_image.Height = 110;

            comments.Height = 30; comments.Width = 90;
    
            image_and_comment.Children.Add(item_image);
        
            //item details
            item_details.Width = 550;
            item_details.Height = 110;
            item_details.Children.Add(notification_type_label);
            item_details.Children.Add(notification_from_label);
            item_details.Children.Add(notification_title_label);

            item_details_description.Orientation = Orientation.Horizontal;
            item_details_description.Children.Add(notification_date_label);
          
            item_details_description.Children.Add(from_text);

            //add item_details_description into item_details
            item_details.Children.Add(item_details_description);

          
            item_grid.Children.Add(image_and_comment);
            item_grid.Children.Add(item_details);
           
            item_grid.Margin = new Thickness(0, 10, 0, 0);


            //controls grid
            StackPanel errorStack = new StackPanel();
            errorStack.Orientation = Orientation.Horizontal;

            //delete button
            Button error_button = new Button();
            error_button.FindResource(ToolBar.ButtonStyleKey);
            error_button.BorderThickness = new Thickness(0, 0, 0, 0);

            //delete icon
            Image error_item_image = new Image
            {
                Width = 25,
                Height = 25,
                Source = new BitmapImage(new Uri(@"/Resources/images/ic_delete_black_18dp.png", UriKind.Relative))
            };

            error_button.Content = error_item_image;

            error_button.Click += (object sender, RoutedEventArgs e) =>
            {

                //delet from server then locally
                try
                {
                    App_Notifications_Server notServer = new App_Notifications_Server();
                    notServer.DeleteANotifications(notificationObj);
                }
                catch
                {
                    Console.WriteLine("Error Deleting data ");
                }

                notifications_sql.DeletenotificationItemsDatabase(

                    notfication_from_name : notificationObj.Notfication_from_name, 
                    notification_from_email : notificationObj.Notfication_from_email
                );
              
             
                //clean ui.
                notifications_contents.Children.Clear();

                //recall parser to fill ui
                // ParseSendingObjectsToUi();

                ParsenNotificationObjectsToUi();


            };

            error_button.Height = 35;
            error_button.Width = 35;
            Style style = this.FindResource("buttonstyle") as Style;
            error_button.Style = style;

            errorStack.Children.Add(notifications_button_container);
            errorStack.Children.Add(error_button);
            
            errorStack.HorizontalAlignment = HorizontalAlignment.Right;
            errorStack.VerticalAlignment = VerticalAlignment.Top;

            errorStack.Opacity = 1;

            //add to ui 
            // item_grid.Opacity = 0.167;

            //use the special item_grid_container
            item_grid_container.Children.Add(item_grid);
            //add erro stack later
            //to be able to access the click function
            item_grid_container.Children.Add(errorStack);

            notifications_contents.Children.Add(item_grid_container);

            


            /**
             * set selcetced item in combiobox
             */

        }
        #endregion

        #region formt length
        private string Format_length(long len)
        {
            if (len > 0)
            {
                string[] sizes = { "b", "KB", "MB", "GB", "TB" };
                int order = 0;
                while (len >= 1024 && order < sizes.Length - 1)
                {
                    order++;
                    len = len / 1024;
                }
                String result = string.Format("{0:0.##} {1}", len, sizes[order]);
                Console.WriteLine("-->{0}", result);
                return result;
            }
            else
            {
                return "0 KB";
            }
        }
        #endregion


        #region Download comms components
        #region OnAraizenServiceDownloadConnectionMessage
        private void OnAraizenServiceDownloadConnectionMessage(NamedPipeConnection<string, string> connection, string message)
        {

            Console.WriteLine("file message {0}", message);
         
        }
        #endregion

        #region OnAraizenServiceDownloadConnectionDisconnected
        private void OnAraizenServiceDownloadConnectionDisconnected(NamedPipeConnection<string, string> connection)
        {
            Console.WriteLine("<b>Disconnected from server</b>");

        }
        #endregion

        #region AraizenServiceDownloadSendMessage
        public void AraizenServiceDownloadSendMessage(App_Download downloadItem)
        {
            string json_str = JsonConvert.SerializeObject(downloadItem);
            AraizenServicePipeDownloads.PushMessage(json_str);


        }
        #endregion

        #endregion

        # region DeleteAll_Click
        private void DeleteAll_Click(object sender, RoutedEventArgs e)
        {
           

            App_Auth_File authFile = new App_Auth_File();
            App_Auth authData = authFile.GetAuthDetailsFile();


            Ui_ShareNotifications_Deletion w = new Ui_ShareNotifications_Deletion();
            if (w.ShowDialog() == false)
            {
                bool proceed = w.DoWeProceedToDeleteAll;
                //if true exist
                if (proceed)
                {

                    App_Notifications_Server notificationServer = new App_Notifications_Server();
                    LoadingProgressring.Opacity = 1;
                    notifications_contents.Opacity = 0;
                    content_infomation.Opacity = 1;
                    content_infomation.Content = "Deleteing All Notifications(In server & locally)";
                    //delete files
                    try
                    {
                        //delete server
                        notificationServer.DeleteAllNotifications(authData.Email, authData.Name);

                        //delete locally
                        App_Notification_Sql notificationSQL = new App_Notification_Sql();
                        notificationSQL.DeleteAllFromnotificationItemsDatabase();

                        //clear ui.
                        notifications_contents.Children.Clear();

                        content_infomation.Opacity = 0;
                        LoadingProgressring.Opacity = 0;

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error" + ex);
                    }


                   
                }

            }

        }
        #endregion

        #region Refresh_Click
        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            notifications_contents.Children.Clear();
            LoadingProgressring.Opacity = 1;
            notifications_contents.Opacity = 0;
            content_infomation.Opacity = 1;
            content_infomation.Content = "Fetching Data From Server";


            App_Notification_Sql notificationSQL = new App_Notification_Sql();

           
            App_Auth_File authFile = new App_Auth_File();
            App_Auth authData = authFile.GetAuthDetailsFile();

            App_Notifications_Server notificationServer = new App_Notifications_Server();
            List<App_Notification> allNotification = null;

            try
            {
                allNotification = notificationServer.GetAllNotificationFromServer(authData.Email, authData.Name);
            }
            catch (Exception err)
            {
                Console.WriteLine("Unable to contact araize share server {0}", err);
            }

            //check if null
            if (allNotification != null)
            {
                //delete all the records
                notificationSQL.DeleteAllFromnotificationItemsDatabase();


                foreach (var newItemNotification in allNotification)
                {
                    notificationSQL.InsertnotificationItemsDatabase(newItemNotification);
                }
                
                //then load ui
                content_infomation.Opacity = 0;
                LoadingProgressring.Opacity = 0;
                notifications_contents.Opacity = 1;
                


                foreach (var newItemNotification in allNotification)
                {
                    RenderUi(
                  newItemNotification
             );
                }
            }

        }

        #endregion

    }
}
