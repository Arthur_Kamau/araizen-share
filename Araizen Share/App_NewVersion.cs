﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    /// <summary>
    /// App new version class
    /// get data from file and write to file
    /// </summary>
    #region App_NewVersion class
    class App_NewVersion
    {

        App_System_Paths paths = new App_System_Paths();
        App_File files = new App_File();

        #region GetUpdateDetails
        public NewVersionDetails GetUpdateDetails()
        {

            string json_str = files.Get_data_from_file(paths.PathToUpdateDetails());
            Console.WriteLine(" Get Update details  --------->{0}", json_str);
            NewVersionDetails verDetails = JsonConvert.DeserializeObject<NewVersionDetails>(json_str);

            return verDetails;

        }
        #endregion
        #region Set update details
        public void SetUpdateDetails(string newVersionPar, string newBuildPar, string newVersionUpdatesPar, string download_link, Boolean is_critical, Boolean user_ignored = false)
        {
            App_File files = new App_File();
            string update_details_in_file = files.Get_data_from_file(paths.PathToUpdateDetails());
            DateTime now = DateTime.Now;
            NewVersionDetails verDetails = new NewVersionDetails(newVersion: newVersionPar, newBuild: newBuildPar, newVersionUpdates: newVersionUpdatesPar, download_link: download_link, is_critical: is_critical, proposed_time: now, ignore: false);

            if (string.IsNullOrEmpty(update_details_in_file))
            {
                List<NewVersionDetails> verDetailsList = new List<NewVersionDetails>();
                verDetailsList.Add(verDetails);

                string json_str = JsonConvert.SerializeObject(verDetails);
                Console.WriteLine("\n SetRegisterDetails --------->{0}", json_str);
                files.Save_data_to_file(path: paths.PathToUpdateDetails(), data: json_str);
            }
            else
            {
                List<NewVersionDetails> verDetails_in_file = JsonConvert.DeserializeObject<List<NewVersionDetails>>(update_details_in_file);
                List<NewVersionDetails> orderd_verDetails_in_file = verDetails_in_file.OrderBy(o => o.Proposed_time).ToList();

                NewVersionDetails latest_orderd_verDetails_in_file = orderd_verDetails_in_file[0];

                if (newBuildPar.Equals(latest_orderd_verDetails_in_file.NewBuild))
                {
                    //same ignore
                    Console.WriteLine("same ignore");
                }
                else
                {
                    //not same
                    verDetails_in_file.Add(verDetails);
                    //add to list
                    string json_str = JsonConvert.SerializeObject(verDetails);
                    //save in file
                    files.Save_data_to_file(path: paths.PathToUpdateDetails(), data: json_str);
                }

            }
        }
        #endregion


    }
    #endregion


    /// <summary>
    /// new version details the version number,build number,changes that come with the new version.
    /// </summary>
    #region NewVersion 
    public class NewVersionDetails
    {
        public NewVersionDetails(string newVersion, string newBuild, string newVersionUpdates, string download_link, Boolean is_critical, Boolean ignore, DateTime proposed_time)
        {
            NewVersion = newVersion;
            NewBuild = newBuild;
            NewVersionUpdates = newVersionUpdates;
            Ignore = ignore;
            DownloadLink = download_link;
            Is_critical = is_critical;
            Proposed_time = proposed_time;
        }

        // public String platform_type { get; set; } //stable or latest
        public String NewVersion { get; set; }
        public String NewBuild { get; set; }
        public String NewVersionUpdates { get; set; } //description
        public String DownloadLink { get; set; }
        public Boolean Ignore { get; set; }
        public Boolean Is_critical { get; set; }
        public DateTime Proposed_time { get; set; }


    }
    #endregion
}
