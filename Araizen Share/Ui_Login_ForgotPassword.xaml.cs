﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for forgot_password.xaml
    /// </summary>
    public partial class Ui_Login_ForgotPassword : MetroWindow
    {

        App_Protect_String ProtectString = new App_Protect_String();
      
        public Ui_Login_ForgotPassword()
        {
            InitializeComponent();

         

            var th = new Task(() =>
            {
                while (true)
                {
                    var IsThereInterent = App_System_Wifi.CheckForInternetConnection();
                    Dispatcher.Invoke((Action)delegate
                    {
                        if (IsThereInterent)
                        {
                            Submit.IsEnabled = true;
                            InternetStatus.Content = "There Is Internet";

                        }
                        else
                        {
                            Submit.IsEnabled = false;
                            InternetStatus.Content = " No Internet";
                        }
                    });
                    Thread.Sleep(1500);
                }
            });
            th.Start();
        }


        private void BackButton_Click(object sender, RoutedEventArgs e)
        {

            Ui_Login ml = new Ui_Login();
            ml.Show();
            this.Close();

        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {

           
            App_Protect_String ProtectString = new App_Protect_String();

            App_Resource res = new App_Resource();
         

            String emailstr = EmailField.Text;


                SendingStatus.Content = "Emails  match,Sending data.";
                Submit.IsEnabled = false;

            App_ForgotPassword app_ForgotPassword = new App_ForgotPassword();

                Task.Run(() =>
                {

                    App_ForgotPasswordItem_Result result = app_ForgotPassword.ForgotPassword(emailstr);


                    if (result != null) {
                        Console.WriteLine("The return from forgot_password is {0}  \n in forgotPassword_EnterEmail ", result);

                        if (result.IsOkay)
                        {

                            Dispatcher.Invoke((Action)delegate
                            {

                                Console.WriteLine(" \n --@@>>>>The email passed is {0}", emailstr);

                               Ui_ForgotPassword_Serial emailSerial = new Ui_ForgotPassword_Serial(email: emailstr);
                                emailSerial.Show();
                                this.Close();


                            });

                        }
                        else
                        {
                            Dispatcher.Invoke((Action)delegate
                            {
                            //   SendingStatus.Content = "Check Araizen portal for passkey recovery key";
                            //"Not Successsfull try again";

                            SendingStatus.Content = "error,ensure you have registered with same email.";
                                Submit.IsEnabled = true;
                            });

                        }
                    }
                    else
                    {
                        Dispatcher.Invoke((Action)delegate
                        {
                            //   SendingStatus.Content = "Check Araizen portal for passkey recovery key";
                            //"Not Successsfull try again";

                            SendingStatus.Content = "error";
                            Submit.IsEnabled = true;
                        });

                    }
                });


        }



    }
}

