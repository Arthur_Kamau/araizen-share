﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Share_File
    {
        public bool ShareObjectsExist(ShareObjectQueryParam queryParam)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.SetAppInstallDetailsServerUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(queryParam);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);

                if (string.IsNullOrEmpty(result))
                {
                    return false;
                }
                else if (result.Equals("false"))
                {
                    return false;
                }
                else if(result.Equals("true") || result.Equals("ok"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    //string recepentEmail,DateTime uploadTime
    class ShareObjectQueryParam
    {
        public string recepientMail { get; set; }
        public DateTime uploadTime { get; set; }

        public ShareObjectQueryParam(string recepientMail, DateTime uploadTime)
        {
            this.recepientMail = recepientMail;
            this.uploadTime = uploadTime;
        }
    }
}
