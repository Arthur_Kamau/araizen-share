﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Password
    {
        #region ForgotPassword update password
        public App_ForgotPasswordItem_Result ForgotPasswordUpdatePassword(string newPassword, string email)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.ForgotPasswrdChangePasswordUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            Console.WriteLine("hey  .............");

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                App_UpdatePassword confirmPassword = new App_UpdatePassword(
                    userNewPassword: newPassword,

          userEmail: email

                    );

                string json = JsonConvert.SerializeObject(confirmPassword);

                Console.WriteLine("hey sending {0} to the server {1}", json, App_Url.ForgotPasswrdChangePasswordUrl());

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result in ForgotPasswordUpdatePassword is {0}", result);

                try
                {
                    App_ForgotPasswordItem_Result items = JsonConvert.DeserializeObject<App_ForgotPasswordItem_Result>(result);
                    return items;
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        #region ConfirmUserPasswordServer
        public App_ConfirmPassword_Result ConfirmUserPasswordServer(App_Auth_Credential credential)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.ConfirmUserPassword());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            string json = JsonConvert.SerializeObject(credential);

            Console.WriteLine("sending data  {0} to {1} ", json, App_Url.ConfirmUserPassword());

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
               
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0} ", result);

                if (result.Equals("err"))
                {
                    Console.WriteLine("\n Erroro in app auth from server");
                    //caution returning null
                    return null;
                }
                else
                {
                    App_ConfirmPassword_Result auth = JsonConvert.DeserializeObject<App_ConfirmPassword_Result>(result);


                    return auth;
                }
            }
        }
        #endregion
    }
}
