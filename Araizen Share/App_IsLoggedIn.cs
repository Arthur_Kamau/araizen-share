﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_IsLoggedIn
    {
        public Tuple<bool, string> isLogggedIn()
        {
            App_Auth_File authFile = new App_Auth_File();
            App_Auth auth = authFile.GetAuthDetailsFile();
            if (auth != null ||  string.IsNullOrEmpty( auth.Name))
            {
                //is logged in
                DateTime now = DateTime.Now;

                TimeSpan span = now.Subtract(Convert.ToDateTime(auth.LogInTime));

                if (span.TotalHours > App_Const.MaxLoggedInHours)
                {
                    return new Tuple<bool, string>(false, "Expired Session");

                }
                else
                {
                    return new Tuple<bool, string>(true, "Is logged in");
                }
               
            }
            else
            {
                return new Tuple<bool, string>(false, "Not Logged in");
            }

        }

        public Tuple<bool, string> AbleToLogIn()
        {
            //get if app is registered 
            //redudancy from splash screen
           
            // App_Register regDetails = res.GetRegisterDetails();
            App_Registry reg = new App_Registry();

            ///get if is blocked b4 regoster or login
            string app_blocked = string.Empty;
            try
            {
                app_blocked = reg.GetAppIsLockedOrNot();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            if (0 < app_blocked.Length)
            {

                return new Tuple<bool, string>(true, "ok");
            }
            else
            {
                return new Tuple<bool, string>(false, "You are blocked");
            }
        }

        public void LogUserOut()
        {
            App_System_Paths paths = new App_System_Paths();
            FileStream fileStream = File.Open(paths.AuthFile(), FileMode.Open);

            /* 
             * Set the length of filestream to 0 and flush it to the physical file.
             *
             * Flushing the stream is important because this ensures that
             * the changes to the stream trickle down to the physical file.
             * 
             */
            fileStream.SetLength(0);
            fileStream.Close(); // This flushes the content, too.

        }
    }
}
