﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Blocked_Server
    {
        #region IsUserBlocked
        public bool IsUserBlockedServer(string email)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.IsUserBlockedUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                App_User_Email userEmail = new App_User_Email(email: email);

                string json = JsonConvert.SerializeObject(userEmail);


                Console.WriteLine("SetDownloadItemIsStarted sending {0} ", json);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0} ", result);

                if (!string.IsNullOrEmpty(result))
                {
                    if (result.ToLower().Equals("true"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }


            }
        }
        #endregion


        #region AppBlockUser
        public void AppBlockUserServer(App_Download downloadItem)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.BlockUserUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(downloadItem);

                Console.WriteLine("SetDownloadItemIsStarted sending {0} ", json);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0} ", result);


            }
        }
        #endregion



        #region UnBlockUser
        public void UnBlockUserServer(App_Download downloadItem)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.UnBlockUserUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(downloadItem);

                Console.WriteLine("SetDownloadItemIsStarted sending {0} ", json);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0} ", result);


            }
        }
        #endregion


        #region UserBlockDetails
        public void UserBlockDetailsServer(string email)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.UserBlockedDetailsUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                App_User_Email userEmail = new App_User_Email(email: email);

                string json = JsonConvert.SerializeObject(userEmail);

                Console.WriteLine("SetDownloadItemIsStarted sending {0} ", json);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0} ", result);


            }
        }
        #endregion


    }
}
