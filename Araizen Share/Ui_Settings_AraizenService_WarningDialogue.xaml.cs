﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_Settings_AraizenService_WarningDialogue.xaml
    /// </summary>
    public partial class Ui_Settings_AraizenService_WarningDialogue : MetroWindow
    {
        bool procced = false;

        public Ui_Settings_AraizenService_WarningDialogue()
        {
            InitializeComponent();
        }

        public Boolean DoWeProceed
        {
            get { return procced; }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            procced = false;
            this.Close();
        }

        private void Proceed_Click(object sender, RoutedEventArgs e)
        {
            procced = true;
            this.Close();
        }

       
    }
}
