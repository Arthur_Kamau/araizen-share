﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_Account.xaml
    /// </summary>
    public partial class Ui_Account : Page
    {
        public Ui_Account()
        {
            InitializeComponent();

            SetUppAccountDetails();

            SetUpPurchaseDeatils();

        }

        private void SetUppAccountDetails()
        {
            App_Auth_File authFile = new App_Auth_File();
            App_Auth authData = authFile.GetAuthDetailsFile();

            if(authData != null)
            {
                registeredEmail.Content = authData.Email;
                registeredName.Content = authData.Name;
                registeredPackage.Content = authData.AccountType;
                LogInDate.Content = Convert.ToDateTime(authData.LogInTime).Date;
            }

        }

        private void SetUpPurchaseDeatils()
        {
            App_Auth_File authFile = new App_Auth_File();
            App_Auth authData = authFile.GetAuthDetailsFile();
            App_Registry registry = new App_Registry();

            //is purchaseed
            if (IsUserPurchasedLicence())
            {
                Console.WriteLine("\n\n user  purchase licence \n\n");
                //user is purchase
                licenceType.Content = "Purchase";
                //purchase day
                purchaseOrInstallDate.Content = "Purchase Day";
                purchaseOrInstallDateValue.Content = registry.GetPurchaseDate().Date;

                //get days remaining until licence ends
                double purchaseDays = Convert.ToDouble(registry.GetPurchasePeriod());
                TimeSpan daysRemainingspan = registry.GetPurchaseDate().AddDays(purchaseDays).Subtract(DateTime.Now);

                daysRemaining.Content = daysRemainingspan.Days.ToString();

                PurchaseButton.Opacity = 0;

            }
            else
            {
                Console.WriteLine("\n\n user not   purchase licence --TRIAl \n\n");
                //user is trial
                licenceType.Content = "Trial";
                //install date
                purchaseOrInstallDate.Content = "Install Date";
                purchaseOrInstallDateValue.Content = authData.InstallTime.Date ;

                //get days remaining until licence ends
                TimeSpan daysRemainingspan = authData.InstallTime.AddDays(App_Const.MaxTrialDays).Subtract(DateTime.Now);

                daysRemaining.Content = daysRemainingspan.Days.ToString();

                PurchaseButton.Opacity = 1;

            }
        }

        #region IsUserPurchasedLicence
        private bool IsUserPurchasedLicence()
        {
            App_Registry registry = new App_Registry();
            App_Purchase_File purchase_data = new App_Purchase_File();

            App_Purchase purchaseFile = purchase_data.GetPurchaseDetailsFile();

            if (purchaseFile != null)
            {
                String purchaseKeyRegistry = registry.GetPurchaseKey();
                String purchaseKeyFile = purchaseFile.SerialKey;

                if (String.IsNullOrEmpty(purchaseKeyRegistry) || String.IsNullOrEmpty(purchaseKeyFile))
                {
                    return false;
                }
                else
                {
                    if (purchaseKeyFile.Equals(purchaseKeyRegistry))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }
        #endregion

        private void ChangeEmail_Click(object sender, RoutedEventArgs e)
        {
            Ui_ChangeEmail newMail = new Ui_ChangeEmail();


            if (newMail.ShowDialog() == false)
            {
                string  theNewMail = newMail.TheNewEmail;
                if (!string.IsNullOrEmpty(theNewMail))
                {
                    //if not null update ui
                    registeredEmail.Content = theNewMail;
                }
            }

        }

        private void ChangePasswrd_Click(object sender, RoutedEventArgs e)
        {
            App_Auth_File authFile = new App_Auth_File();
            App_Auth autData = authFile.GetAuthDetailsFile();

            Ui_ChangePassword changePass = new Ui_ChangePassword(email: autData.Email);
            changePass.ShowDialog();


        }

        private void PurchaseButton_Click(object sender, RoutedEventArgs e)
        {
            Ui_Purchase pur = new Ui_Purchase(KillOnexit: false);
            pur.ShowDialog();
        }
    }
}
