﻿using MahApps.Metro.Controls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_Purchase.xaml
    /// </summary>
    public partial class Ui_Purchase : MetroWindow// Window
    {
        Boolean killOnExitPurchase = false;
    App_Protect_String ProtectString = new App_Protect_String();

    public Ui_Purchase(Boolean KillOnexit)
    {

        InitializeComponent();

        if (KillOnexit)
        {
            killOnExitPurchase = true;
        }

        ConstructorStuff();
    }



    private void ConstructorStuff()
    {

        Submit.IsEnabled = false;

        var th = new Task(() =>
        {
            while (true)
            {
                var IsThereInterent = App_System_Wifi.CheckForInternetConnection();
                Dispatcher.Invoke((Action)delegate
                {
                    if (IsThereInterent)
                    {
                        Status.Content = "There Is Internet";
                        Submit.IsEnabled = true;
                    }
                    else
                    {
                        Status.Content = " No Internet";
                        Submit.IsEnabled = false;
                    }
                });
                Thread.Sleep(2000);
            }
        });
        th.Start();

    }

    private void Submit_Click(object sender, RoutedEventArgs e)
    {
            LoadingProgressring.Opacity = 1;
            contentsContainer.Opacity = 0;

            String PurchaseSerialstr = PurchaseSerial.Text;
            App_Purchase_Server appPuchase = new App_Purchase_Server();
            App_Auth_File authFile = new App_Auth_File();
            App_Resource res = new App_Resource();
            App_Auth auth = authFile.GetAuthDetailsFile();

            AuthPurchaseDetails authPurchase = new AuthPurchaseDetails(serialKey: PurchaseSerialstr, email: auth.Email);


        bool Commsresult = appPuchase.AuthPurchaseDetails(authPurchase);

        if (Commsresult)
        {


            //true ->Right purchase key
            result.Content = "Successful";


                App_PurchaseDetailsContainer purcaseDetails = appPuchase.GetPurchaseDetailsServer(auth);


            if (purcaseDetails == null || purcaseDetails.IsOkay == false)
            {
                result.Content = "Unable to fetch purchase details,Try again.";
            }
            else
            {

                App_Registry reg = new App_Registry();


                    DateTime purchaseDate = Convert.ToDateTime(purcaseDetails.Data.Date.ToString());

                reg.StorePurchaseKey(purcaseDetails.Data.SerialKey);
                reg.StorePurchasePeriod(purcaseDetails.Data.PurchaseDays.ToString());
                reg.StorePurchaseDate(purchaseDate);

                    //  res.SetPurchaseDetails(key: PurchaseSerialstr, purchaseTime: purchaseDay, period: PeriodLicence);

                    App_Purchase_File purFile = new App_Purchase_File();
                    App_Licence_Data licenceDta = new App_Licence_Data();

                  
                    App_Licence licence = new App_Licence(
                        isPurchased:true,
                        licenceName: auth.Name,
                        licenceEmail: auth.Email,
                        licenceKey : purcaseDetails.Data.SerialKey,
                        licencedOn : purchaseDate,
                        licenceDays : purcaseDetails.Data.PurchaseDays
                        );

                    purFile.SetPurchaseDetailsFile(purcaseDetails.Data);
                    licenceDta.SetLicenceDetails(licence);

                this.Close();


            }

        }
        else
        {
                LoadingProgressring.Opacity = 0;
                contentsContainer.Opacity = 1;

                //wrong purchase key
                result.Content = "Try again,If problem persist contact Araizen Personel";

        }
    }

    private void UpdateAraizenPurchaseDetails(String Serverstr)
    {

    }


    private void MetroWindow_Closed(object sender, EventArgs e)
    {
        if (killOnExitPurchase)
        {
            Process.GetCurrentProcess().Kill();
        }
        else
        {
            this.Close();
        }
    }
}
}

