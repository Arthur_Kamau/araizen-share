﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_ForgotPassword_Serial.xaml
    /// </summary>
    public partial class Ui_ForgotPassword_Serial : MetroWindow
    {
        String userEmail ;
        public Ui_ForgotPassword_Serial(String email)
        {

            InitializeComponent();

            userEmail = email;

            Console.WriteLine(" \n\n ---------------------->  user email = {0} passed in passwrod {1}", userEmail,email);
            var th = new Task(() =>
            {
                while (true)
                {
                    var IsThereInterent = App_System_Wifi.CheckForInternetConnection();
                    Dispatcher.Invoke((Action)delegate
                    {
                        if (IsThereInterent)
                        {
                            Submit.IsEnabled = true;
                            InternetStatus.Content = "There Is Internet";

                        }
                        else
                        {
                            Submit.IsEnabled = false;
                            InternetStatus.Content = " No Internet";
                        }
                    });
                    Thread.Sleep(1500);
                }
            });
            th.Start();
        }
        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            String serialstr = Serial.Text;

            Task.Run(() =>
            {

                // App_System_Communication ac = new App_System_Communication();
                App_ForgotPassword appForgotPassword = new App_ForgotPassword();

                Console.WriteLine(" \n\n ---------------------->  user email = {0} ", userEmail);
                App_ForgotPasswordItem_Result isOk = appForgotPassword.ForgotPasswordConfirmSerail(serial: serialstr, email:userEmail);

                if(isOk != null)
                {
                    if (isOk.IsOkay == true)
                    {
                        Dispatcher.Invoke((Action)delegate
                        {
                            //open change pass
                            Ui_Login_ForgotPassword_ChangePassword recovery = new Ui_Login_ForgotPassword_ChangePassword(email: userEmail);
                            recovery.Show();
                            this.Close();
                        });
                    }
                    else
                    {
                        Dispatcher.Invoke((Action)delegate
                        {
                            //update status
                            IsSuccessful.Content = "Unsucessful please try again.";
                        });
                    }
                }
                else
                {
                    Dispatcher.Invoke((Action)delegate
                    {
                        //error
                        IsSuccessful.Content = "Error , restart again.";
                    });
                }
            });
            IsSuccessful.Content = "Sending information ..";
            Submit.IsEnabled = false;
            InternetStatus.Content = "";
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Ui_Login_ForgotPassword forgotPasswordEmail = new Ui_Login_ForgotPassword();
            forgotPasswordEmail.Show();
            this.Close();
        }
    }
}