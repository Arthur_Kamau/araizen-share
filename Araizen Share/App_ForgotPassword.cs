﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_ForgotPassword
    {
        #region ForgotPassword confirm email
        public App_ForgotPasswordItem_Result ForgotPassword(string email )
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.ForgotPasswrdUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                App_ForgotPasswordItem afi = new App_ForgotPasswordItem(userEmail: email);

                string json = JsonConvert.SerializeObject(afi);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);

                try
                {
                    App_ForgotPasswordItem_Result items = JsonConvert.DeserializeObject<App_ForgotPasswordItem_Result>(result);
                    return items;
                }
                catch
                {
                    return null;
                }

            }
        }
        #endregion

        #region ForgotPassword confirm serial
        public App_ForgotPasswordItem_Result ForgotPasswordConfirmSerail(string serial,string email)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.ForgotPasswrdConfirmCodeUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                App_ForgotPasswordConfirmItem confirmPassword = new App_ForgotPasswordConfirmItem(
                    userConfirmationCode :serial,
            
          userEmail : email
                    
                    );
                string json = JsonConvert.SerializeObject(confirmPassword);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);

                try
                {
                    App_ForgotPasswordItem_Result items = JsonConvert.DeserializeObject<App_ForgotPasswordItem_Result>(result);
                    return items;
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion



       

        
    }


    public class App_ForgotPasswordItem_Result
{
    public string Reason { get; set; }
    public bool IsOkay { get; set; }

        public App_ForgotPasswordItem_Result(string reason, bool isOkay)
        {
            Reason = reason;
            IsOkay = isOkay;
        }
    }

public class App_ForgotPasswordItem
    {
        public string  UserEmail { get; set; }

        public App_ForgotPasswordItem(string userEmail)
        {
            UserEmail = userEmail;
        }
    }

    public class App_ForgotPasswordConfirmItem
    {
        public string UserConfirmationCode { get; set; }
        public string UserEmail { get; set; }

        public App_ForgotPasswordConfirmItem(string userConfirmationCode, string userEmail)
        {
            UserConfirmationCode = userConfirmationCode;
            UserEmail = userEmail;
        }
    }

   public class App_UpdatePassword
    {
        public string UserEmail { get; set; }
        public string UserNewPassword { get; set; }

        public App_UpdatePassword(string userEmail, string userNewPassword)
        {
            UserEmail = userEmail;
            UserNewPassword = userNewPassword;
        }
    }

    
}
