﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_HelpPage.xaml
    /// </summary>
    public partial class Ui_HelpPage : Page
    {
        public Ui_HelpPage()
        {
            InitializeComponent();

            string myHTMLContent = System.IO.Path.GetFullPath( @"./Resources/html/index.html");

            myWebBrowser.Visibility = System.Windows.Visibility.Hidden;
            myWebBrowser.Source = new Uri(myHTMLContent);
            myWebBrowser.LoadCompleted += loaded_LoadCompleted;
        }

        private void loaded_LoadCompleted(object sender, NavigationEventArgs e)
        {
            myWebBrowser.Visibility = System.Windows.Visibility.Visible;

        }


    }
}
