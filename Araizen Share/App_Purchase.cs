﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    /// <summary>
    /// purchase object
    ///    the day,key and period
    /// </summary>
    #region App_Purchase Clasee
   public class App_Purchase
    {

        public string Date { get; set; }
        public int PurchaseDays { get; set; }
        public string SerialKey { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public App_Purchase(string date, int purchaseDays, string serialKey, string email, string phoneNumber)
        {
            Date = date;
            PurchaseDays = purchaseDays;
            SerialKey = serialKey;
            Email = email;
            PhoneNumber = phoneNumber;
        }
    }
    #endregion

    #region App_PurchaseDetailsContainer
    public class App_PurchaseDetailsContainer {
    public bool IsOkay { get; set; }
    public string Reason { get; set; }
    public App_Purchase Data { get; set; }

        public App_PurchaseDetailsContainer(bool isOkay, string reason, App_Purchase data)
        {
            IsOkay = isOkay;
            Reason = reason;
            Data = data;
        }
    }
#endregion

    //#region  AuthPurchase
    //public class AuthPurchase
    //{
    //    public string PurchaseSerial { get; set; }
    //    public string UserEmail { get; set; }

    //    public AuthPurchase(string purchaseSerial, string userEmail)
    //    {
    //        PurchaseSerial = purchaseSerial;
    //        UserEmail = userEmail;
    //    }
    //}
    //#endregion


    public class AuthPurchaseDetails
    {
            public string Email { get; set; }
            public string SerialKey { get; set; }

        public AuthPurchaseDetails(string email, string serialKey)
        {
            Email = email;
            SerialKey = serialKey;
        }
    }
}
