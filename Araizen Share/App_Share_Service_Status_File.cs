﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Share_Service_Status_File
    {

        App_System_Paths paths = new App_System_Paths();
        App_File files = new App_File();

        
        #region GetShareServiceStatus
        public App_Share_Service_Status GetShareServiceStatus()
        {

            string json_str = files.Get_data_from_file(paths.ShareServiceStatusJsonFile());
            Console.WriteLine(" App_Auth --------->{0}", json_str);
            App_Share_Service_Status data = JsonConvert.DeserializeObject<App_Share_Service_Status>(json_str);


            return data;

        }
        #endregion


        #region SetShareServiceStatus
        public void SetShareServiceStatus(App_Share_Service_Status status )
        {
            
            string json_str = JsonConvert.SerializeObject(status);
          
            files.Save_data_to_file(path: paths.ShareServiceStatusJsonFile(), data: json_str);

        }
        #endregion
    }
}
