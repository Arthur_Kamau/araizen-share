﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_RecoverAccount
    {
    }


    /// <summary>
    /// for recovering lost or forgoteen account
    ///  name,email,userkey,appkey,register time
    /// </summary>
    #region RecoverAccount
    class RecoverAccount
    {
        public RecoverAccount(string name, string email, string userKey, string appKey, DateTime registerTime, string accountType)
        {
            Name = name;
            Email = email;
            UserKey = userKey;
            AppKey = appKey;
            RegisterTime = registerTime;
            AccountType = accountType;
        }

        public String Name { get; set; }
        public String Email { get; set; }
        public String AccountType { get; set; }
        public String UserKey { get; set; }
        public String AppKey { get; set; }
        public DateTime RegisterTime { get; set; }

    }
    #endregion
}
