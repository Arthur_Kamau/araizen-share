﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Notifications_Server
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notification"></param>
        /// <returns></returns>
        #region GetLatestNotificationFromServer
        public List<App_Notification> GetLatestNotificationFromServer(string myEmail,string name)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.GetLatestNotificationFromFromServer());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                SendMail myMail = new SendMail(myMail: myEmail, name: name);
                string json = JsonConvert.SerializeObject(myMail);

                Console.WriteLine("\n\n sending to server for notifications {0} ", json);
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0} ", result);

                if (result.Equals("err"))
                {
                    Console.WriteLine("\n Erroro in app auth from server");
                    //caution returning null
                    return null;
                }
                if (string.IsNullOrEmpty(result))
                {
                    Console.WriteLine("\n Erroro in app auth from server --null");
                    //caution returning null
                    return null;
                }else if (result.Length < 4)
                {
                    Console.WriteLine("\n Erroro in app auth from server --less than 4");
                    //caution returning null
                    return null;
                }
                else
                {
                    List<App_Notification> Anotification = JsonConvert.DeserializeObject<List<App_Notification>>(result);
                    return Anotification;
                }
            }
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="notification"></param>
        /// <returns></returns>
        #region GetAllNotificationFromServer
        public List<App_Notification> GetAllNotificationFromServer(string myEmail, string name)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.GetAllNotificationFromServer());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                SendMail myMail = new SendMail(myMail: myEmail, name: name);
                string json = JsonConvert.SerializeObject(myMail);

                Console.WriteLine("\n\n sending to server for notifications {0} ", json);
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0} ", result);

                if (result.Equals("err"))
                {
                    Console.WriteLine("\n Erroro in app auth from server");
                    //caution returning null
                    return null;
                }
                if (string.IsNullOrEmpty(result))
                {
                    Console.WriteLine("\n Erroro in app auth from server --null");
                    //caution returning null
                    return null;
                }
                else if (result.Length < 4)
                {
                    Console.WriteLine("\n Erroro in app auth from server --less than 4");
                    //caution returning null
                    return null;
                }
                else
                {
                    List<App_Notification> Anotification = JsonConvert.DeserializeObject<List<App_Notification>>(result);
                    return Anotification;
                }
            }
        }
        #endregion


        /// <summary>
        /// deletes all nuser notifications
        /// </summary>
        /// <param name="myEmail"></param>
        /// <param name="name"></param>
        #region DeleteAllNotifications
        public void DeleteAllNotifications(string myEmail, string name)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.DeleteAllNotificationsFromServer());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                SendMail myMail = new SendMail(myMail: myEmail, name: name);
                string json = JsonConvert.SerializeObject(myMail);

                Console.WriteLine("\n\n sending to server for notifications {0} ", json);
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0} ", result);
               
            }
        }
        #endregion

        /// <summary>
        /// delets one notification from the server
        /// </summary>
        /// <param name="aNotification"></param>
        #region DeleteANotifications
        public void DeleteANotifications(App_Notification aNotification)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.DeleteANotificationFromServer());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
               
                string json = JsonConvert.SerializeObject(aNotification);

                Console.WriteLine("\n\n sending to server for notifications {0} ", json);
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0} ", result);

            }
        }
        #endregion




        #region GetDownloadShareObject
        public App_Download_Data_From_Notification GetDownloadShareObject(App_Notification notification)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.GetAppDownloadFromServer());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(notification);
                Console.WriteLine(" sent to server for download  = {0}" , json);
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0} ", result);

                if (result.Equals("err"))
                {
                    Console.WriteLine("\n Erroro in app auth from server");
                    //caution returning null
                    return null;
                }
                else 
                {
                    App_Download_Data_From_Notification aDownloadObjectNotification = JsonConvert.DeserializeObject<App_Download_Data_From_Notification>(result);
                    return aDownloadObjectNotification;
                }
            }
        }
        #endregion
    }

    #region class SendMail
    class SendMail
    {
        public string Name { get; set; }
        public string MyMail { get; set; }

        public SendMail(string name, string myMail)
        {
            Name = name;
            MyMail = myMail;
        }
    }
    #endregion
}
