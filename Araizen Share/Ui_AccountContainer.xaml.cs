﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Account_Container.xaml
    /// </summary>
    public partial class Ui_AccountContainer : MetroWindow
    {
        public Ui_AccountContainer()
        {
            InitializeComponent();

            //first time loading show register
            content_container.Source = new Uri("register.xaml", UriKind.RelativeOrAbsolute);
            //set top left to login button
            showLoginButton();
            //hide forgot password button
            showEmptyForgotPasswordButton();






        }

        #region showLoginButton
        private void showLoginButton()
        {
            Button login_page_button = new Button();
            login_page_button.Height = 30;
            login_page_button.Width = 30;
            login_page_button.Background = new SolidColorBrush(Colors.Transparent);
            login_page_button.BorderBrush = new SolidColorBrush(Colors.Transparent);
            login_page_button.BorderThickness = new Thickness(0, 0, 0, 0);

            login_page_button.Click += (object sender, RoutedEventArgs e) =>
            {

                //show login content
                content_container.Source = new Uri("login.xaml", UriKind.RelativeOrAbsolute);
                //show register top button
                showRegisterButton();
                //hide forgot password
                showForgotPasswordButton();
            };


            Image login_page_button_image = new Image();
            login_page_button_image.Height = 20;
            login_page_button_image.Width = 20;
            login_page_button_image.Source = new BitmapImage(
                new Uri(@"/Resources/images/ic_account_circle_black_24dp.png", UriKind.Relative));

            //add image to button
            login_page_button.Content = login_page_button_image;

            //add button to ui
            RegisterLoginContainer.Children.Add(login_page_button);

        }
        #endregion

        #region showRegisterButton
        private void showRegisterButton()
        {
            
                 Button register_page_button = new Button();
            register_page_button.Height = 30;
            register_page_button.Width = 30;
            register_page_button.Background = new SolidColorBrush(Colors.Transparent);
            register_page_button.BorderBrush = new SolidColorBrush(Colors.Transparent);
            register_page_button.BorderThickness = new Thickness(0, 0, 0, 0);

            register_page_button.Click += (object sender, RoutedEventArgs e) =>
            {

                //show login content
                content_container.Source = new Uri("register.xaml", UriKind.RelativeOrAbsolute);
                //show register top button
                showLoginButton();
                //hide forgot password
                showEmptyForgotPasswordButton();
            };


            Image login_page_button_image = new Image();
            login_page_button_image.Height = 20;
            login_page_button_image.Width = 20;
            login_page_button_image.Source = new BitmapImage(
                new Uri(@"/Resources/images/ic_supervisor_account.png", UriKind.Relative));

            //add image to button
            register_page_button.Content = login_page_button_image;

            //add button to ui
            RegisterLoginContainer.Children.Add(register_page_button);
        }
        #endregion

        #region showForgotPasswordButton
        private void showForgotPasswordButton()
        {
            Button forogt_password_page_button = new Button();
            forogt_password_page_button.Height = 30;
            forogt_password_page_button.Width = 30;
            forogt_password_page_button.Background = new SolidColorBrush(Colors.Transparent);
            forogt_password_page_button.BorderBrush = new SolidColorBrush(Colors.Transparent);
            forogt_password_page_button.BorderThickness = new Thickness(0, 0, 0, 0);

            forogt_password_page_button.Click += (object sender, RoutedEventArgs e) =>
            {

                //show forgot password content
                content_container.Source = new Uri("forgot_password.xaml", UriKind.RelativeOrAbsolute);
                //show register top button
                showLoginButton();
                //hide forgot password
                showEmptyForgotPasswordButton();
            };


            Image login_page_button_image = new Image();
            login_page_button_image.Height = 20;
            login_page_button_image.Width = 20;
            login_page_button_image.Source = new BitmapImage(
                new Uri(@"/Resources/images/baseline_record_voice_over_black_18dp.png", UriKind.Relative));

            //add image to button
            forogt_password_page_button.Content = login_page_button_image;

            //add button to ui
            RegisterLoginContainer.Children.Add(forogt_password_page_button);
        }
        #endregion

        #region showEmptyForgotPasswordButton
        private void showEmptyForgotPasswordButton()
        {

        }
        #endregion
    }
}
