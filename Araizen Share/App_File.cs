﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_File
    {
        String encryptionkey = "AU3gDhloX5MJ6CneMmD6o7ef";// App_Protect.GetFileEncryptDecryptPassword();//String.Empty;


        //public App_File()
        //{
        //  //  App_Protect_String ProtectString = new App_Protect_String();
        //   // encryptionkey = ProtectString.DecryptString(encryptionkey);
        //}

        public void Save_data_to_file(string path, string data)
        {
            //if file does not exist create it
            //create dir container if not exist

            CreateFileIfNotExist(path, data);
            SaveToFile(path, data);

        }
        private  void CreateFileIfNotExist(string path, string data)
        {
           
            //create file if not exist
            if (File.Exists(path))
            {
                Console.WriteLine("File exists = {0}", path);
            }
            else
            {
                Console.WriteLine("create file in path = {0}", path);
                var the_file = File.Create(@path);
                the_file.Close();
            }
        }
        private  void SaveToFile(string path, string data)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(path))
                {
                    Console.WriteLine("writing to file");

                    App_Protect_String protect = new App_Protect_String();
                    String ProtectedData = protect.EncryptString(txt: data);//passkey: encryptionkey);
                    sw.WriteLine(ProtectedData);
                    sw.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("erro saving {0} \n\n error {1}", path, e);
            }
        }
        public  string Get_data_from_file(string file_path)
        {
            try
            {
                // Create an instance of StreamReader to read from a file.  
                // The using statement also closes the StreamReader.  
                using (StreamReader sr = new StreamReader(file_path))
                {
                    String DecryptedData ="";
                    string line;
                    string str = string.Empty;
                    // Read and display lines from the file until  
                    // the end of the file is reached.  
                    while ((line = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                        str += line;
                    }
                    Console.WriteLine("-------->{0}", str);

                    if (str.Length > 1 || !String.IsNullOrEmpty(str) || ! String.IsNullOrWhiteSpace(str))
                    {
                        App_Protect_String protect = new App_Protect_String();
                         DecryptedData = protect.DecryptString(enctxt: str);
                    }
                    sr.Close();
                    return DecryptedData;
                }
            }
            catch (Exception e)
            {
                // Let the user know what went wrong. 
                Console.WriteLine("The file could not be read:");

                Console.WriteLine(e.Message);
            }
            return "";
        }

        //back up  is file ready
        public  bool IsFileReady(String filename)
        {
            //if the file can be opened  for exclusive access  it means the file is no longer locked by another process
            try
            {
                using (FileStream inputstreasm = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    if (inputstreasm.Length > 0)
                    {
                        inputstreasm.Close();
                        return true;
                    }
                    else
                    {
                        inputstreasm.Close();
                        return false;
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception {0}", e);
                return false;
            }
        }

    }
}
