﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{

    public enum Log_LeveL
    {
        Critical,
        Info,
        Disaster
    }



    #region
    class App_InfoLog{

        App_System_Paths paths = new App_System_Paths();
        App_File files = new App_File();


        #region SetUserLog
        public void SetInfoLogFile(App_log_Info info_log)
        {
            //get if existing logs
            List<App_log_Info> user_logs = GetInfoLogs();

            //add and save to file
            if (user_logs != null)
            {
                user_logs.Add(info_log);

                string user_logs_json_str = JsonConvert.SerializeObject(user_logs);
                Console.WriteLine("\n set info logs --------->{0}", user_logs_json_str);
                files.Save_data_to_file(path: paths.ShareInfoLogFile(), data: user_logs_json_str);
            }
            else
            {
                //if null
                List<App_log_Info> user_logs_new_obj = new List<App_log_Info>();
                user_logs_new_obj.Add(info_log);
                string user_log_json_str = JsonConvert.SerializeObject(user_logs_new_obj);
                Console.WriteLine("\n set info logs --------->{0}", user_log_json_str);
                files.Save_data_to_file(path: paths.ShareInfoLogFile(), data: user_log_json_str);
            }
        }
        #endregion
        #region GetUserLogs
        public List<App_log_Info> GetInfoLogs()
        {
            string json_str = files.Get_data_from_file(paths.ShareInfoLogFile());
            Console.WriteLine(" get info logs --------->{0}", json_str);
            List<App_log_Info> logs_crush = JsonConvert.DeserializeObject<List<App_log_Info>>(json_str);
            return logs_crush;

        }
        #endregion


        #region SetUserLogServer
        private void SetInfoLogServer(List<App_log_Info> info_log)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.SetUserLogServerUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(info_log);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);
            }
        }
        #endregion
    }

    #endregion


    #region App_UserLog class
    class App_UserLog
    {
        App_System_Paths paths = new App_System_Paths();
        App_File files = new App_File();


        #region SetUserLog
        public void SetUserLogFile(App_Log_User user_log)
        {
            //get if existing logs
            List<App_Log_User> user_logs = GetUserLogs();

            //add and save to file
            if (user_logs != null)
            {
                user_logs.Add(user_log);

                string user_logs_json_str = JsonConvert.SerializeObject(user_logs);
                Console.WriteLine("\n set user logs --------->{0}", user_logs_json_str);
                files.Save_data_to_file(path: paths.ShareUserLogFile(), data: user_logs_json_str);
            }
            else
            {
                //if null
                List<App_Log_User> user_logs_new_obj = new List<App_Log_User>();
                user_logs_new_obj.Add(user_log);
                string user_log_json_str = JsonConvert.SerializeObject(user_logs_new_obj);
                Console.WriteLine("\n set user logs --------->{0}", user_log_json_str);
                files.Save_data_to_file(path: paths.ShareUserLogFile(), data: user_log_json_str);
            }
        }
        #endregion
        #region GetUserLogs
        public List<App_Log_User> GetUserLogs()
        {
            string json_str = files.Get_data_from_file(paths.ShareUserLogFile());
            Console.WriteLine(" get user logs --------->{0}", json_str);
            List<App_Log_User> logs_crush = JsonConvert.DeserializeObject<List<App_Log_User>>(json_str);
            return logs_crush;

        }
        #endregion


        #region SetUserLogServer
        private void SetUserLogServer(List<App_Log_User> user_log)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.SetUserLogServerUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(user_log);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);
            }
        }
        #endregion
    }
    #endregion

    /// <summary>
    /// get and set crush logs to file 
    /// </summary>
    #region App_CrushLog
    class App_CrushLog
    {
        App_System_Paths paths = new App_System_Paths();
        App_File files = new App_File();

        #region SetCrushLog
        public void SetCrushLog(App_Log_Crush crush_log)
        {
           
            //get if existing logs
            List<App_Log_Crush> crush_logs = GetCrushLogs();

            //add and save to file
            if (crush_logs != null)
            {
                crush_logs.Add(crush_log);

                string json_str = JsonConvert.SerializeObject(crush_logs);
                Console.WriteLine("\n set crush logs --------->{0}", json_str);
                files.Save_data_to_file(path: paths.ShareCrushLogFile(), data: json_str);
            }
            else
            {
                //if null
                List<App_Log_Crush> user_crush_logs = new List<App_Log_Crush>();
                user_crush_logs.Add(crush_log);
                string json_str = JsonConvert.SerializeObject(user_crush_logs);
                Console.WriteLine("\n set crush logs --------->{0}", json_str);
                files.Save_data_to_file(path: paths.ShareCrushLogFile(), data: json_str);
            }
        }
        #endregion
        #region GetCrushLogs
        public List<App_Log_Crush> GetCrushLogs()
        {
            string json_str = files.Get_data_from_file(paths.ShareCrushLogFile());
            Console.WriteLine(" crush logs --------->{0}", json_str);
            List<App_Log_Crush> logs_crush = JsonConvert.DeserializeObject<List<App_Log_Crush>>(json_str);
            return logs_crush;

        }
        #endregion


        #region SetCrushLogServer
        private void SetCrushLogServer(List<App_Log_Crush> crush_log)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.SetCrushLogServerUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(crush_log);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("the result is {0}", result);
            }
        }
        #endregion
    }
    #endregion



    /// <summary>
    /// class log info
    /// </summary>
    #region App_log_Info
    public class App_log_Info
    {
        public DateTime Info_Time { get; set; }
        public String  Info_Log_User_Email { get; set; }
        public String Info_Log_User_Machine_Name { get; set; }
        public String Info_Log_Os_Name { get; set; }
        public String Info_Log_App_Version { get; set; }
        public int Info_upload_objects  { get; set; }
        public int Info_download_object { get; set; }

        public App_log_Info(DateTime info_Time, string info_Log_User_Email, string info_Log_User_Machine_Name, string info_Log_Os_Name, string info_Log_App_Version, int info_upload_objects, int info_download_object)
        {
            Info_Time = info_Time;
            Info_Log_User_Email = info_Log_User_Email;
            Info_Log_User_Machine_Name = info_Log_User_Machine_Name;
            Info_Log_Os_Name = info_Log_Os_Name;
            Info_Log_App_Version = info_Log_App_Version;
            Info_upload_objects = info_upload_objects;
            Info_download_object = info_download_object;
        }
    }
    #endregion

    /// <summary>
    /// Crush object datetime and the exception
    /// </summary>
    #region Crush log
    class App_Log_Crush
    {
        public App_Log_Crush() { }
        public App_Log_Crush(DateTime theDateTime, string theException)
        {
            TheDateTime = theDateTime;
            TheException = theException;
        }

        public DateTime TheDateTime { get; set; }
        public String TheException { get; set; }
    }
    #endregion

    /// <summary>
    /// app user log
    ///  uptime,downtime,computer name,user key
    /// </summary>
    #region user log
    class App_Log_User
    {
       
        public DateTime User_App_Uptime { get; set; }
        public string User_Machine_Name { get; set; }
        public string User_Machine_Ip { get; set; }
        public string User_Is_Licenced { get; set; }
        public DateTime User_App_downTime { get; set; }
        public int User_Download_Number { get; set; }
        public int User_Upload_Number { get; set; }

        public App_Log_User(DateTime user_App_Uptime, string user_Machine_Name, string user_Machine_Ip, string user_Is_Licenced, DateTime user_App_downTime, int user_Download_Number, int user_Upload_Number)
        {
            User_App_Uptime = user_App_Uptime;
            User_Machine_Name = user_Machine_Name;
            User_Machine_Ip = user_Machine_Ip;
            User_Is_Licenced = user_Is_Licenced;
            User_App_downTime = user_App_downTime;
            User_Download_Number = user_Download_Number;
            User_Upload_Number = user_Upload_Number;
        }
    }
    #endregion

}
