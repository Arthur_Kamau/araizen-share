﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Licence_Data
    {
        App_System_Paths paths = new App_System_Paths();
        App_File files = new App_File();

        #region GetPurchaseDetails
        public App_Licence GetLicenceDetails()
        {
            string json_str = files.Get_data_from_file(paths.ShareLicenceJsonFile());
            Console.WriteLine(" GetPurchaseDetails --------->{0}", json_str);
            App_Licence pur = JsonConvert.DeserializeObject<App_Licence>(json_str);
            return pur;

        }
        #endregion


        #region SetPurchaseDetails
        public void SetLicenceDetails(App_Licence licence)
        {

            string json_str = JsonConvert.SerializeObject(licence);
            Console.WriteLine("\n purchase details --------->{0}", json_str);
            files.Save_data_to_file(path: paths.ShareLicenceJsonFile(), data: json_str);

        }
        #endregion
    }

    #region
    class App_Licence
    {
        public bool IsPurchased { get; set; }
        public string LicenceName { get; set; }
        public string LicenceEmail { get; set; }
        public string LicenceKey { get; set; }
        public DateTime LicencedOn { get; set; }
        public int LicenceDays { get; set; }

        public App_Licence(bool isPurchased, string licenceName, string licenceEmail, string licenceKey,DateTime licencedOn, int licenceDays)
        {
            IsPurchased = isPurchased;
            LicenceName = licenceName;
            LicenceEmail = licenceEmail;
            LicenceKey = licenceKey;
            LicencedOn = licencedOn;
            LicenceDays = licenceDays;
        }
    }
    #endregion
   
}
