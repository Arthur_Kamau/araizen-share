﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Share_Service_Status
    {
        public string Service { get; set; }
        public bool Status  { get; set; }

        public App_Share_Service_Status(bool status, string service = "Araizen  Share")
        {
            Service = service;
            Status = status;
        }
    }
}
