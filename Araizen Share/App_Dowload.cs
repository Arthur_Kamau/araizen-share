﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    public class App_Download
    {
        public String SenderName { get; set; }
        public String SenderEmail { get; set; }
        public bool IsComplete { get; set; }
        public bool IsStarted { get; set; }
        public String FileName { get; set; }
        public long FileSize { get; set; }
        public String FileType { get; set; }
        public int TotalNumberOfFiles { get; set; }
        public int FileEachItemSize { get; set; }
        public int CurrentDownloadItem { get; set; }
        public long UnixTimeFileItemReferenceId { get; set; }
        public String FileDownloadUrl { get; set; }
        public string UploadDate { get; set; }
        public String FileEachItemLocation { get; set; }

        public App_Download(string senderName, string senderEmail, bool isComplete, bool isStarted, string fileName, long fileSize, string fileType, int totalNumberOfFiles, int fileEachItemSize, int currentDownloadItem, long unixTimeFileItemReferenceId, string fileDownloadUrl, string uploadDate, string fileEachItemLocation)
        {
            SenderName = senderName;
            SenderEmail = senderEmail;
            IsComplete = isComplete;
            IsStarted = isStarted;
            FileName = fileName;
            FileSize = fileSize;
            FileType = fileType;
            TotalNumberOfFiles = totalNumberOfFiles;
            FileEachItemSize = fileEachItemSize;
            CurrentDownloadItem = currentDownloadItem;
            UnixTimeFileItemReferenceId = unixTimeFileItemReferenceId;
            FileDownloadUrl = fileDownloadUrl;
            UploadDate = uploadDate;
            FileEachItemLocation = fileEachItemLocation;
        }
    }

    public class App_Download_Update_Data
    {
        public string Itemkey { get; set; }
        public int ProgressValue { get; set; }
        public string ProgressPercentage { get; set; }

        public App_Download_Update_Data(string itemkey, int progressValue, string progressPercentage)
        {
            Itemkey = itemkey;
            ProgressValue = progressValue;
            ProgressPercentage = progressPercentage;
        }
    }



}