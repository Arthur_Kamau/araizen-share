﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_ShareNotifications_Dialogue.xaml
    /// </summary>
    public partial class Ui_ShareNotifications_Error_Dialogue : MetroWindow
    {
        bool procced = true;

        public Ui_ShareNotifications_Error_Dialogue(App_Notification notification)
        {
            InitializeComponent();
        }

        public Boolean DoWeProceed
        {
            get { return procced; }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            procced = false;
            this.Close();
        }

        private void Proceed_Click(object sender, RoutedEventArgs e)
        {
            procced = true;
            this.Close();
        }
    }
}
