﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Service_Communication
    {
    }
     [Serializable]
    public class ShareMessage
    {
        public int Id;
        public string Text;

        public ShareMessage(int id, string text)
        {
            Id = id;
            Text = text;
        }

        public override string ToString()
        {
            return string.Format("\"{0}\" (message ID = {1})", Text, Id);
        }
    }
}
