﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Const
    {
       public static int MaxLoggedInHours = 1000;//1000 hours 
        public static int MaxTrialDays = 45;//1000 hours 

        public static string PipeNameShareServer = "araizen-share-server-";
        public static string PipeNameUpload = "araizen-share-server-upload";
        public static string PipeNameDownload = "araizen-share-server-download";
        public static string PipeNameNotification = "araizen-share-server-notifications";
        public static string PipeNameInternetAvailability = "araizen-share-server-internet-availability";


        //public static int SplitSmall = 20 * 1024 * 1024;
        //public static int SplitMedium = 40 * 1024 * 1024;
        //public static int SplitLarge = 60*1024*1024;

        public static int SplitSmall = 10 * 1024 * 1024;
        public static int SplitMedium = 15 * 1024 * 1024;
        public static int SplitLarge = 20 * 1024 * 1024;
        public static int FileSplitSize()
        {
            string size = Araizen_Share.Properties.Settings.Default.SplitSize;
            if (size.ToLower().Equals("small"))
            {
                return SplitSmall;
            }
            else if (size.ToLower().Equals("medium"))
            {
                return SplitMedium;
            }
            else if (size.ToLower().Equals("large"))
            {
                return SplitLarge;
            }
            else
            {
                return SplitMedium;
            }


        }
    }
}
