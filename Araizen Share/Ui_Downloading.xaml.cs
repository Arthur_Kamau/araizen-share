﻿
using MahApps.Metro.Controls;
using NamedPipeWrapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Downloading.xaml
    /// </summary>
    public partial class Ui_Downloading : Page
    {
        //List<string> item_list = new List<string>();


        //App_System_Communication ac = new App_System_Communication();
        //App_System_Paths path = new App_System_Paths();
        App_Download_Sql download_sql = new App_Download_Sql();
        List<App_Download> downloadingItems;
        List<String> filesInAraizenShareFolder;
        //List<String> uploadDirItems;
        //List<String> itemsInUploadDirNotInJson;

        App_System_Paths paths = new App_System_Paths();
        String pathToUploadDb = string.Empty;
        string[] charsToRemoveForValidName = new string[] { "@", "#", ",", ".", ";", "'", "-", "_", "\\", "/", "  " };
        App_System_Paths ally_paths = new App_System_Paths();

        //pub  model
        App_Download_Pub app_download_pub = new App_Download_Pub();


        private readonly NamedPipeClient<string> AraizenServicePipeDownloads = new NamedPipeClient<string>(App_Const.PipeNameDownload);

       
        //this is the default constructor
        #region Ui_Downloading
        public Ui_Downloading()
        {
            InitializeComponent();

            AraizenServicePipeDownloads.ServerMessage += OnAraizenServiceDownloadConnectionMessage;
            AraizenServicePipeDownloads.Disconnected += OnAraizenServiceDownloadConnectionDisconnected;
            AraizenServicePipeDownloads.Start();
           
            //populate items ui
            ParseSendingObjectsToUi();

        }
        #endregion

        #region GetAllFilesinAraizenShareFolder
        private List<String> GetAllFilesinAraizenShareFolder()
        {
            //    //upload dir  items(children
            List<String> folderNames = new List<string>();

            string shareOperations = ally_paths.ShareOperationsFolder();



            String[] share_folder_contents = Directory.GetDirectories(shareOperations);

            List<string> folder_paths = share_folder_contents.ToList<string>();

            foreach (string folderPath in folder_paths)
            {
                DirectoryInfo dir = new DirectoryInfo(folderPath);


                Console.WriteLine("\n\n getting path {0} get the name {1} \n\n ", folderPath, dir.Name);
                folderNames.Add(dir.Name);
            }

            return folderNames;
        }
        #endregion

        #region ParseSendingObjectsToUi
        private void ParseSendingObjectsToUi()
        {
            //initializethe functions
            //path to db
            pathToUploadDb = paths.DownloadSQL();

            //list from db
            downloadingItems = download_sql.GetDownloadItemsDatabase();

            //update ui that show empty 
            if (downloadingItems != null && downloadingItems.Count > 0)
            {
                content_infomation.Opacity = 0;
            }


            //get if the actua files exist
            filesInAraizenShareFolder = GetAllFilesinAraizenShareFolder();


            foreach (App_Download downloadObj in downloadingItems)
            {
              
                RenderUi(
                    name: downloadObj.FileName,
                    source: downloadObj.SenderEmail,
                    type: downloadObj.FileType,
                    size:   Convert.ToInt32(downloadObj.FileSize),
                    current_upload_item_counterInt: downloadObj.CurrentDownloadItem,
                    fileItemKey: GetFileItemKey(downloadObj.UnixTimeFileItemReferenceId == null ? " " : downloadObj.UnixTimeFileItemReferenceId.ToString(), downloadObj.FileName),
                    unixTimeFileItemReferenceId: downloadObj.UnixTimeFileItemReferenceId == null ? " " : downloadObj.UnixTimeFileItemReferenceId.ToString(),
                     isStarted : downloadObj.IsStarted, 
                     isCompleted : downloadObj.IsComplete,
                    uploadDate : downloadObj.UploadDate
                    );
            }
        }
        #endregion

        #region GetFileItemKey
        string GetFileItemKey(string unixTime, string fileName)
        {
            string originalName = string.Concat(fileName, unixTime);

            //valid name charsToRemoveForValidName
            string filterSpecialCharacters = new string((from c in originalName
                                                         where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                                         select c
          ).ToArray());


            string filterSpacesAndSpecialCharacters = filterSpecialCharacters.Replace(" ", string.Empty);

            return filterSpacesAndSpecialCharacters;

        }
        #endregion

        /// <summary>
        /// add folder items to ui  -->Add_items_to_ui(String name,String owner,String type,long size)
        /// </summary>
        /// <param name="name"></param>
        /// <param name="owner"></param>
        /// <param name="type"></param>
        /// <param name="size"></param>
        /// <param name="target_mail"></param>
        /// <param name="counterInt"></param>
        /// <param name="unixTimeFileItemReferenceId"></param> 
        #region Add_items_to_ui
        private void RenderUi(String name,string source, String type, long size, int current_upload_item_counterInt, string fileItemKey, string unixTimeFileItemReferenceId,bool isStarted, bool isCompleted,string uploadDate)
        {
            App_System_Paths asps = new App_System_Paths();

            Grid item_grid_container = new Grid();
            StackPanel item_grid = new StackPanel();
            StackPanel image_and_comment = new StackPanel();
            StackPanel comments = new StackPanel();

            StackPanel item_details = new StackPanel();
            StackPanel item_details_description = new StackPanel();
            StackPanel download_stream = new StackPanel();



            //who is hosting the content
            Label item_parent_label = new Label
            //
            {
                Content = "Source : \t" + source.ToString()
                //Content = "Targe mail : ke@mai.com"
            };
            //name of the content
            Label item_name_label = new Label
            {
                Content = string.Concat("Name  : \t", name)
            };
            //size
            Label item_size_label = new Label
            {
                Content = string.Concat("Size :  ", Format_length(size))
            };

            //type -> word video music document
            Label item_type_label = new Label();

            int numberOfFiles = (int)Math.Ceiling((double)size / App_Const.FileSplitSize());
           // int percentage = (int)Math.Ceiling((double)(current_upload_item_counterInt / numberOfFiles) * 100);
            string percentage = string.Empty;

           
            if (isStarted)
            {
                //percentage = (int)Math.Ceiling((double)(current_upload_item_counterInt / numberOfFiles) * 100);
             //    percentage = ((current_upload_item_counterInt == 0 ? 1: current_upload_item_counterInt / numberOfFiles ==0 ? 1: numberOfFiles) * 100).ToString();  // (int)Math.Ceiling((double)(current_upload_item_counterInt / numberOfFiles) * 100);

            }
            else
            {
                current_upload_item_counterInt = 0;
                //percentage = (int)Math.Ceiling((double)((current_upload_item_counterInt +1) / numberOfFiles) * 100);
                 //percentage = (((current_upload_item_counterInt + 1) / numberOfFiles == 0 ? 1 : numberOfFiles) * 100).ToString();  // (int)Math.Ceiling((double)(current_upload_item_counterInt / numberOfFiles) * 100);

            }
            StackPanel uploaded_percentage = new StackPanel();
            uploaded_percentage.Orientation = Orientation.Horizontal;
            uploaded_percentage.Width = 100;


            Label uploaded_percentage_staticText = new Label();
            Label uploaded_percentage_dynamicText = new Label();

            uploaded_percentage_staticText.Content = "Upload: ";
            //set unique name for ui updating

            uploaded_percentage_dynamicText.Name = fileItemKey + "uploadValue";
            uploaded_percentage_dynamicText.Content = percentage + "%";

            uploaded_percentage.Children.Add(uploaded_percentage_staticText);
            uploaded_percentage.Children.Add(uploaded_percentage_dynamicText);



            item_type_label.Content = string.Concat("  Type  :  ", type);


            //progress indicator
            MetroProgressBar upload_progress = new MetroProgressBar();
            upload_progress.Minimum = 0;
            upload_progress.Maximum = numberOfFiles;
            upload_progress.MaxHeight = 4;
            upload_progress.Height = 3;
            upload_progress.Value = current_upload_item_counterInt;
            //set unique name for ui updating
            upload_progress.Name = fileItemKey + "uploadProgressBarValue";


            upload_progress.Foreground = new System.Windows.Media.SolidColorBrush(Colors.Indigo);


            app_download_pub.OnChange += (sender, e) =>
            {
                Console.WriteLine("\n\n =========>>>>>   subscriber download data with  => " + e.Value);

                App_Download_Update_Data upload_data = JsonConvert.DeserializeObject<App_Download_Update_Data>(e.Value);

                if (upload_data.Itemkey.Equals(fileItemKey))
                {

                    Dispatcher.Invoke(() =>
                    {
                        uploaded_percentage_dynamicText.Content = upload_data.ProgressPercentage + " %";
                        upload_progress.Value = upload_data.ProgressValue;
                    });


                    if (upload_data.ProgressPercentage.Equals("100"))
                    {
                        Dispatcher.Invoke(() =>
                        {
                            //clean ui.
                            // uploading_contents.Children.Clear();

                            //recall parser to fill ui
                            // ParseSendingObjectsToUi();
                        });

                    }
                }

            };


            Image item_image = new Image();
        
            item_grid.Orientation = Orientation.Horizontal;
            item_grid.Width = uploading_contents.Width;
            item_grid.Height = 100;

            image_and_comment.Orientation = Orientation.Vertical;
            //comments.Orientation = Orientation.Horizontal;

            item_image.Width = 100; item_image.Height = 100;


            //comment_button.Height = 25; comment_button.Width = 25;
            //thumbs_up_button.Height = 25; thumbs_up_button.Width = 25;
            //thumbs_down_button.Height = 25; thumbs_down_button.Width = 25;

            comments.Height = 30; comments.Width = 90;
           
            image_and_comment.Children.Add(item_image);
            // image_and_comment.Children.Add(comments);

            //item details
            item_details.Width = 500;
            item_details.Height = 100;
            item_details.Children.Add(item_name_label);
            item_details.Children.Add(item_parent_label);

            item_details_description.Orientation = Orientation.Horizontal;
            item_details_description.Children.Add(item_size_label);
            item_details_description.Children.Add(item_type_label);
            item_details_description.Children.Add(uploaded_percentage);

            //add item_details_description into item_details
            item_details.Children.Add(item_details_description);

            download_stream.Orientation = Orientation.Vertical;




            List<string> video_ext = new List<string>() { ".mp4", ".mov", ".wmv", ".avi", ".mkv", ".flv" };
            List<string> image_ext = new List<string>() { ".png", ".jpeg", ".gif", ".bmp", ".tiff", ".psd", ".webp" };
            List<string> music_ext = new List<string>() { ".mp3", ".ogg", ".aac", ".atrac", ".aiff", ".wma", ".vorbis", ".opus" };
            List<string> wordDocument_ext = new List<string>() { ".doc", ".docx", ".dotx", ".rtf", ".dot" };
            List<string> textFile_ext = new List<string>() { ".txt", ".log" };
            List<string> webFile_ext = new List<string>() { ".html", ".htm" };




            if (video_ext.Contains(type.ToLower()))
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/video_file.png", UriKind.Relative));


            }
            else if (wordDocument_ext.Contains(type.ToLower()))
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/WinWordLogoSmall.scale-140.png", UriKind.Relative));


            }

            else if (type.ToLower().Equals(".pdf"))
            {


                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/pdf.png", UriKind.Relative));


            }
            else if (type.ToLower().Equals(".xml"))
            {
                //String resourceDir = asps.Curr_Resources_Dir();
                //System.Uri resourceImageUri = new System.Uri(System.IO.Path.Combine(resourceDir, "images/file.jpg"));

                //Console.WriteLine("->Resource Dir ={0} \n resource image uri ={1}", asps.Curr_Resources_Dir(), resourceImageUri);

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/file.jpg", UriKind.Relative));

            }

            else if (music_ext.Contains(type.ToLower()))
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/audio_file_2.png", UriKind.Relative));

            }
            else if (image_ext.Contains(type.ToLower()))
            {

                //System.Uri resourceImageUri = new System.Uri(file_path);

                //item_image.Source = new BitmapImage();

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/files.png", UriKind.Relative));

            }
            else
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/files.png", UriKind.Relative));

            }


            //comment_image.Source = new BitmapImage(new Uri(@"C:\Users\Arthur-Kamau\source\repos\sambaza-desktop-ui\sambaza-desktop-ui\resources\ic_comment_black_24dp.png"));
            //thumbs_down_image.Source = new BitmapImage(new Uri(@"C:\Users\Arthur-Kamau\source\repos\sambaza-desktop-ui\sambaza-desktop-ui\resources\ic_thumb_down_black_24dp_1x.png"));

            //thumbs_up_image.Source = new BitmapImage(new Uri(@"C:\Users\Arthur-Kamau\source\repos\sambaza-desktop-ui\sambaza-desktop-ui\resources\ic_thumb_up_black_24dp_1x.png"));



            item_grid.Children.Add(image_and_comment);
            item_grid.Children.Add(item_details);
            item_grid.Children.Add(download_stream);
            item_details.Children.Add(upload_progress);


            item_grid.Margin = new Thickness(0, 10, 0, 0);


            ///confirm the file actually exist not just a database entry
            ///debuging
            ///
            Console.WriteLine("\n\n -----> check all the files in {0}", ally_paths.ShareOperationsFolder());
            foreach (string item in filesInAraizenShareFolder)
            {
                Console.WriteLine("\n\n --------- file items in araizen folder {0}  ----------\n\n", System.IO.Path.GetDirectoryName(item));
            }


            //controls grid
            StackPanel errorStack = new StackPanel();


            //delete button
            Button error_button = new Button();
            error_button.FindResource(ToolBar.ButtonStyleKey);
            error_button.BorderThickness = new Thickness(0, 0, 0, 0);

            //delete icon
            Image error_item_image = new Image
            {
                Width = 25,
                Height = 25,
                Source = new BitmapImage(new Uri(@"/Resources/images/ic_delete_black_18dp.png", UriKind.Relative))
            };

            error_button.Content = error_item_image;

            error_button.Click += (object sender, RoutedEventArgs e) =>
            {
                Console.WriteLine("\n\n\n  ----------------->>>>>> delete by user choice item folder {0} \n\n\n", System.IO.Path.Combine(ally_paths.ShareUploadFolder(), name));
                download_sql.DeletedownloadItemsDatabase(
                      senderEmail : source ,  fileName : name,  uploadDate: uploadDate
                 );
               
                //delet the file
                try
                {
                    string deleteFilePath = System.IO.Path.Combine(ally_paths.ShareUploadFolder(), name);
                    System.IO.Directory.Delete(deleteFilePath, true);

                }
                catch
                {
                    Console.WriteLine("erro deleting file use initiated");
                }
                //clean ui.
                uploading_contents.Children.Clear();

                //recall parser to fill ui
                ParseSendingObjectsToUi();

            };

            error_button.Height = 35;
            error_button.Width = 35;
            Style style = this.FindResource("buttonstyle") as Style;
            error_button.Style = style;

            errorStack.Children.Add(error_button);

            errorStack.HorizontalAlignment = HorizontalAlignment.Right;
            errorStack.VerticalAlignment = VerticalAlignment.Top;

            errorStack.Opacity = 1;

            //add to ui 
            // item_grid.Opacity = 0.167;

            //use the special item_grid_container
            item_grid_container.Children.Add(item_grid);
            //add erro stack later
            //to be able to access the click function
            item_grid_container.Children.Add(errorStack);

            uploading_contents.Children.Add(item_grid_container);

           
            

        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataItem"></param>
        #region CallRefreshUi
        private void CallRefreshUi(string downloadData)
        {
            Console.WriteLine("publisher refresh ui downloadData data with  => " + downloadData);

            app_download_pub.Raise(downloadData);
        }
      #endregion


        #region formt length
        private string Format_length(long len)
        {
            string[] sizes = { "b", "KB", "MB", "GB", "TB" };
            int order = 0;
            while (len >= 1024 && order < sizes.Length - 1)
            {
                order++;
                len = len / 1024;
            }
            String result = string.Format("{0:0.##} {1}", len, sizes[order]);
            Console.WriteLine("-->{0}", result);
            return result;
        }
        #endregion

        #region page loaded
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
           

        }
        #endregion


        #region Download comms components
        #region OnAraizenServiceDownloadConnectionMessage
        private void OnAraizenServiceDownloadConnectionMessage(NamedPipeConnection<string, string> connection, string message)
        {           
            CallRefreshUi(message);
        }
        #endregion

        #region OnAraizenServiceDownloadConnectionDisconnected
        private void OnAraizenServiceDownloadConnectionDisconnected(NamedPipeConnection<string, string> connection)
        {
            Console.WriteLine("<b>Disconnected from server</b>");

        }
        #endregion
        
        #region AraizenServiceDownloadSendMessage
        public void AraizenServiceDownloadSendMessage(App_Download downloadItem)
        {
          
            string json_str = JsonConvert.SerializeObject(downloadItem);
            AraizenServicePipeDownloads.PushMessage(json_str);


        }
        #endregion
        #endregion

    }
}
