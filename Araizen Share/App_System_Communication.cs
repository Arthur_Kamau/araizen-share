﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_System_Communication
    {
        //private string name;
       // private string email;
        //private string user_key;
       // private string user_account_type;

        private string os_version;
        private string comupter_mac;
        private string comupter_name;
        private string app_build;
        private string app_version;

       // private DateTime InstallTime;

        App_Protect_String ProtectString = new App_Protect_String();

        #region constructor
        //constructor to set variables
        public App_System_Communication()
        {
            App_Resource res = new App_Resource();
           

           // App_Register UserDetails = res.GetRegisterDetails();
            App_Purchase_Server userPurchaseDetails = new App_Purchase_Server();

           // user_key = ProtectString.DecryptString(UserDetails.Userkey);
           // user_account_type = ProtectString.DecryptString(UserDetails.AccountType);
            
            comupter_mac = App_System_Details.GetMacAddress();
            comupter_name = App_System_Details.GetComputerName(); 

          //  email = ProtectString.DecryptString(UserDetails.Email);
          //  name = ProtectString.DecryptString(UserDetails.Name);

            app_build = App_Object.GetApp_Build_isPrelease().ToString();
            app_version = App_Object.GetApp_Version();

          //  InstallTime = UserDetails.RegisterTime;
            os_version = App_System_Details.OsVersion();

        }
        #endregion

        #region SendRegisterDetails
        public bool SendRegisterDetails(String email,String name,String user_account_type,DateTime InstallTime)
        {
            /**
             * 
             *     email;
             *    name;
             *    account type;
             *    user_key;
             *    install_time;
             *     computer_name;
             *   computer_mac_address;
             */
            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();

                
                post_data.Add("user_email", email);
                post_data.Add("user_name", name);
                post_data.Add("user_account_type",user_account_type );
                post_data.Add("install_time", InstallTime.ToString());
                post_data.Add("computer_name", comupter_name);
                post_data.Add("computer_mac_address", comupter_mac);

                Console.WriteLine("Send reg details {0}",email);

                try
                {
                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/user_register_details/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("SendRegisterDetails my output {0}", responsebody);
                    if (response.Equals("ok"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }catch(Exception ex)
                {
                    Console.WriteLine(" error in user_register_details {0} ", ex);
                    return true;
                }
            }

        }
        #endregion

        #region Auth password
        public bool AuthPass(string email,string pass)
        {
            return true;
        }
        #endregion

        #region SendComputerInstallDetails
        public bool SendComputerInstallDetails()
        {
            /**
             *  user_key;
                install_time;
                computer_name;
                os_version
                computer_mac_address;
             * 
             */
            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();

                
                post_data.Add("install_time", DateTime.Now.ToString());
                post_data.Add("computer_name", comupter_name);
                 post_data.Add("os_version", os_version);
                post_data.Add("computer_mac_address", comupter_mac);

                try
                {
                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/user_computer_details/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("SendComputerInstallDetails my output {0}", responsebody);
                    if (response.Equals("ok"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }catch(Exception ex)
                {
                    Console.WriteLine("Error in user_computer_details {0}  ",ex);
                    return true;
                }
            }

        }
        #endregion

        #region SendFeedBack
        public bool SendFeedBack(string feedbacktext,String email)
        {
            /**
             * name
             * email
             * send time
             * feedback
             */

            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();

                
                post_data.Add("user_email", email);
                post_data.Add("sendtime", DateTime.Now.ToString());
                post_data.Add("feedbacktext", feedbacktext);

                try
                {
                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/feedback/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("SendFeedBack my output {0}", responsebody);
                    if (responsebody.Equals("ok"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }catch(Exception ex)
                {
                    Console.WriteLine("Error in  feedback {0}",ex);
                    return true;
                }
            }
            
        }
        #endregion

        #region GetPurchaseDetails
        public string GetPurchaseDetails(string user_purchase_key,String email)
        {
            /**
             *  user_key;
                user_email;
                user_purchase_key;
             * 
             */
            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();

                
                post_data.Add("user_email", email);
                post_data.Add("user_purchase_key", user_purchase_key);

                try
                {
                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/get_purchase_details/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("GetPurchaseDetails my output {0}", responsebody);
                    return responsebody;
                }catch(Exception ex)
                {
                    Console.WriteLine("Error in get_purchase_details {0}",ex);
                    return string.Empty;
                }
            }

        }
        #endregion

        #region ConfirmPurchaseDetails
        public Boolean ConfirmPurchaseDetails(string user_purchase_key,String email)
        {
            /**
             * user_key
             *  user_email;
             *   user_purchase_key;
             * 
             */
            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();

                
                post_data.Add("user_email", email);
                post_data.Add("user_purchase_key", user_purchase_key);

                try
                {
                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/confirm_purchase_details/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("ConfirmPurchaseDetails my output {0}", responsebody);
                    if (responsebody.Equals("ok"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }catch(Exception ex)
                {
                    Console.WriteLine("Error in confirm_purchase_details error {0}", ex);
                    return false;
                }
            }

        }
        #endregion

        #region CheckNewVersion
        public string CheckNewVersion(String email,String build,String platformType)
        {
            /**
             *  App_Build
             *  App_PlatformType
             * 
             */
            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();


                post_data.Add("app_build", build);
                post_data.Add("app_platformtype", platformType);
                post_data.Add("email", email);

                try
                {

                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/new_version/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("CheckNewVersion my output {0}", responsebody);
                    return responsebody;
                }catch(Exception ex)
                {
                    Console.WriteLine("Error in new version {0}", ex);
                    return string.Empty;
                }
            }

        }
        #endregion

        #region forgot_password
        public Boolean forgot_password(string email_par)
        {
            /**
             *  User_Key
             *  email 
             * 
             */
            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();

                
                post_data.Add("user_email", email_par);

                Console.WriteLine("email par in forgot password {0}",email_par);

                try
                {
                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/forgot_password/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("forgot_password my output {0}", responsebody);
                    if (responsebody.ToLower().Equals("ok"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }catch(Exception ex)
                {
                    Console.WriteLine("error in forgot_password {0}",ex);
                    return false;
                }
            }

        }
        #endregion

        #region forgot_password_serial_confirm
        public Boolean forgot_password_serial_confirm(string serial,String email)
        {
            /**
             *  User_Key
             *  email 
             *  serial
             * 
             */
            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();

                
                post_data.Add("user_email", email);
                post_data.Add("serial", serial);


                try
                {
                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/forgot_password_serial_confirm/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("forgot_password_serial_confirm my output {0}", responsebody);
                    if (responsebody.Equals("ok"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }catch(Exception ex)
                {
                    
                    Console.WriteLine("error forgot_password_serial_confirm  {0}",ex);
                    return false;
                }
            }

        }
        #endregion

        #region previous_account_details
        public string previous_account_details(string user_key_par, string email_par)
        {
            /**
             * email;
             * user_key;
             */
            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();


                post_data.Add("user_key", user_key_par);
                post_data.Add("user_email", email_par);

                try
                {
                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/previous_account/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("previous_account_details my output {0}", responsebody);
                    return responsebody;
                }catch(Exception ex)
                {
                    Console.WriteLine(" error in previous_account {0}", ex);
                    return string.Empty;
                }
            }

        }
        #endregion

        #region update_email
        public Boolean update_email(string email_new)
        {
            /**
             * email;
             * user_key;
             * 
             */
            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();

                
                post_data.Add("email_new", email_new);


                try
                {
                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/update_email/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("update_email my output {0}", responsebody);
                    if (response.Equals("ok"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Error in update_email {0}", ex);
                    return false;
                }
            }

        }
        #endregion

        #region block_application
        public Boolean block_application(int period, string reason)
        {
            /**
                    user_key;
                    computer_mac_address;
                    period;
                    reason;
             * 
             */
            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();

                
                post_data.Add("computer_mac_address", comupter_mac);
                post_data.Add("period", period.ToString());
                post_data.Add("reason", reason);

                try
                {
                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/block_application/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("block_application my output {0}", responsebody);
                    if (response.Equals("ok"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }catch(Exception ex)
                {
                    Console.WriteLine("Error in block_application  {0}",ex);
                    return false;
                }
            }

        }
        #endregion

        #region unblock_application
        public Boolean unblock_application( string unblock_key,String email)
        {
            /**
                    user_key;
                    computer_mac_address;
                    period;
                    reason;
             * 
             */
            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();


                post_data.Add("email", email);
                post_data.Add("unblock_key", unblock_key);



                try
                {
                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/unblock_application/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("unblock_application my output {0}", responsebody);
                    if (response.Equals("ok"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }catch(Exception ex)
                {
                    Console.WriteLine("Error unblock_application detail {0}",ex);
                    return false;
                }
            }

        }
        #endregion

        #region is_app_genuine
        public Boolean is_app_genuine(string user_purchase_key,String email)
        {
            /**
             *       user_key;
             *      user_email;
             *      user_purchase_key;
             * 
             */
            using (var client = new WebClient())
            {
                var post_data = new System.Collections.Specialized.NameValueCollection();

                
                post_data.Add("user_email", email);
                post_data.Add("purchase_key", user_purchase_key);



                try
                {
                    byte[] response = client.UploadValues("https://www.araizen.com/share/comms/v2/is_app_genuine/", "POST", post_data);
                    var responsebody = Encoding.UTF8.GetString(response);
                    Console.WriteLine("is_app_genuine my output {0}", responsebody);
                    if (response.Equals("ok"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }catch(Exception ex)
                {
                    Console.WriteLine(" error in is_app_genuine {0} ",ex);
                    return true;
                }
            }

        }
        #endregion

    }
}
