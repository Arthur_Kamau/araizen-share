﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for feedback.xaml
    /// </summary>
    public partial class Ui_Feedback : MetroWindow
    {
        String emailstr;
        App_Auth_File authFile = new App_Auth_File();

        public Ui_Feedback()
        {
            InitializeComponent();

      
            App_Resource res = new App_Resource();
            App_Auth authDetails = authFile.GetAuthDetailsFile();

            App_Protect_String ProtectString = new App_Protect_String();

            emailstr = authDetails.Email;

            FeedBackNow.Content = DateTime.Now;

            var th = new Task(() =>
            {
                while (true)
                {
                    var IsThereInterent = App_System_Wifi.CheckForInternetConnection();
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        if (IsThereInterent)
                        {
                            Submit.IsEnabled = true;
                            InternetStatus.Content = "There Is Internet";

                        }
                        else
                        {
                            Submit.IsEnabled = false;
                            InternetStatus.Content = " No Internet";
                        }
                    });
                    Thread.Sleep(1500);
                }
            });
            th.Start();
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {

            // String emailstr = EmailField.Text;

            DateTime SendTime = DateTime.Now;
            String feedBack = TextFeedbackContents.Text;
            App_Resource res = new App_Resource();
            App_Auth authDetails = authFile.GetAuthDetailsFile();


            
            App_Feedback_Items itemFeedback = new App_Feedback_Items(feedbackText: feedBack, userEmail: authDetails.Email, feedbackTime: DateTime.Now);
            App_Feedback feedback = new App_Feedback();
            bool result = feedback.SendAppUserFeedBack(feedBack: itemFeedback);


            if (result)
            {
                this.Close();
            }
            else
            {
                InternetStatus.Content = "Internet Error,try again.";
            }

        }
    }
}
