﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for User_History.xaml
    /// </summary>
    public partial class Ui_UserHistory : Page
    {
        //App_System_Communication ac = new App_System_Communication();
        //App_System_Paths path = new App_System_Paths();
        App_History_Sql historySql = new App_History_Sql();
        List<App_History> historyItems;
        List<String> filesInAraizenShareFolder;
       

        App_System_Paths paths = new App_System_Paths();
        String pathToUploadDb = string.Empty;
        string[] charsToRemoveForValidName = new string[] { "@", "#", ",", ".", ";", "'", "-", "_", "\\", "/", "  " };
        App_System_Paths ally_paths = new App_System_Paths();

        public Ui_UserHistory()
        {
            InitializeComponent();


            //populate items ui
            ParseSendingObjectsToUi();

        }

        private List<String> getAllFilesinAraizenShareFolder()
        {
            //    //upload dir  items(children
            List<String> folderNames = new List<string>();

            string shareOperations = ally_paths.ShareOperationsFolder();



            String[] share_folder_contents = Directory.GetDirectories(shareOperations);

            List<string> folder_paths = share_folder_contents.ToList<string>();

            foreach (string folderPath in folder_paths)
            {
                DirectoryInfo dir = new DirectoryInfo(folderPath);


                Console.WriteLine("\n\n getting path {0} get the name {1} \n\n ", folderPath, dir.Name);
                folderNames.Add(dir.Name);
            }

            return folderNames;
        }

        private void ParseSendingObjectsToUi()
        {
            //initializethe functions
            //path to db
            pathToUploadDb = paths.UploadSQL();

            //list from db
            historyItems = historySql.GetHistoryItemsDatabase();

            //update ui that show empty 
            if (historyItems != null && historyItems.Count > 0)
            {
                content_infomation.Opacity = 0;
            }


            //get if the actua files exist
            filesInAraizenShareFolder = getAllFilesinAraizenShareFolder();


            foreach (App_History historyObj in historyItems)
            {
                //SortParsedSendingFileItemsToUi(uploadObjFileItem: uploadObj.FileItem, destinationEmail: uploadObj.DestinationEmailClients);

                RenderUi(
                    action: historyObj.Action,
                    name: historyObj.FileName,
                    destination: historyObj.DestinationEmail,
                    type: historyObj.FileType,
                    size: Convert.ToInt32(historyObj.FileSize),
                    source: historyObj.SourceEmail,
                    fileItemKey: GetFileItemKey(historyObj.UnixTimeFileItemReferenceId, historyObj.FileName),
                    completionDate: historyObj.CompletionDate.ToString(),
                    unixTimeFileItemReferenceId: historyObj.UnixTimeFileItemReferenceId == null ? " " : historyObj.UnixTimeFileItemReferenceId.ToString()
                    );
            }
        }

        string GetFileItemKey(string unixTime, string fileName)
        {
            string originalName = string.Concat(fileName, unixTime);

            //valid name charsToRemoveForValidName
            string filterSpecialCharacters = new string((from c in originalName
                                                         where char.IsWhiteSpace(c) || char.IsLetterOrDigit(c)
                                                         select c
          ).ToArray());


            string filterSpacesAndSpecialCharacters = filterSpecialCharacters.Replace(" ", string.Empty);

            return filterSpacesAndSpecialCharacters;

        }

        /// <summary>
        /// add folder items to ui  -->Add_items_to_ui(String name,String owner,String type,long size)
        /// </summary>
        /// <param name="name"></param>
        /// <param name="owner"></param>
        /// <param name="type"></param>
        /// <param name="size"></param>
        /// <param name="target_mail"></param>
        /// <param name="counterInt"></param>
        /// <param name="unixTimeFileItemReferenceId"></param> 
        #region Add_items_to_ui
        private void RenderUi(string action, String name, string destination, String type, long size, string source, string fileItemKey, string completionDate, string unixTimeFileItemReferenceId="")
        {
            App_System_Paths asps = new App_System_Paths();

            Grid item_grid_container = new Grid();
            StackPanel item_grid = new StackPanel();
            StackPanel image_and_comment = new StackPanel();
            StackPanel comments = new StackPanel();

            StackPanel item_details = new StackPanel();
            StackPanel item_details_description = new StackPanel();
            StackPanel download_stream = new StackPanel();



            //who is hosting the content
            Label item_parent_label = new Label
            //
            {
                Content = destination.ToString()
                //Content = "Targe mail : ke@mai.com"
            };
            //name of the content
            Label item_name_label = new Label
            {
                Content = string.Concat("Name \t", name)
            };
            //size
            Label item_size_label = new Label
            {
                Content = string.Concat("Size ", Format_length(size))
            };
            //type -> word video music document
            Label item_type_label = new Label();

           
            StackPanel uploaded_percentage = new StackPanel();
            uploaded_percentage.Orientation = Orientation.Horizontal;
            uploaded_percentage.Width = 120;
            uploaded_percentage.Height = 33;


            uploaded_percentage.Margin = new Thickness(18, 0, 0, 0);


            Label uploaded_percentage_staticText = new Label();
            Label uploaded_percentage_dynamicText = new Label();
            uploaded_percentage_staticText.Foreground = new System.Windows.Media.SolidColorBrush(Colors.Black);
            uploaded_percentage_dynamicText.Foreground = new System.Windows.Media.SolidColorBrush(Colors.Black);

            uploaded_percentage_staticText.Content = "Action: "+action;
            //set unique name for ui updating

            uploaded_percentage.Children.Add(uploaded_percentage_staticText);

            item_type_label.Content = string.Concat("\t\t\t Type  :", type);


            //progress indicator
            Label upload_progress_complete_date = new Label();

            //set unique name for ui updating
            upload_progress_complete_date.Name = fileItemKey + "uploadProgressBarValue";
            upload_progress_complete_date.Content = "Completed on"+ completionDate;




            Image item_image = new Image();
            // item_image.Height = 70;
            // item_image.Width = 80;

            //Image comment_image = new Image();
            //Image thumbs_up_image = new Image();
            //Image thumbs_down_image = new Image();



            item_grid.Orientation = Orientation.Horizontal;
            item_grid.Width = history_contents.Width;
            item_grid.Height = 100;

            image_and_comment.Orientation = Orientation.Vertical;
            //comments.Orientation = Orientation.Horizontal;

            item_image.Width = 100; item_image.Height = 100;


            //comment_button.Height = 25; comment_button.Width = 25;
            //thumbs_up_button.Height = 25; thumbs_up_button.Width = 25;
            //thumbs_down_button.Height = 25; thumbs_down_button.Width = 25;

            comments.Height = 30; comments.Width = 90;
          
            image_and_comment.Children.Add(item_image);
           
            //item details
            item_details.Width = 500;
            item_details.Height = 100;
            item_details.Children.Add(item_name_label);
            item_details.Children.Add(item_parent_label);

            item_details_description.Orientation = Orientation.Horizontal;
            item_details_description.Children.Add(item_size_label);
            item_details_description.Children.Add(item_type_label);
            //item_details_description.Children.Add(upload_progress_complete_date);

            //add item_details_description into item_details
            item_details.Children.Add(item_details_description);

            download_stream.Orientation = Orientation.Vertical;




            List<string> video_ext = new List<string>() { ".mp4", ".mov", ".wmv", ".avi", ".mkv", ".flv" };
            List<string> image_ext = new List<string>() { ".png", ".jpeg", ".gif", ".bmp", ".tiff", ".psd", ".webp" };
            List<string> music_ext = new List<string>() { ".mp3", ".ogg", ".aac", ".atrac", ".aiff", ".wma", ".vorbis", ".opus" };
            List<string> wordDocument_ext = new List<string>() { ".doc", ".docx", ".dotx", ".rtf", ".dot" };
            List<string> textFile_ext = new List<string>() { ".txt", ".log" };
            List<string> webFile_ext = new List<string>() { ".html", ".htm" };




            if (video_ext.Contains(type.ToLower()))
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/video_file.png", UriKind.Relative));


            }
            else if (wordDocument_ext.Contains(type.ToLower()))
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/WinWordLogoSmall.scale-140.png", UriKind.Relative));


            }

            else if (type.ToLower().Equals(".pdf"))
            {


                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/pdf.png", UriKind.Relative));


            }
            else if (type.ToLower().Equals(".xml"))
            {
                //String resourceDir = asps.Curr_Resources_Dir();
                //System.Uri resourceImageUri = new System.Uri(System.IO.Path.Combine(resourceDir, "images/file.jpg"));

                //Console.WriteLine("->Resource Dir ={0} \n resource image uri ={1}", asps.Curr_Resources_Dir(), resourceImageUri);

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/file.jpg", UriKind.Relative));

            }

            else if (music_ext.Contains(type.ToLower()))
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/audio_file_2.png", UriKind.Relative));

            }
            else if (image_ext.Contains(type.ToLower()))
            {

                //System.Uri resourceImageUri = new System.Uri(file_path);

                //item_image.Source = new BitmapImage();

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/files.png", UriKind.Relative));

            }
            else
            {

                item_image.Source = new BitmapImage(new Uri(@"/Resources/images/files.png", UriKind.Relative));

            }


            item_grid.Children.Add(image_and_comment);
            item_grid.Children.Add(item_details);
            item_grid.Children.Add(download_stream);
            item_details.Children.Add(upload_progress_complete_date);


            item_grid.Margin = new Thickness(0, 10, 0, 0);


            //controls grid
            StackPanel errorStack = new StackPanel();


            //delete button
            Button error_button = new Button();
            error_button.FindResource(ToolBar.ButtonStyleKey);
            error_button.BorderThickness = new Thickness(0, 0, 0, 0);

            //delete icon
            Image error_item_image = new Image
            {
                Width = 25,
                Height = 25,
                Source = new BitmapImage(new Uri(@"/Resources/images/ic_delete_black_18dp.png", UriKind.Relative))
            };

            error_button.Content = error_item_image;

            error_button.Click += (object sender, RoutedEventArgs e) =>
            {
                Console.WriteLine("\n\n\n  ----------------->>>>>> delete by user choice item folder {0} \n\n\n", System.IO.Path.Combine(ally_paths.ShareUploadFolder(), name));
                historySql.DeleteHistoryItemsDatabase(
                     fileName : name,  senderEmail: source,  recepientEmail : destination
                  
                );
                
                
                //clean ui.
                history_contents.Children.Clear();

                //recall parser to fill ui
                ParseSendingObjectsToUi();

            };

            error_button.Height = 35;
            error_button.Width = 35;
            Style style = this.FindResource("buttonstyle") as Style;
            error_button.Style = style;

            errorStack.Children.Add(error_button);

            errorStack.HorizontalAlignment = HorizontalAlignment.Right;
            errorStack.VerticalAlignment = VerticalAlignment.Top;

            errorStack.Opacity = 1;

            //add to ui 
            // item_grid.Opacity = 0.167;

            //use the special item_grid_container
            item_grid_container.Children.Add(item_grid);
            //add erro stack later
            //to be able to access the click function
            item_grid_container.Children.Add(errorStack);

            history_contents.Children.Add(item_grid_container);

        }
       
           
        

        
        #endregion

        #region formt length
        private string Format_length(long len)
        {
            string[] sizes = { "b", "KB", "MB", "GB", "TB" };
            int order = 0;
            while (len >= 1024 && order < sizes.Length - 1)
            {
                order++;
                len = len / 1024;
            }
            String result = string.Format("{0:0.##} {1}", len, sizes[order]);
            Console.WriteLine("-->{0}", result);
            return result;
        }
        #endregion

    }
}
