﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using static System.Console;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Ui_Settings : Page
    {
        App_Share_Service_Status_File shareServiceStatus = new App_Share_Service_Status_File();
       // App_Share_Service_Status  = null;
        bool fromConstructor = false;
        public Ui_Settings()
        {
            InitializeComponent();

            SetUpUserSettings();

            SetUpApplicationDetail();

        }

        static public event EventHandler openLoginRegisterOption;
        private void Logout_Click(object sender, RoutedEventArgs e)
        {

            //show warning dialogue
            Ui_LogoutDialogue w = new Ui_LogoutDialogue();
            if (w.ShowDialog() == false)
            {
               bool proceed  = w.DoWeProceedToLogOut;
                //if true exist
                if (proceed)
                {

                    killAndOrStopAraizenService();

                    //delete files
                    try
                    {
                        DeleteAuthFiles();
                    }catch(Exception ex)
                    {
                        Console.WriteLine("Error" +ex);
                    }
                    if (openLoginRegisterOption != null)
                    {
                        openLoginRegisterOption(this, e);
                    }
                }
                
            }
        }

        private void killAndOrStopAraizenService()
        {
            var listItems_console = Process.GetProcessesByName("Araizen_share_console");
            var listItems_service = Process.GetProcessesByName("Araizen_share_service");
            // kill araizen service process
            foreach (var process in listItems_console) { process.Kill(); }
            foreach (var process in listItems_service) { process.Kill(); }

            //stop the service

            if (ShareServiceState().ToLower().Equals("running") || ShareServiceState().ToLower().Equals("starting"))
            {
                StartService("AraizenShareService", 300);
            }
        }


        #region DeleteApplicationData
        private void KillServiceDeleteApplicationDataAndStopApplication()
        {
           
            //stop service
            //stop console application if running
            killAndOrStopAraizenService();
            //delete
            // notifications , download , upload  & history
            DeleteDatabaseFiles();
            //delete registry key if app is licence is purchased
            DeleteRegistryEntries();

            //delete all the files  in araizen operations folder
            //staging area
            App_System_Paths paths = new App_System_Paths();
            System.IO.DirectoryInfo d2 = new DirectoryInfo(path: paths.ShareOperationsFolder());
            foreach (FileInfo file in d2.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in d2.GetDirectories())
            {
                dir.Delete(true);
            }


            //restart application
            System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
        }
        #endregion

        #region DeleteRegistryEntries
        private void DeleteRegistryEntries()
        {
            App_Registry registery = new App_Registry();
            string serial = registery.GetPurchaseKey();

            //if not null or empty
            if (!string.IsNullOrEmpty(serial))
            {
                registery.StorePurchaseKey(string.Empty);
                registery.StorePurchasePeriod(string.Empty);
                //set date to app releas dat
                DateTime value = new DateTime(2019, 2, 28);
                registery.StorePurchaseDate(value);

            }
           
        }
        #endregion
        #region DeleteDatabaseFiles
        private void DeleteDatabaseFiles()
        {
            App_System_Paths paths = new App_System_Paths();

            if (File.Exists(paths.NotificationsSQL()))
            {
                File.Delete(paths.NotificationsSQL());
            }
            else { WriteLine($"{paths.NotificationsSQL()}   does not exist"); }

            if (File.Exists(paths.HistorySQL()))
            {
                File.Delete(paths.HistorySQL());
            }
            else { WriteLine($"{paths.HistorySQL()}   does not exist"); }

            if (File.Exists(paths.DownloadSQL()))
            {
                File.Delete(paths.DownloadSQL());
            }
            else { WriteLine($"{paths.DownloadSQL()}   does not exist"); }

            if (File.Exists(paths.UploadSQL()))
            {
                File.Delete(paths.UploadSQL());
            }
            else { WriteLine($"{paths.UploadSQL()}   does not exist"); }
        }
        #endregion

        #region DeleteAuthFiles
        private void DeleteAuthFiles()
        {
            App_System_Paths paths = new App_System_Paths();
            Console.WriteLine("\n json etc Delete all files and folders in {0}", paths.GetSystemApplicationDataStorage());
            Console.WriteLine("\n uploads download etc Delete all files and folders in {0}", paths.ShareOperationsFolder());
            //json file etc
            System.IO.DirectoryInfo di = new DirectoryInfo(path:paths.GetSystemApplicationDataStorage());
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
            //staging area
            System.IO.DirectoryInfo d2 = new DirectoryInfo(path: paths.ShareOperationsFolder());
            foreach (FileInfo file in d2.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in d2.GetDirectories())
            {
                dir.Delete(true);
            }
        }
        #endregion


        #region openHistoryFromSettings event handler
        static public event EventHandler openHistoryFromSettings;
        #endregion

        #region Open Histroty ui 
        private void Histoy_Click(object sender, RoutedEventArgs e)
        {
            if (openHistoryFromSettings != null)
            {
                openHistoryFromSettings(this, e);
            }
        }
        #endregion


        static public event EventHandler openDocumetationFromSettings;

        #region Documentation_Click
        private void Documentation_Click(object sender, RoutedEventArgs e)
        {
            if (openDocumetationFromSettings != null)
            {
                openDocumetationFromSettings(this, e);
            }
        }
        #endregion

        #region SetUpApplicationDetail
        private void SetUpApplicationDetail()
        {
            //version info
           String Versionstr = App_Object.GetApp_Version(); 
           Version.Content = Versionstr;

            //build special type
            bool isSpecial = App_Object.GetApp_Build_IsSpecial();
            if (isSpecial)
            {
                BuildSpecial.Content = "Normal Build";
            }
            else
            {
                BuildSpecial.Content = "Special Build";
            }

            //prelease
            bool prelease = App_Object.GetApp_Build_isPrelease();
            if (prelease)
            {
                BuildType.Content = "Prelease Version";
            }
            else
            {
                BuildType.Content = "Stable Version";
            }

        }
        #endregion


        String SendDataStr = String.Empty;
        String PlatformTypeStr = String.Empty;
        String SystemResourcesStr = String.Empty;
        Boolean StartonBoot;
        String SplitSizeStr = String.Empty;

        #region SetUpUserSettings
        private void  SetUpUserSettings()
        {
            //get setting
            SendDataStr = Araizen_Share.Properties.Settings.Default.Send_AnonymousData;
            PlatformTypeStr = Araizen_Share.Properties.Settings.Default.PlatformType;
            SystemResourcesStr = Araizen_Share.Properties.Settings.Default.System_ResourcesUsage;
            StartonBoot = Araizen_Share.Properties.Settings.Default.StartAppOnBoot;
            SplitSizeStr = Araizen_Share.Properties.Settings.Default.SplitSize;

            if (string.IsNullOrEmpty(SendDataStr))
            { SendDataOption.SelectedIndex = 0; }
            else
            {
                if (SendDataStr == "SendData")
                { SendDataOption.SelectedIndex = 0; }
                else { SendDataOption.SelectedIndex = 1; }
            }


            if (string.IsNullOrEmpty(PlatformTypeStr))
            { PlatformOption.SelectedIndex = 0; }
            else
            {
                if (PlatformTypeStr == "PlatformStable")
                { PlatformOption.SelectedIndex = 0; }
                else { PlatformOption.SelectedIndex = 1; }
            }

            if (string.IsNullOrEmpty(SystemResourcesStr))
            { SystemResources.SelectedIndex = 0; }
            else
            {
                if (SystemResourcesStr == "Optimal")
                { SystemResources.SelectedIndex = 0; }
                else { SystemResources.SelectedIndex = 1; }
            }

            if (string.IsNullOrEmpty(SplitSizeStr))
            { UploadSizes.SelectedIndex = 1; }
            else
            {
                if (SplitSizeStr == "Minimum")
                { UploadSizes.SelectedIndex = 0; }
                else if (SplitSizeStr == "Medium") { UploadSizes.SelectedIndex = 1; }
                else  { UploadSizes.SelectedIndex =2; }
            }

            if(ShareServiceState().ToLower().Equals("running") || ShareServiceState().ToLower().Equals("starting"))
            {

                serviceController.IsChecked = true;
                serviceStatus.Text = "Service is Running";
                serviceStatus.Foreground = new SolidColorBrush(Colors.Green);

                App_Share_Service_Status newStatus = new App_Share_Service_Status(status:true);
                    shareServiceStatus.SetShareServiceStatus(newStatus);
            }
            else if(ShareServiceState().ToLower().Equals("stopped"))
            {
               
                    serviceController.IsChecked = false;

                    serviceStatus.Text = "Service is Stopped,Unable to download or upload";
                    serviceStatus.Foreground = new SolidColorBrush(Colors.DarkRed);
                
            }
            if (ShareServiceState().ToLower().Equals("paused"))
            {

                serviceController.IsChecked = false;

                serviceStatus.Text = "Service is paused,Unable to download or upload";
                serviceStatus.Foreground = new SolidColorBrush(Colors.DarkRed);

            }else if (ShareServiceState().ToLower().Equals("notinstalled"))
            {
                serviceController.Opacity = 0;
                serviceStatus.Text = " Araizen Share Service not installed ";
                serviceStatus.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                serviceController.Opacity = 0;
                serviceStatus.Text = "Araizen Share Service Status Unknown(Not Running). ??.";
                serviceStatus.Foreground = new SolidColorBrush(Colors.DarkRed);
            }
        }
        #endregion

        #region ShareServiceState
        private string ShareServiceState()
        {
            string serviceName = "AraizenShareService";
            ServiceController sc = new ServiceController(serviceName);

            try
            {
                switch (sc.Status)
                {
                    case ServiceControllerStatus.Running:
                        return "Running";
                    case ServiceControllerStatus.Stopped:
                        return "Stopped";
                    case ServiceControllerStatus.Paused:
                        return "Paused";
                    case ServiceControllerStatus.StopPending:
                        return "Stopping";
                    case ServiceControllerStatus.StartPending:
                        return "Starting";
                    default:
                        return "Status Changing";
                }
            }catch(Exception ex)
            {
                Console.WriteLine("not installed {0}", ex);
                return "NotInstalled";
            }
        }
        #endregion

        #region SystemResources_SelectionChanged
        private void SystemResources_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = SystemResources.SelectedIndex;
            String SystemResourcesOptionSelected = String.Empty;
            if (index == 0) { SystemResourcesOptionSelected = "Optimal"; }
            else { SystemResourcesOptionSelected = "Minimal"; }

            Araizen_Share.Properties.Settings.Default.System_ResourcesUsage = (SystemResourcesOptionSelected);
            Araizen_Share.Properties.Settings.Default.Save();
        }
        #endregion

        #region SendDataOption_SelectionChanged
        private void SendDataOption_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = SendDataOption.SelectedIndex;
            String SendDataOptionSelected = String.Empty;
            if (index == 0) { SendDataOptionSelected = "SendData"; }
            else { SendDataOptionSelected = "DoNotSendData"; }

            Araizen_Share.Properties.Settings.Default.Send_AnonymousData = (SendDataOptionSelected);

            Araizen_Share.Properties.Settings.Default.Save();

        }
        #endregion

        #region PlatformOption_SelectionChanged
        private void PlatformOption_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = PlatformOption.SelectedIndex;
            String PlatformOptionSelected = String.Empty;
            if (index == 0) { PlatformOptionSelected = "PlatformStable"; }
            else { PlatformOptionSelected = "PlatformUnsatble"; }

            Araizen_Share.Properties.Settings.Default.PlatformType = (PlatformOptionSelected);
            Araizen_Share.Properties.Settings.Default.Save();

        }
        #endregion

        #region ApplicationDetails_Click
        private void ApplicationDetails_Click(object sender, RoutedEventArgs e)
        {
            Ui_AboutApp aboutApp = new Ui_AboutApp();
            aboutApp.Show();
        }
        #endregion

        #region LicenceDetails_Click
        private void LicenceDetails_Click(object sender, RoutedEventArgs e)
        {
            Ui_Licence m = new Ui_Licence();
            m.ShowDialog();
        }
        #endregion

        #region ServiceController_Checked
        private void ServiceController_Checked(object sender, RoutedEventArgs e)
        {
            
                Ui_Settings_AraizenService_WarningDialogue w = new Ui_Settings_AraizenService_WarningDialogue();
                if (w.ShowDialog() == false)
                {
                    bool weProceed = w.DoWeProceed;
                    if (weProceed)
                    {
                        serviceStatus.Text = "Service is Stopped";
                        serviceStatus.Foreground = new SolidColorBrush(Colors.Red);
                        App_Share_Service_Status newStatus = new App_Share_Service_Status(status: true);
                        shareServiceStatus.SetShareServiceStatus(newStatus);
                    }
                }
           
        }
        #endregion

        #region ServiceController_Unchecked
        private void ServiceController_Unchecked(object sender, RoutedEventArgs e)
        {

            if (fromConstructor)
            {
                serviceStatus.Text = "Service is Running";
                serviceStatus.Foreground = new SolidColorBrush(Colors.Green);
                App_Share_Service_Status newStatus = new App_Share_Service_Status(status: true);
                shareServiceStatus.SetShareServiceStatus(newStatus);

            }
        }
        #endregion

        #region StopService
        public static void StopService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
            }
            catch
            {
                // ...
            }
        }
        #endregion

        #region StartService
        public static void StartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch
            {
                // ...
            }
        }
        #endregion

        #region RestartService
        public static void RestartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                // count the rest of the timeout
                int millisec2 = Environment.TickCount;
                timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch
            {
                // ...
            }
        }
        #endregion

        #region  SystemReset_Click
        private void SystemReset_Click(object sender, RoutedEventArgs e)
        {
           
            Ui_Settings_SystemReset_WarningDialogue w = new Ui_Settings_SystemReset_WarningDialogue();
            if (w.ShowDialog() == false)
            {
                bool weProceed = w.DoWeProceed;
                if (weProceed)
                {
                    // all the files /folders under operations folder

                    //delete all the  databses
                    KillServiceDeleteApplicationDataAndStopApplication();


                }
            }
        }
        #endregion
    }
}
