﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Ui_Register : MetroWindow
    {
        //Boolean killOnExitPurchase = false;

        public Ui_Register(Boolean KillOnexit)
        {
            //if (KillOnexit)
            //{
            //    killOnExitPurchase = true;
            //}


            InitializeComponent();

            var th = new Task(() =>
            {
                while (true)
                {
                    var IsThereInterent = App_System_Wifi.CheckForInternetConnection();
                    try
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            if (IsThereInterent)
                            {
                             //   submit.IsEnabled = true;
                                Internetstatus.Foreground = new SolidColorBrush(Colors.Green);
                                Internetstatus.Content = "There Is Internet,Proceed.";
                            }
                            else
                            {
                                submit.IsEnabled = false;
                                Internetstatus.Foreground = new SolidColorBrush(Colors.Red);
                                Internetstatus.Content = "Connect to Internet to proceed";
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("internal error");
                    }
                    Thread.Sleep(1500);
                }
            });
            th.Start();
            
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            LoadingProgressring.Opacity = 1;
            contentsContainer.Opacity = 0;


            //check all he fields 

            String nameStr = name.Text;
            String emailStr = email.Text;

            //selected ccount type
            int index = accountType.SelectedIndex;
            String accountTypeStr = String.Empty;
            if (index == 0) { accountTypeStr = "personal"; }
            else { accountTypeStr = "Business"; }


            String mypasswordStr = mypassword.Password.ToString();
            String macAddressStr = App_System_Details.GetMacAddress();
            String appVersionStr = App_Object.GetApp_Version();
            DateTime regTime = DateTime.Now;
            //save App type
            App_Register reg = new App_Register(
                name: nameStr,
                email: emailStr,
                macAddress: macAddressStr,
                appVersion: appVersionStr,
                registerTime: regTime,
                accountType: accountTypeStr,
                password: mypasswordStr
                );
            App_Install install = new App_Install();
            App_Install_Register ireg = new App_Install_Register();
            App_Auth_File authFile = new App_Auth_File();

            App_Register_Result regs = ireg.SetAppRegisterDetailsServer(reg);

            Console.WriteLine("Register ui The result is {0}",reg);
            if (regs.isOkay == true)
            {
                Console.WriteLine("--ok--");


               ireg.SetAppRegisterDetailsFile(reg);


                ireg.SetAppInstallFile(
                        installTimePar: DateTime.Now,
                        macAddressPar: App_System_Details.GetMacAddress()
                       );

                //set auth details
                //DateTime userInstallAppDate,DateTime ,
                //String name, String accountType, String token, String email
                authFile.SetAuthDetailsFile(
                    userInstallAppDate: regTime,
                    logInTime : regTime,
                    name: nameStr,
                    accountType:accountTypeStr,
                    token: regs.reason,
                    email: emailStr

                    );


                Main_Window main_window = new Main_Window();
                main_window.Show();
                this.Hide();
            }
            else
            {
                LoadingProgressring.Opacity = 0;
                contentsContainer.Opacity = 1;

                Status.Content = regs.reason + "Try Again";
            }


        }

        private void Licence_Click(object sender, RoutedEventArgs e)
        {
            App_System_Wifi.Open_in_browser(App_Object.AraizenShareTermsAndConditionsUrl);

        }

        private void Mypasswordconfirm_KeyUp(object sender, KeyEventArgs e)
        {
            if (mypassword.Password.ToString().Equals(mypasswordconfirm.Password.ToString()))
            {
               // Status.Content = "Password and Confirm Password match";

                //String resourceDir = asps.Curr_Resources_Dir();
                //System.Uri resourceImageUri = new System.Uri(System.IO.Path.Combine(resourceDir, "images/ic_check_black_24dp_1x.png"));

                //  Console.WriteLine("->Resource Dir ={0} \n resource image uri ={1}", asps.Curr_Resources_Dir(), resourceImageUri);

                IsConfirmPasswordOk.Source = new BitmapImage(new Uri(@"/Resources/images/ic_check_black_24dp_1x.png", UriKind.Relative));
                submit.IsEnabled = true;
            }
            else { IsConfirmPasswordOk.Source = null; }
        }

        private void Mypassword_KeyUp(object sender, KeyEventArgs e)
        {
            String pass = mypassword.Password.ToString();
            if (IsPasswordValid(pass))
            {
              //  Status.Content = "Password ok";
                //this
                //var src = new Uri(@"Araizen_Crypto;component/image/ic_check_black_24dp_1x.png");
                //or
                IspasswordOk.Source = new BitmapImage(new Uri(@"/Resources/images/ic_check_black_24dp_1x.png", UriKind.Relative));


            }
            else { IspasswordOk.Source = null; }
        }


        private void Email_KeyUp(object sender, KeyEventArgs e)
        {
            String emailstr = email.Text;
            var res = IsValidEmail(emailstr);
            if (res)
            {
               // Status.Content = "Email Ok";

                //String resourceDir = asps.Curr_Resources_Dir();
                //System.Uri resourceImageUri = new System.Uri(System.IO.Path.Combine(resourceDir, "images/ic_check_black_24dp_1x.png"));

                //Console.WriteLine("->Resource Dir ={0} \n resource image uri ={1}", asps.Curr_Resources_Dir(), resourceImageUri);

                IsemailOk.Source = new BitmapImage(new Uri(@"/Resources/images/ic_check_black_24dp_1x.png", UriKind.Relative));

            }
            else { IsemailOk.Source = null; }
        }

        private void Name_KeyUp(object sender, KeyEventArgs e)
        {
            String namestr = name.Text;
            var res = IsNameValid(namestr);
            if (res)
            {
                //Status.Content = "Name ok";
                //String resourceDir = asps.Curr_Resources_Dir();
                //System.Uri resourceImageUri = new System.Uri(System.IO.Path.Combine(resourceDir, "images/ic_check_black_24dp_1x.png"));

                //Console.WriteLine("->Resource Dir ={0} \n resource image uri ={1}", asps.Curr_Resources_Dir(), resourceImageUri);

                //IsNameOk.Source  = new BitmapImage(new Uri("pack://application:,,,/AssemblyName;component/Resources/ic_check_black_24dp_1x.png"));
                IsNameOk.Source = new BitmapImage(new Uri(@"Resources/images/ic_check_black_24dp_1x.png", UriKind.Relative));

            }
            else { IsNameOk.Source = null; }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Ui_LoginRegisterOption loginRegister = new Ui_LoginRegisterOption();
            loginRegister.Show();
            this.Hide();

        }

        private void AccountType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        bool IsPasswordValid(String pass)
        {
            if (pass.Length > 4)
            {
                return true;
            }
            return false;
        }
        bool IsNameValid(String name)
        {
            if (name.Length > 2)
            {
                return true;
            }
            return false;
        }
        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

    }
}
