﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share
{
    /// <summary>
    /// single tone to avoid expensive calls to the slow file system
    /// </summary>
    #region App_Object
    class App_Object
    {
        //public const string App_Version_Const= "1.0.1";
        //public const string App_Build_Const= "1537763040";
  
        public static string  GetApp_Version()
        {
            //String ver = Araizen_Share.Properties.Settings.Default.AppVersion;

            //if(string.IsNullOrEmpty(ver) || string.IsNullOrWhiteSpace(ver))
            //{
            //    Araizen_Share.Properties.Settings.Default.AppVersion = App_Version_Const;//ProtectString.EncryptString(random.RandomItemName(25));
            //    Araizen_Share.Properties.Settings.Default.Save();
            //    return App_Version_Const;
            //}
            //else
            //{
            //    return ver;
            //}

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);

          return  versionInfo.ProductVersion;

        }
        public  static bool GetApp_Build_IsSpecial  () {

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);

            return versionInfo.IsSpecialBuild;

        }

        public static bool GetApp_Build_isPrelease()
        {

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);

            return versionInfo.IsPreRelease;

        }



        public static int ProcessorCount = Environment.ProcessorCount;

        public static int MaxProcessorThreads = Environment.ProcessorCount;
        public static int NormalProcessorThreads = Environment.ProcessorCount / 2;

        //public static readonly int SetRecourceUsage = GetResourceThreads();

        public static string SystemResourcesStr = Araizen_Share.Properties.Settings.Default.System_ResourcesUsage;


        private static String Optimal = "optimal";
        private const String Minimal = "minimal";

        /**
         * 
         * Arraizen URlS for communication
         * 
         * 
         */
         //terms and conditions
         public static String AraizenShareTermsAndConditionsUrl = "http://www.araizen.com/araizen/share/terms_and_conditions";
        //send feedback
         public static String AraizenShareSendFedbackUrl= "http://www.araizen.com/araizen/share/comms/Feeddback";
        //generate forrgot password serial url
         public static String AraizenShareCreateForgotPasswordSerialUrl = "http://www.araizen.com/araizen/share/comms/ForgotPassword_EmailConfirmation";
        //confirm password recovery serial
         public static String AraizenShareConfirmForgotPasswordSerialIsGenuineUrl = "http://www.araizen.com/araizen/share/comms/ForgotPassword_SerialConfirmation";
        //check new version
        public static String AraizenShareCheck_New_VersionUrl = "http://www.araizen.com/araizen/share/comms/NewVersion";
        //UpdateAppId
        public static String AraizenShareUpdateAppIdUrl = "http://www.araizen.com/araizen/share/comms/CheckUpdate";
        //send app install details
        public static String AraizenShareSend_Install_DetailsUrl = "http://www.araizen.com/share/comms/InstallApplicationDetails";
        //send user details
        public static String AraizenShareSend_User_DetailsUrl = "http://www.araizen.com/share/comms/InstallUserDetails.php";
        //get purchase details
        public static String AraizenShareGet_Purchase_DetailsUrl = "http://www.araizen.com/share/comms/GetPurchaseDetails";
        //check purchase key is ok
        public static String AraizenShareAuthentifyPurchaseKeyUrl = "http://www.araizen.com/araizen/share/comms/ConfirmPurchaseKey";
        //check if application is genuine  -->purchased application
        public static String AraizenShareIsAppGenuineUrl = "http://www.araizen.com/araizen/share/comms/IsApplicationGenuine";
        //block the application url
        public static String AraizenShareBlockApplicationUrl = "http://www.araizen.com/araizen/share/comms/BlockApplication";
        //get previous version url
        public static String AraizenShareGetPreviousAccountDetailsUrl = "http://www.araizen.com/araizen/comms/share/comms/RecoverPreviousAccountDetails";
        //unblock the application url
        public static String AraizenShareUnBlockApplicationUrl = "http://www.araizen.com/araizen/comms/share/comms/UnBlockApplications";




        


    }
    #endregion

}

