﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Araizen_Share
{
    class App_Upload_Server_Communication
    {

        #region UploadFileShareObject
        public string UploadFileShareObject(App_Upload uploadObject)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.UploadsShareFileObject());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(uploadObject);
                Console.WriteLine("hey write this ite for uploa \n {0}\n",json);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine("\n\n from App upload server communication the result is {0} ", result);

                return result;
            }
        }
        #endregion


        #region  Upload file
       // public static async Task<string> Upload(string name, string email, string date, string fileName, string filePath)
        public static string Upload(string name, string email, string date, string fileName, string filePath)
        {
            byte[] fileData = System.IO.File.ReadAllBytes(filePath);//HttpContext.Current.Server.MapPath(path));

            using (var client = new HttpClient())
            {
                using (var content =
                    new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                {

                    content.Add(new StreamContent(new MemoryStream(fileData)), "file", fileName);

                    ///share/upload
                    /////name , email , date
                    using (
                       var message =
                            client.PostAsync(string.Format(App_Url.UploadsUrlAddEmailDateFileName() + "{0}/{1}/{2}", email, date, name), content)) //http://127.0.0.1:12000/share/upload/sample-file
                    {
                        var input = message.ContinueWith((x)=> {

                            return "\n response -------from upload";
                        });//Content.ReadAsStringAsync();
                        Console.WriteLine("\n\n upload response {0}", input);
                        return "";
                       // return !string.IsNullOrWhiteSpace(input) ? Regex.Match(input, @"http://\w*\.directupload\.net/images/\d*/\w*\.[a-z]{3}").Value : null;
                    }
                }
            }
        }
        #endregion

    }
}
