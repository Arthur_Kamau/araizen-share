﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Araizen_Share
{
    /// <summary>
    /// Interaction logic for Ui_Login_ForgotPassword_ChangePassword.xaml
    /// </summary>
    public partial class Ui_Login_ForgotPassword_ChangePassword : MetroWindow
    {
        private string userEmail = string.Empty;

        public Ui_Login_ForgotPassword_ChangePassword(string email)
        {
            InitializeComponent();
            App_Registry reg = new App_Registry();
            //disable non essential keys
            Submit.IsEnabled = false;
            ConfirmNewPasskey.IsEnabled = false;

            userEmail = email;


        }

       

        private void ConfirmNewPasskey_KeyUp(object sender, KeyEventArgs e)
        {

            String newpass = NewPasskey.Password.ToString();
            String connewpas = ConfirmNewPasskey.Password.ToString();

            if (newpass.Equals(connewpas))
            {
                Submit.IsEnabled = true;
                IsSuccessful.Content = " ";
            }
            else
            {
                IsSuccessful.Content = "New password and confirm password should match";
            }

        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {

            String passwrodstr = NewPasskey.Password.ToString();

            Task.Run(() =>
            {

                // App_System_Communication ac = new App_System_Communication();
                App_Password appForgotPassword = new App_Password();

                App_ForgotPasswordItem_Result isOk = appForgotPassword.ForgotPasswordUpdatePassword(email: userEmail,newPassword: passwrodstr);

                if (isOk != null)
                {
                    if (isOk.IsOkay == true)
                    {
                        Dispatcher.Invoke((Action)delegate
                        {
                            //open login
                            Ui_Login_ForgotPassword_GoTo_Login proceeedToLogin = new Ui_Login_ForgotPassword_GoTo_Login();
                            proceeedToLogin.Show();
                            this.Close();
                        });
                    }
                    else
                    {
                        Dispatcher.Invoke((Action)delegate
                        {
                            //update status
                            IsSuccessful.Content = "Unsucessful please try again.";
                        });
                    }
                }
                else
                {
                    Dispatcher.Invoke((Action)delegate
                    {
                        //error
                        IsSuccessful.Content = "Error , restart App or try again.";
                    });
                }
            });
            IsSuccessful.Content = "Sending information ..";
            Submit.IsEnabled = false;
            InternetStatus.Content = "";


            this.Close();
        }

        private void NewPasskey_KeyUp(object sender, KeyEventArgs e)
        {
            String passwrodstr = NewPasskey.Password.ToString();

            if(passwrodstr.Length > 5)
            {
                ConfirmNewPasskey.IsEnabled = true;
                IsSuccessful.Content = " ";
            }
            else
            {
                IsSuccessful.Content = "Password should be more than 5 characters";
            }
        }
    }
}
