﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace Araizen_Share_Console
{
    class App_Notification_Sql
    {
        #region CreateNotificationItemsDatabase
        public void CreateNotificationItemsDatabase(string path)
        {
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=" + path))
            {
                conn.Open();


                string sqlQuery = @"
                    CREATE TABLE IF NOT EXISTS Notification_items 
                    (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,

                        notfication_from_name varchar(255),
                        notfication_from_email varchar(255),

                        notification_type varchar(255),  
                        notification_description varchar(255),

                        notifcation_seen varchar(255) ,
                        notification_date varchar(255),

                        notification_foreign_key_for_download varchar(255),  
                        notofication_url varchar(255)
                      
                    )";

                SQLiteCommand command = new SQLiteCommand(sqlQuery, conn);
                command.ExecuteNonQuery();
            }

        }
        #endregion

        /*
         *******************************************************************************************
         * 
         * insert functions
         * 
         * *******************************************************************************************
         */
        #region InsertNotificationItemsDatabase
        public void InsertNotificationItemsDatabase (App_Notification notificationItem)
        {
            App_System_Paths paths = new App_System_Paths();
            string notificationItemPath = paths.NotificationsSQL();
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=" + notificationItemPath))
            {
                conn.Open();

                string sqlQuery = string.Format(@"
                        insert into highscores 
                        (
                         notfication_from_name,
                        notfication_from_email ,

                        notification_type ,  
                        notification_description,

                        notifcation_seen ,
                        notification_date,

                        notification_foreign_key_for_download ,  
                        notofication_url 
                        ) values (
                       
                            '{0}', '{1}',
                            '{2}', '{3}',
                            '{4}' ,'{5}',
                            '{6}' , '{7}'


                        )",
                        notificationItem.Notfication_from_name, notificationItem.Notfication_from_email,
                        notificationItem.Notification_type , notificationItem.Notification_description,
                        notificationItem.Notifcation_seen, notificationItem.Notification_date,
                        notificationItem.Notification_foreign_key_for_download, notificationItem.Notofication_url

                 );


                SQLiteCommand command = new SQLiteCommand(sqlQuery, conn);
                command.ExecuteNonQuery();

            }
        }
        #endregion



        /*
         *******************************************************************************************
         * 
         * Get functions
         * 
         * *******************************************************************************************
         */
        #region GetDownloadItemsDatabase
        public void GetDownloadItemsDatabase()
        {
            App_System_Paths paths = new App_System_Paths();
            string notificationItemPath = paths.NotificationsSQL();
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=" + notificationItemPath))
            {
                conn.Open();

                SQLiteCommand command = new SQLiteCommand("Select * from download_items", conn);
                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    Console.WriteLine(reader["id"]);

                reader.Close();
            }
        }
        #endregion

        /*
        *******************************************************************************************
        * 
        * update function
        * 
        * *******************************************************************************************
        */
        #region UpdateNotificationItemsDatabase
        public void UpdateNotificationItemsDatabase()
        {
            App_System_Paths paths = new App_System_Paths();
            string notificationItemPath = paths.NotificationsSQL();
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=" + notificationItemPath))
            {
                conn.Open();

                SQLiteCommand command = new SQLiteCommand("Select * from download_items", conn);
                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    Console.WriteLine(reader["id"]);

                reader.Close();
            }
        }
        #endregion

        /*
         *******************************************************************************************
         * 
         * Delete function
         * 
         * *******************************************************************************************
         */
        #region DeleteNotificationItemsDatabase
        public void DeleteNotificationItemsDatabase()
        {
            App_System_Paths paths = new App_System_Paths();
            string notificationItemPath = paths.NotificationsSQL();
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=" + notificationItemPath))
            {
                conn.Open();

                SQLiteCommand command = new SQLiteCommand("Select * from download_items", conn);
                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    Console.WriteLine(reader["id"]);

                reader.Close();
            }
        }
        #endregion

    }
}
