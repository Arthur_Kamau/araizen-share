﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share_Console
{
    class App_Registry
    {
        /**
         * master pass key
         */ 
        public void StorePassKey(String passkey)
        {
            RegistryKey rkey = Registry.CurrentUser.CreateSubKey("A_Share");
            rkey.SetValue("pass", passkey);
        }
        public string GetPassKey()
        {
            try
            {
                RegistryKey rkey = Registry.CurrentUser.OpenSubKey("A_Share");
                String pass = Convert.ToString(rkey.GetValue("pass"));
                return pass;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
            return string.Empty;
        }

       

        /**
         * app start up 
         */
        RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

        public void StartAppOnBootTrue(string appPath, string appName = "Araizen Share") {
            Console.WriteLine("Application executable path {0}", appPath);
            rk.SetValue(appName, appPath);
        }
         public void StartAppOnBootFalse(string appPath, string appName = "Araizen Share") {
            rk.DeleteValue(appName, false);
        }

        /**
         * purchase key ,period and date
         */ 
        public void StorePurchaseKey(String passkey)
        {
            RegistryKey rkey = Registry.CurrentUser.CreateSubKey("A_Share");
            rkey.SetValue("Purchasekey", passkey);
        }
        public string GetPurchaseKey()
        {
            try
            {
                RegistryKey rkey = Registry.CurrentUser.OpenSubKey("A_Share");
                String pass = Convert.ToString(rkey.GetValue("Purchasekey"));
                Console.Write("registry purchasekey {0}",pass);
                return pass;
             }catch(Exception ex)
            {
                Console.Write(ex);
            }
            return string.Empty;
        }
        public void StorePurchasePeriod(String period)
        {
            RegistryKey rkey = Registry.CurrentUser.CreateSubKey("A_Share");
            rkey.SetValue("Purchaseperiod", period);
        }
        public string GetPurchasePeriod()
        {
            try
            {
                RegistryKey rkey = Registry.CurrentUser.OpenSubKey("A_Share");
                String pass = Convert.ToString(rkey.GetValue("Purchaseperiod"));
                Console.Write("registry purchase period {0}", pass);
                return pass;
            } catch(Exception ex)
            {
                Console.Write(ex);
            }
            return string.Empty;
        }

        public void StorePurchaseDate(DateTime purchaseDay)
        {
            RegistryKey rkey = Registry.CurrentUser.CreateSubKey("A_Share");
            rkey.SetValue("Purchaseday", purchaseDay);
        }
        public DateTime GetPurchaseDate()
        {
            try
            {
                RegistryKey rkey = Registry.CurrentUser.OpenSubKey("A_Share");
                String purchaseday = Convert.ToString(rkey.GetValue("Purchaseday"));
                Console.Write("registry purchasedate {0}", purchaseday);
                return DateTime.Parse(purchaseday);
            } catch(Exception ex)
            {
                Console.Write(ex);
            }
            return DateTime.Now;
        }

        /**
         * Application Recoverykey keys
         */
        public void StoreAraizenPreviousVersionRecoveryKey(String EncryptionKey)
        {
            RegistryKey rkey = Registry.CurrentUser.CreateSubKey("A_Share");
            rkey.SetValue("RecoveryKey", EncryptionKey);
        }
        public String GetAraizenPreviousVersionRecoveryKey()
        {
            try
            {
                RegistryKey rkey = Registry.CurrentUser.OpenSubKey("A_Share");
                String EncryptionKey = rkey.GetValue("RecoveryKey").ToString();
                return EncryptionKey;
            } catch(Exception ex)
            {
                Console.Write(ex);
            }
            return string.Empty;
        }



        /**
        * is Application blocked key
        */
        public void SetAppIsBlockedOrNot (Boolean ToBlockOrNot)
        {
            RegistryKey rkey = Registry.CurrentUser.CreateSubKey("A_Share");
            rkey.SetValue("AppBLock", ToBlockOrNot);
        }
        public Boolean GetAppIsBlockedOrNot()
        {
            try
            {
                RegistryKey rkey = Registry.CurrentUser.OpenSubKey("A_Share");
                Boolean IsBlocked = Convert.ToBoolean(rkey.GetValue("AppBLock").ToString().ToLower());
                return IsBlocked;
            }catch(Exception ex)
            {
                Console.Write(ex);
            }
            return false;
        }
        /**
          * is Application locked key
          */
        public void SetAppIsLockedOrNot(Boolean TolockOrNot)
        {
            RegistryKey rkey = Registry.CurrentUser.CreateSubKey("A_Share");
            rkey.SetValue("AppLock", TolockOrNot);
        }
        public string GetAppIsLockedOrNot()
        {
            try
            {
                RegistryKey rkey = Registry.CurrentUser.OpenSubKey("A_Share");
                string Islocked = rkey.GetValue("AppLock").ToString();
                return Islocked;
            }catch(Exception ex)
            {
                Console.Write(ex);
            }
            return string.Empty;
        }
        public void SetBlockedDate(DateTime date)
        {
            RegistryKey rkey = Registry.CurrentUser.CreateSubKey("A_Share");
            rkey.SetValue("AppLock_Date", date);
        }
        public DateTime GetBlockedDate()
        {
            try
            {
                RegistryKey rkey = Registry.CurrentUser.OpenSubKey("A_Share");
                DateTime BlockedDate = DateTime.Parse(rkey.GetValue("AppLock_Date").ToString());
                return BlockedDate;
            }catch(Exception ex)
            {
                Console.Write(ex);
            }
            return DateTime.Now;
        }



    }
}
