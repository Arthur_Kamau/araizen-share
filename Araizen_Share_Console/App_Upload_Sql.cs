﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace Araizen_Share_Console
{

    class App_Upload_Sql
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paths"></param>
        #region CreateUploadItemsDatabase
        public void CreateUploadItemsDatabase(string paths)
        {

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + paths))
            {
                sqlConnection.Open();


                string sql_item = @"
                    CREATE TABLE IF NOT EXISTS upload_items 
                    (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,

                        SourceClientsEmail varchar(255),
                        SourceClientsName varchar(255),  
                        DestinationEmailClients varchar(255),
                        Description varchar(255) ,
                        UploadDate varchar(255),
                        UnixTimeFileItemReferenceId varchar(255),  
                        FileMachineUpload varchar(255)
                      
                    )";


                string sql_file_item = @"
                    CREATE TABLE IF NOT EXISTS upload_file_item 
                    (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        FileName varchar(255),
                        FileSize varchar(255), 
                        TotalUploadItems varchar(255),
                        FileExt varchar(255),
                        FileEachItemSize  varchar(255),
                        CurrentUploadItem varchar(255),  
                        FileDownloadUrl varchar(255),

                        UploadDate varchar(255),
                        UnixTimeFileItemReferenceId varchar(255),  
                        FileLocation varchar(255),
                        IsStarted varchar(255),

                        IsCompleted varchar(255),  
                        FileEachItemLocation varchar(255)
                    )";

                SQLiteCommand command_item = new SQLiteCommand(sql_item, sqlConnection);
                command_item.ExecuteNonQuery();


                SQLiteCommand command_file_item = new SQLiteCommand(sql_file_item, sqlConnection);
                command_file_item.ExecuteNonQuery();

            }
        }
        #endregion

        /*
       *******************************************************************************************
       * 
       * insert functions
       * 
       * *******************************************************************************************
       */

        /// <summary>
        /// insert item
        /// </summary>
        /// <param name="uploadItem"></param>
        #region InsertUploadItemsDatabase
        public void InsertUploadItemsDatabase(App_Upload uploadItem)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.UploadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {

                //delete duplicate entry in database

                DeleteUploadItemsDatabase(uploadItem.UnixTimeFileItemReferenceId.ToString());
                DeleteFileItemsDatabase(uploadItem.UnixTimeFileItemReferenceId.ToString());

                //continue to insert
                sqlConnection.Open();
                string sqlQuery = string.Format(
           @"INSERT INTO upload_items
            (
                SourceClientsEmail, SourceClientsName,
                DestinationEmailClients ,Description,
                UploadDate , UnixTimeFileItemReferenceId ,
                FileMachineUpload
            )
            VALUES
            (
                '{0}', '{1}',
                '{2}', '{3}',
                '{4}', '{5}',
                '{6}'
            ); 
            ",
            uploadItem.SourceClientsEmail, uploadItem.SourceClientsName,
            String.Join(",", uploadItem.DestinationEmailClients), uploadItem.Description,
            uploadItem.UploadDate, uploadItem.UnixTimeFileItemReferenceId,
            uploadItem.FileMachineUpload


          );


                List<App_Upload_File_Item> uploadFileItems = uploadItem.FileItem;

                foreach (var fileItem in uploadFileItems)
                {
                    InsertUploadFileItemsDatabase(fileItem);
                }

                Console.WriteLine("\n\n --->> {0}", sqlQuery);
                //sqlCommand = sqlConnection.CreateCommand();
                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();
            }
        }
        #endregion

        /// <summary>
        /// insert file item
        /// </summary>
        /// <param name="uploadFileItem"></param>
        #region InsertUploadFileItemsDatabase
        public void InsertUploadFileItemsDatabase(App_Upload_File_Item uploadFileItem)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.UploadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();
                string sqlQuery = string.Format(
           @"INSERT INTO upload_file_item
            (
                FileName, FileSize, TotalUploadItems,
                FileExt ,FileEachItemSize,
                CurrentUploadItem ,FileDownloadUrl ,
                UploadDate , UnixTimeFileItemReferenceId , 
                FileLocation ,  IsStarted ,
                IsCompleted , FileEachItemLocation
            )
            VALUES
            (
                '{0}', '{1}','{2}',
                '{3}','{4}',
                '{5}', '{6}',
                '{7}', '{8}',
                '{9}', '{10}',
                '{11}', '{12}'
            ); 
            ",
           uploadFileItem.FileName, uploadFileItem.FileSize, uploadFileItem.TotalUploadItems,
           uploadFileItem.FileExt, uploadFileItem.FileEachItemSize,
           uploadFileItem.CurrentUploadItem, uploadFileItem.FileDownloadUrl,
           uploadFileItem.UploadDate, uploadFileItem.UnixTimeFileItemReferenceId,
           uploadFileItem.FileLocation, uploadFileItem.IsStarted,
           uploadFileItem.IsCompleted, uploadFileItem.FileEachItemLocation


          );

                Console.WriteLine("\n\n\n ---> {0}", sqlQuery);
                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();

            }
        }
        #endregion

        /*
        *******************************************************************************************
        * 
        * Get functions
        * 
        * *******************************************************************************************
        */

        /// <summary>
        /// 
        /// </summary>
        /// <returns> List<App_Upload> </returns>
        #region GetAllUploadItemsDatabase
        public List<App_Upload> GetAllUploadItemsDatabase()
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.UploadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();

                List<App_Upload> upload_items = new List<App_Upload>();

                string sql = @"
                select   
                SourceClientsEmail, SourceClientsName,
                DestinationEmailClients ,Description,
                UploadDate , UnixTimeFileItemReferenceId ,
                FileMachineUpload

                from upload_items order by id";


                SQLiteCommand command = new SQLiteCommand(sql, sqlConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {

                    Console.WriteLine("");

                    string userSourceEmail = reader["SourceClientsEmail"].ToString();
                    string userSourceName = reader["SourceClientsName"].ToString();
                    List<string> userDestinationemails = reader["DestinationEmailClients"].ToString().Split(",".ToCharArray()).ToList<string>();
                    string userDescription = reader["SourceClientsEmail"].ToString();
                    string userUploadDate = reader["UploadDate"].ToString();
                    long userUnixTimeFileItemReferenceId = Convert.ToInt64(reader["UnixTimeFileItemReferenceId"].ToString());
                    string userFileMachineUpload = reader["FileMachineUpload"].ToString();


                    //get file details
                    List<App_Upload_File_Item> uploadFileItems = GetFileItemsDatabase(userUnixTimeFileItemReferenceId);


                    App_Upload anUploadItem = new App_Upload(
                          sourceEmail: userSourceEmail,
                           sourceName: userSourceName,
                            destinationemails: userDestinationemails,
                             description: userDescription,
                             fileMachineUpload: userFileMachineUpload,
                              uploadTime: userUploadDate,
                               unixFileTimeReferenceId: userUnixTimeFileItemReferenceId,
                                fileItem: uploadFileItems
                    );

                    Console.WriteLine("GetAllUploadItemsDatabase email = '{0}' and unixFileTimeReferenceId = '{1}'", userSourceEmail, userUnixTimeFileItemReferenceId);

                    upload_items.Add(anUploadItem);
                }


                return upload_items;
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="column_name"></param>
        /// <returns></returns>
        #region GetAnUploadItemsDatabase
        public App_Upload GetAnUploadItemsDatabase(string value, string column_name)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.UploadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();

                string sql = string.Format(

                @"
                select   
                SourceClientsEmail, SourceClientsName,
                DestinationEmailClients ,Description,
                UploadDate , UnixTimeFileItemReferenceId ,
                FileMachineUpload

                from upload_items where '{0}' = '{1}' order by id", column_name, value
                );


                SQLiteCommand command = new SQLiteCommand(sql, sqlConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {

                    string userSourceEmail = reader["SourceClientsEmail"].ToString();
                    string userSourceName = reader["SourceClientsName"].ToString();
                    List<string> userDestinationemails = reader["SourceClientsName"].ToString().Split(",".ToCharArray()).ToList<string>();
                    string userDescription = reader["SourceClientsEmail"].ToString();
                    string userUploadDate = reader["UploadDate"].ToString();
                    long userUnixTimeFileItemReferenceId = Convert.ToInt64(reader["UnixTimeFileItemReferenceId"].ToString());
                    string userFileMachineUpload = reader["FileMachineUpload"].ToString();


                    //get file details
                    List<App_Upload_File_Item> uploadFileItems = GetFileItemsDatabase(userUnixTimeFileItemReferenceId);


                    App_Upload anUploadItem = new App_Upload(
                          sourceEmail: userSourceEmail,
                           sourceName: userSourceName,
                            destinationemails: userDestinationemails,
                             description: userDescription,
                             fileMachineUpload: userFileMachineUpload,
                              uploadTime: userUploadDate,
                               unixFileTimeReferenceId: userUnixTimeFileItemReferenceId,
                                fileItem: uploadFileItems
                    );

                    Console.WriteLine("GetAnUploadItemsDatabase email = {0} and unixFileTimeReferenceId = {1}", userSourceEmail, userUnixTimeFileItemReferenceId);

                    return anUploadItem;
                }
                return null;
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UnixTimeFileItemReferenceId"></param>
        /// <returns> List<App_Upload_File_Item> </returns>
        #region GetFileItemsDatabase
        public List<App_Upload_File_Item> GetFileItemsDatabase(long UnixTimeFileItemReferenceId)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.UploadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();

                List<App_Upload_File_Item> upload_items = new List<App_Upload_File_Item>();

                string sql = string.Format(

                     @"
                select   
                        FileName ,
                        TotalUploadItems,
                        FileSize ,  
                        FileExt ,
                        FileEachItemSize  ,
                        CurrentUploadItem ,  
                        FileDownloadUrl ,

                        UploadDate ,
                          
                        FileLocation ,
                        IsStarted ,

                        IsCompleted ,  
                        FileEachItemLocation 

                from upload_file_item where UnixTimeFileItemReferenceId ='{0}' order by id",
                     UnixTimeFileItemReferenceId
                    );

                SQLiteCommand command = new SQLiteCommand(sql, sqlConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {

                    string useFileName = reader["FileName"].ToString();
                    string userFileSize = reader["FileSize"].ToString();
                    string totalUploadItems = reader["TotalUploadItems"].ToString();

                    string userFileExt = reader["FileExt"].ToString();

                    string useFileEachItemSize = reader["FileEachItemSize"].ToString();
                    string userCurrentUploadItem = reader["CurrentUploadItem"].ToString();
                    string userFileDownloadUrl = reader["FileDownloadUrl"].ToString();


                    string useUploadDate = reader["UploadDate"].ToString();
                    string userFileLocation = reader["FileLocation"].ToString();
                    string userIsStarted = reader["IsStarted"].ToString();


                    string userIsCompleted = reader["IsCompleted"].ToString();
                    string userFileEachItemLocation = reader["FileEachItemLocation"].ToString();


                    App_Upload_File_Item anUploadItem = new App_Upload_File_Item(
                         fileName: useFileName,
                         totalUploadItems: Convert.ToInt32(totalUploadItems),
                         fileSize: Convert.ToInt64(userFileSize),
                         fileExt: userFileExt,
                         fileEachItemSize: Convert.ToInt32(useFileEachItemSize),
                         currentUploadItem: Convert.ToInt32(userCurrentUploadItem),
                         fileDownloadUrl: userFileDownloadUrl,
                         uploadDate: useUploadDate,
                         fileLocation: userFileLocation,
                         isStarted: userIsStarted.Equals("1") || userIsStarted.ToLower().Equals("true") ? true : false,//Convert.ToBoolean(userIsStarted),
                         isCompleted: userIsCompleted.Equals("1") || userIsCompleted.ToLower().Equals("true") ? true : false,// Convert.ToBoolean(userIsCompleted),
                         fileEachItemLocation: userFileEachItemLocation,
                         unixTimeFileItemReferenceId: UnixTimeFileItemReferenceId

                    );

                    Console.WriteLine("GetFileItemsDatabase  file name = {0} and unixFileTimeReferenceId = {1}", useFileName, UnixTimeFileItemReferenceId);

                    upload_items.Add(anUploadItem);
                }

                return upload_items;
            }
        }
        #endregion

        /*
        *******************************************************************************************
        * 
        * Update functions
        * 
        * *******************************************************************************************
        */
        /// <summary>
        /// update
        /// </summary>
        /// <param name="file_name"></param>
        #region UpdateUploadFileItemsIsStartedDatabase
        public void UpdateUploadFileItemsIsStartedDatabase(string file_name)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.UploadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();

                string sqlQuery = string.Format(

                    @"UPDATE upload_file_item 
                    SET IsStarted = 'true' 
                    WHERE FileName = '{0}'

                    ", file_name
                    );

                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();


            }
        }
        #endregion

        /// <summary>
        /// update
        /// </summary>
        /// <param name="value"></param>
        /// <param name="file_name"></param>
        #region UpdateUploadFileItemsNumberDatabase
        public void UpdateUploadFileItemsNumberDatabase(string value, string file_name)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.UploadSQL();

            //    sqlConnection = new SQLiteConnection("Data Source="+uploadItemPath+";Version=3;");
            //    sqlConnection.Open();
            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();


                string sqlQuery = string.Format(

                    @"UPDATE upload_file_item 
                    SET CurrentUploadItem = '{0}'
                    WHERE FileName = '{1}'

                    ", value, file_name
                    );

                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();


            }
        }
        #endregion

        /// <summary>
        /// update
        /// </summary>
        /// <param name="file_name"></param>
        #region UpdateUploadFileItemsIsCompletedDatabase
        public void UpdateUploadFileItemsIsCompletedDatabase(string file_name)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.UploadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();

                string sqlQuery = string.Format(

                    @"UPDATE upload_file_item 
                    SET IsCompleted = 'true'  
                    WHERE FileName = '{0}'

                    ", file_name
                    );

                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();


            }
        }
        #endregion

        /****************************************************************
         * 
         *
         * check for duplicates and delete if items exist check by name
         *
         *
         */
        /// <summary>
        /// delete item based on   UnixTimeFileItemReferenceId
        /// </summary>
        /// <param name="UnixTimeFileItemReferenceId"></param>
        #region DeleteUploadItemsDatabase
        public void DeleteUploadItemsDatabase(string UnixTimeFileItemReferenceId)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.UploadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();
                //delete item
                string sqlQuery = string.Format(@"DELETE FROM upload_items WHERE UnixTimeFileItemReferenceId = '{0}'", UnixTimeFileItemReferenceId);
                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();
            }
        }
        #endregion

        /// <summary>
        /// delete file item
        /// </summary>
        /// <param name="UnixTimeFileItemReferenceId"></param>
        #region DeleteFileItemsDatabase
        public void DeleteFileItemsDatabase(string UnixTimeFileItemReferenceId)
        {
            App_System_Paths paths = new App_System_Paths();
            string uploadItemPath = paths.UploadSQL();

            using (SQLiteConnection sqlConnection = new SQLiteConnection(@"Data Source=" + uploadItemPath))
            {
                sqlConnection.Open();

                //delete item
                string sqlQuery = string.Format(@"DELETE FROM upload_file_item WHERE UnixTimeFileItemReferenceId = '{0}'", UnixTimeFileItemReferenceId);
                SQLiteCommand command = new SQLiteCommand(sqlQuery, sqlConnection);
                command.ExecuteNonQuery();
            }
        }
        #endregion
    }
}