﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Araizen_Share_Console
{
    /// <summary>
    /// deal with file io
    /// </summary>
    #region App_Auth_File class
    class App_Auth_File
    {
        App_System_Paths paths = new App_System_Paths();
        App_File files = new App_File();

        /// <summary>
        /// get auth details from file
        /// </summary>
        /// <returns></returns>
        #region GetAuthDetails
        public App_Auth GetAuthDetailsFile()
        {
           
            string json_str = files.Get_data_from_file(paths.AuthFile());
          
            App_Auth auth = JsonConvert.DeserializeObject<App_Auth>(json_str);


            return auth;

        }
        #endregion

        /// <summary>
        /// set auth details to file
        /// </summary>
        /// <param name="logInTime"></param>
        /// <param name="name"></param>
        /// <param name="token"></param>
        /// <param name="email"></param>
        #region SetAuthDetailsFile
        public void SetAuthDetailsFile(DateTime userInstallAppDate,DateTime logInTime, String name, String accountType, String token, String email)
        {
            App_Auth auth = new App_Auth(name: name, token: token, accountType: accountType,installTime:userInstallAppDate,logInTime: logInTime.ToString(), email: email);

            string json_str = JsonConvert.SerializeObject(auth);
         
            files.Save_data_to_file(path: paths.AuthFile(), data: json_str);

        }
        #endregion




        #region GetAuthDetailsServer
        public App_Auth_Credential_Result GetAuthDetailsServer(App_Auth_Credential credential)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(App_Url.AppLoginServerUrl());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(credential);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
              
                if (result.Equals("err"))
                {
                    Console.WriteLine("\n Erroro in app auth from server");
                    //caution returning null
                    return null;
                }
                else
                {
                    App_Auth_Credential_Result auth = JsonConvert.DeserializeObject<App_Auth_Credential_Result>(result);

                    auth.AuthData.LogInTime = DateTime.Now.ToString();
                    return auth;
                }
            }
        }
        #endregion




       
    }
    #endregion


  

    public class App_ConfirmPassword_Result
    {
        public string Reason { get; set; }
        public bool IsOkay { get; set; }

        public App_ConfirmPassword_Result(string reason, bool isOkay)
        {
            Reason = reason;
            IsOkay = isOkay;
        }
    }
    /// <summary>
    /// Auth object ghost passkey and main passkey
    /// </summary>
    #region Auth class
    class App_Auth
    {
        public App_Auth(string name, string token,string accountType, DateTime installTime, string logInTime, String email)
        {
            Name = name;
            Email = email;
            Token = token;
            InstallTime = installTime;
            AccountType = accountType;
            LogInTime = logInTime;

        }

        public String Name { get; set; }
        public String Email { get; set; }
        public String AccountType { get; set; }
        public String Token { get; set; }
        public String LogInTime { get; set; }
        public DateTime InstallTime { get; set; }

    }
    #endregion

    #region
    class App_Auth_Credential
    {


       public string Password;
        public string Email;
      

        public App_Auth_Credential(string password, string email)
        {
            Password = password;
            Email = email;
          
        }
    }
    #endregion

    class App_Auth_Credential_Result
    {
        public bool isAuthenticated;
        public App_Auth AuthData;
        public int attempt;

        public App_Auth_Credential_Result(bool isAuthenticated, App_Auth authData, int attempt)
        {
            this.isAuthenticated = isAuthenticated;
            AuthData = authData;
            this.attempt = attempt;
        }
    }
}
