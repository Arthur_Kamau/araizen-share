﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Windows;

namespace Araizen_Share_Console
{
    class App_System_Notification
    {

        public static void ShowCustomMessage(string messageTitle, string messageBody)
        {
            NotifyIcon notifyIcon = new NotifyIcon();
           // Stream iconStream = System.Windows.Application.GetResourceStream(new Uri("pack://application:,,,/Resources/images/img-.ico")).Stream;
          //  notifyIcon.Icon = new System.Drawing.Icon(iconStream);
            notifyIcon.Text = string.Format(messageTitle);
            notifyIcon.Visible = true;
            notifyIcon.ShowBalloonTip(1000, messageTitle, messageBody, ToolTipIcon.Info);
            notifyIcon.Visible = false;
            notifyIcon.Dispose();
        }
        public static void ShowInfoSystemIcon()
        {
            NotifyIcon notifyIcon = new NotifyIcon();
          //  Stream iconStream = System.Windows.Application.GetResourceStream(new Uri("pack://application:,,,/Resources/images/img-.ico")).Stream;
          //  notifyIcon.Icon = new System.Drawing.Icon(iconStream);
            notifyIcon.Text = string.Format("Araizen Share has loaded.");
            notifyIcon.Visible = true;
            notifyIcon.ShowBalloonTip(5000, "Success Loading", "Araizen Share has successfully loaded", ToolTipIcon.Info);
            notifyIcon.Visible = false;
            notifyIcon.Dispose();
        }

        public static void FiniishedLoadingTask()
        {
            NotifyIcon notifyIcon = new NotifyIcon();
         //   Stream iconStream = System.Windows.Application.GetResourceStream(new Uri("pack://application:,,,/Resources/images/img-.ico")).Stream;
            //notifyIcon.Icon = new System.Drawing.Icon(iconStream);
            notifyIcon.Text = string.Format("Araizen Share has Finished task.");
            notifyIcon.Visible = true;
            notifyIcon.ShowBalloonTip(5000, "Success Finished Task", "Araizen Share has successfully finished task", ToolTipIcon.Info);
            notifyIcon.Visible = false;
            notifyIcon.Dispose();
        }
    }
}
