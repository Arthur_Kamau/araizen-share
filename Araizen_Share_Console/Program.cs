﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Araizen_Share_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //check if db file exist
            //if not exist create the
            CreateDatabaseFilesIfNecessary();


            var exitCode = HostFactory.Run(x =>
            { 
                 x.Service<ShareBootstrap>(s =>
                 {
                     s.ConstructUsing(sharebootstraper => new ShareBootstrap());

                     s.WhenStarted(sharebootstraper => sharebootstraper.Start());
                     s.WhenStopped(sharebootstraper => sharebootstraper.Stop());

                     s.WhenPaused(sharebootstraper => sharebootstraper.Pause());
                     s.WhenContinued(sharebootstraper => sharebootstraper.Resume());

                    // s.BeforeInstall(sharebootstraper => sharebootstraper.BeforeInstall());
                    // s.AfterInstall(sharebootstraper => sharebootstraper.AfterInstall());
                    // s.BeforeUninstall(sharebootstraper => sharebootstraper.BeforeUninstall());

                 });
                x.RunAsLocalSystem();
                x.SetServiceName("AraizenShareService");
                x.SetDisplayName("Araizen Share Service");
                x.SetDescription("Araizen Share service that powers Araizen share ui");
                //let service start automatically
                x.StartAutomatically();
            });

            int exitCodeValue = (int)Convert.ChangeType(exitCode, exitCode.GetTypeCode());
            Environment.ExitCode = exitCodeValue;
        }

        #region CreateDatabaseFilesIfNecessary
        private static void CreateDatabaseFilesIfNecessary()
        {
            //create databases if not exist
            //
            //first get all paths
            //notifications db
            App_System_Paths paths = new App_System_Paths();

          var pathToNotificationsDb = paths.NotificationsSQL();

            //downloading db
         var   pathToDownloadDb = paths.DownloadSQL();

            //uploading db
          var  pathToUploadDb = paths.UploadSQL();

            //history db
           var  pathToHistoryDb = paths.HistorySQL();

            

        }
        #endregion

    }
}
